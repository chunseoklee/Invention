/*! 3 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Inv
{
  public struct AndroidLayoutSize
  {
    public AndroidLayoutSize(int Width, int Height)
    {
      this.Width = Width;
      this.Height = Height;
    }

    public static readonly AndroidLayoutSize Empty = new AndroidLayoutSize();

    public int Width;
    public int Height;
  }

  public interface AndroidLayoutElement
  {
    AndroidLayoutSize CalculateSize(AndroidLayoutSize MaxSize);
    Android.Views.View View { get; }
  }

  public sealed class AndroidLayoutButton : Android.Widget.FrameLayout, AndroidLayoutElement
  {
    public AndroidLayoutButton(Android.Content.Context context)
      : base(context)
    {
      this.Clickable = true;
      base.Background = new AndroidBackground();
    }

    public new AndroidBackground Background
    {
      get { return (AndroidBackground)base.Background; }
    }

    public void SetContentView(AndroidLayoutElement Element)
    {
      if (this.ContentElement != Element)
      {
        if (this.ContentElement != null)
          RemoveView(this.ContentElement.View);

        this.ContentElement = Element;

        if (this.ContentElement != null)
          AddView(this.ContentElement.View);
      }
    }

    protected override void OnMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
      base.OnMeasure(widthMeasureSpec, heightMeasureSpec);

      int ResultWidth;
      int ResultHeight;

      if (ContentElement == null)
      {
        ResultWidth = 0;
        ResultHeight = 0;
      }
      else
      {
        var DesiredSize = ContentElement.CalculateSize(new AndroidLayoutSize(Width, Height));

        var WidthMode = MeasureSpec.GetMode(widthMeasureSpec);
        var WidthSize = MeasureSpec.GetSize(widthMeasureSpec);
        var HeightMode = MeasureSpec.GetMode(heightMeasureSpec);
        var HeightSize = MeasureSpec.GetSize(heightMeasureSpec);

        //Measure Width
        if (WidthMode == MeasureSpecMode.Exactly)
        {
          //Must be this size
          ResultWidth = WidthSize;
        }
        else if (WidthMode == MeasureSpecMode.AtMost)
        {
          //Can't be bigger than...
          ResultWidth = Math.Min(DesiredSize.Width, WidthSize);
        }
        else
        {
          //Be whatever you want
          ResultWidth = DesiredSize.Width;
        }

        //Measure Height
        if (HeightMode == MeasureSpecMode.Exactly)
        {
          //Must be this size
          ResultHeight = HeightSize;
        }
        else if (HeightMode == MeasureSpecMode.AtMost)
        {
          //Can't be bigger than...
          ResultHeight = Math.Min(DesiredSize.Height, HeightSize);
        }
        else
        {
          //Be whatever you want
          ResultHeight = DesiredSize.Height;
        }
      }

      SetMeasuredDimension(ResultWidth, ResultHeight);
    }
    protected override void OnSizeChanged(int w, int h, int oldw, int oldh)
    {
      base.OnSizeChanged(w, h, oldw, oldh);

      if (ContentElement != null)
      {
        ContentElement.View.LayoutParameters = new AndroidLayoutButton.LayoutParams(w, h)
        {
          LeftMargin = 0,
          TopMargin = 0
        };
      }
    }

    View AndroidLayoutElement.View
    {
      get { return this; }
    }
    AndroidLayoutSize AndroidLayoutElement.CalculateSize(AndroidLayoutSize MaxSize)
    {
      return ContentElement != null ? ContentElement.CalculateSize(MaxSize) : AndroidLayoutSize.Empty;
    }

    private AndroidLayoutElement ContentElement;
  }

  public sealed class AndroidLayoutEdit : Android.Widget.EditText, AndroidLayoutElement
  {
    public AndroidLayoutEdit(Android.Content.Context context)
      : base(context)
    {
      base.Background = new AndroidBackground();

      SetSingleLine();
    }

    public new AndroidBackground Background
    {
      get { return (AndroidBackground)base.Background; }
    }

    View AndroidLayoutElement.View
    {
      get { return this; }
    }
    AndroidLayoutSize AndroidLayoutElement.CalculateSize(AndroidLayoutSize MaxSize)
    {
      Measure(MaxSize.Width, MaxSize.Height);

      return new AndroidLayoutSize(MeasuredWidth, MeasuredHeight);
    }
  }

  public sealed class AndroidLayoutLabel : Android.Widget.TextView, AndroidLayoutElement
  {
    public AndroidLayoutLabel(Android.Content.Context context)
      : base(context)
    {
      base.Background = new AndroidBackground();

      Ellipsize = Android.Text.TextUtils.TruncateAt.End;
    }

    public new AndroidBackground Background
    {
      get { return (AndroidBackground)base.Background; }
    }

    View AndroidLayoutElement.View
    {
      get { return this; }
    }
    AndroidLayoutSize AndroidLayoutElement.CalculateSize(AndroidLayoutSize MaxSize)
    {
      Measure(MaxSize.Width, MaxSize.Height);

      return new AndroidLayoutSize(MeasuredWidth, MeasuredHeight);
    }
  }

  public enum AndroidLayoutHorizontal
  {
    Stretch,
    Center,
    Left,
    Right
  }

  public enum AndroidLayoutVertical
  {
    Stretch,
    Center,
    Top,
    Bottom
  }

  public sealed class AndroidLayoutContainer : Android.Widget.FrameLayout, AndroidLayoutElement
  {
    public AndroidLayoutContainer(Android.Content.Context context)
      : base(context)
    {
      this.ContentWidth = -1;
      this.ContentHeight = -1;
      this.ContentMinimumWidth = -1;
      this.ContentMinimumHeight = -1;
      this.ContentMaximumWidth = -1;
      this.ContentMaximumHeight = -1;
      this.ContentVisible = true;
    }

    public void SetContentVisiblity(bool ContentVisible)
    {
      if (ContentVisible != this.ContentVisible)
      {
        this.ContentVisible = ContentVisible;

        this.Arrange();
      }
    }
    public void SetContentHorizontal(AndroidLayoutHorizontal ContentHorizontal)
    {
      if (ContentHorizontal != this.ContentHorizontal)
      {
        this.ContentHorizontal = ContentHorizontal;

        this.Arrange();
      }
    }
    public void SetContentVertical(AndroidLayoutVertical ContentVertical)
    {
      if (ContentVertical != this.ContentVertical)
      {
        this.ContentVertical = ContentVertical;

        this.Arrange();
      }
    }
    public void SetContentElement(AndroidLayoutElement ContentElement)
    {
      if (ContentElement != this.ContentElement)
      {
        if (this.ContentElement != null)
          RemoveView(this.ContentElement.View);

        this.ContentElement = ContentElement;

        if (this.ContentElement != null)
          AddView(this.ContentElement.View);

        this.Arrange();
      }
    }
    public void SetContentWidth(int ContentWidth)
    {
      if (ContentWidth != this.ContentWidth)
      {
        this.ContentWidth = ContentWidth;

        this.Arrange();
      }
    }
    public void SetContentHeight(int ContentHeight)
    {
      if (ContentHeight != this.ContentHeight)
      {
        this.ContentHeight = ContentHeight;

        this.Arrange();
      }
    }
    public void SetContentMinimumWidth(int ContentMinimumWidth)
    {
      if (ContentMinimumWidth != this.ContentMinimumWidth)
      {
        this.ContentMinimumWidth = ContentMinimumWidth;

        this.Arrange();
      }
    }
    public void SetContentMaximumWidth(int ContentMaximumWidth)
    {
      if (ContentMaximumWidth != this.ContentMaximumWidth)
      {
        this.ContentMaximumWidth = ContentMaximumWidth;

        this.Arrange();
      }
    }
    public void SetContentMinimumHeight(int ContentMinimumHeight)
    {
      if (ContentMinimumHeight != this.ContentMinimumHeight)
      {
        this.ContentMinimumHeight = ContentMinimumHeight;

        this.Arrange();
      }
    }
    public void SetContentMaximumHeight(int ContentMaximumHeight)
    {
      if (ContentMaximumHeight != this.ContentMaximumHeight)
      {
        this.ContentMaximumHeight = ContentMaximumHeight;

        this.Arrange();
      }
    }

    protected override void OnSizeChanged(int w, int h, int oldw, int oldh)
    {
      base.OnSizeChanged(w, h, oldw, oldh);

      if (ContentElement != null)
      {
        var Element = ContentElement.View;

        if (!ContentVisible)
        {
          Element.Visibility = ViewStates.Gone;
        }
        else
        {
          var Bounds = new Android.Graphics.Rect(0, 0, w, h);

          var ContentSize = ((AndroidLayoutElement)this).CalculateSize(new AndroidLayoutSize(w, h));

          Element.Visibility = ViewStates.Visible;

          var SizeWidth = ContentSize.Width;
          int SizeX;

          if (SizeWidth > w)
          {
            SizeWidth = w;
            SizeX = 0;
          }
          else
          {
            switch (ContentHorizontal)
            {
              case AndroidLayoutHorizontal.Stretch:
                if (/*(ContentSize.Width > Bounds.Width) ||*/ (ContentWidth >= 0 && ContentWidth < w) || (ContentMaximumWidth >= 0 && ContentMaximumWidth < w))
                {
                  SizeWidth = ContentSize.Width;
                  SizeX = (w - SizeWidth) / 2;
                }
                else
                {
                  SizeWidth = w;
                  SizeX = 0;
                }
                break;

              case AndroidLayoutHorizontal.Center:
                SizeX = (w - SizeWidth) / 2;
                break;

              case AndroidLayoutHorizontal.Left:
                SizeX = 0;
                break;

              case AndroidLayoutHorizontal.Right:
                SizeX = w - SizeWidth;
                break;

              default:
                throw new Exception("LayoutVertical not handled.");
            }
          }

          var SizeHeight = ContentSize.Height;
          int SizeY;

          if (SizeHeight > h)
          {
            SizeHeight = h;
            SizeY = 0;
          }
          else
          {
            switch (ContentVertical)
            {
              case AndroidLayoutVertical.Stretch:
                if (/*(ContentSize.Height > Bounds.Height) ||*/ (ContentHeight >= 0 && ContentHeight < h) || (ContentMaximumHeight >= 0 && ContentMaximumHeight < h))
                {
                  SizeHeight = ContentSize.Height;
                  SizeY = (h - SizeHeight) / 2;
                }
                else
                {
                  SizeHeight = h;
                  SizeY = 0;
                }
                break;

              case AndroidLayoutVertical.Center:
                SizeY = (h - SizeHeight) / 2;
                break;

              case AndroidLayoutVertical.Top:
                SizeY = 0;
                break;

              case AndroidLayoutVertical.Bottom:
                SizeY = h - SizeHeight;
                break;

              default:
                throw new Exception("LayoutVertical not handled.");
            }
          }

          Element.LayoutParameters = new AndroidLayoutContainer.LayoutParams(SizeWidth, SizeHeight)
          {
            LeftMargin = SizeX,
            TopMargin = SizeY
          };
        }
      }
    }
    protected override void OnMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
      base.OnMeasure(widthMeasureSpec, heightMeasureSpec);

      int ResultWidth;
      int ResultHeight;

      if (ContentElement == null)
      {
        ResultWidth = 0;
        ResultHeight = 0;
      }
      else
      {
        var DesiredSize = ContentElement.CalculateSize(new AndroidLayoutSize(Width, Height));

        var WidthMode = MeasureSpec.GetMode(widthMeasureSpec);
        var WidthSize = MeasureSpec.GetSize(widthMeasureSpec);
        var HeightMode = MeasureSpec.GetMode(heightMeasureSpec);
        var HeightSize = MeasureSpec.GetSize(heightMeasureSpec);

        //Measure Width
        if (WidthMode == MeasureSpecMode.Exactly)
        {
          //Must be this size
          ResultWidth = WidthSize;
        }
        else if (WidthMode == MeasureSpecMode.AtMost)
        {
          //Can't be bigger than...
          ResultWidth = Math.Min(DesiredSize.Width, WidthSize);
        }
        else
        {
          //Be whatever you want
          ResultWidth = DesiredSize.Width;
        }

        //Measure Height
        if (HeightMode == MeasureSpecMode.Exactly)
        {
          //Must be this size
          ResultHeight = HeightSize;
        }
        else if (HeightMode == MeasureSpecMode.AtMost)
        {
          //Can't be bigger than...
          ResultHeight = Math.Min(DesiredSize.Height, HeightSize);
        }
        else
        {
          //Be whatever you want
          ResultHeight = DesiredSize.Height;
        }
      }

      SetMeasuredDimension(ResultWidth, ResultHeight);
    }

    View AndroidLayoutElement.View
    {
      get { return this; }
    }
    AndroidLayoutSize AndroidLayoutElement.CalculateSize(AndroidLayoutSize MaxSize)
    {
      var Result = AndroidLayoutSize.Empty;

      if (ContentVisible)
      {
        if (ContentWidth >= 0 && ContentHeight >= 0)
        {
          Result = new AndroidLayoutSize(ContentWidth, ContentHeight);
        }
        else
        {
          var ContentSize = MaxSize;
          if (ContentWidth >= 0)
            ContentSize.Width = ContentWidth;
          else if (ContentMaximumWidth >= 0 && ContentMaximumWidth < ContentSize.Width)
            ContentSize.Width = ContentMaximumWidth;

          if (ContentHeight >= 0)
            ContentSize.Height = ContentHeight;
          else if (ContentMaximumHeight >= 0 && ContentMaximumHeight < ContentSize.Height)
            ContentSize.Height = ContentMaximumHeight;

          Result = ContentElement != null ? ContentElement.CalculateSize(ContentSize) : AndroidLayoutSize.Empty;

          if (ContentWidth >= 0)
          {
            Result.Width = ContentWidth;
          }
          else
          {
            if (ContentMinimumWidth >= 0)
              Result.Width = Math.Max(ContentMinimumWidth, Result.Width);

            if (ContentMaximumWidth >= 0)
              Result.Width = Math.Min(ContentMaximumWidth, Result.Width);
          }

          if (ContentHeight >= 0)
          {
            Result.Height = ContentHeight;
          }
          else
          {
            if (ContentMinimumHeight >= 0)
              Result.Height = Math.Max(ContentMinimumHeight, Result.Height);

            if (ContentMaximumHeight >= 0)
              Result.Height = Math.Min(ContentMaximumHeight, Result.Height);
          }
        }

        Result.Width += PaddingLeft + PaddingRight;
        Result.Height += PaddingTop + PaddingBottom;
      }

      return Result;
    }

    private AndroidLayoutElement ContentElement;
    private AndroidLayoutVertical ContentVertical;
    private AndroidLayoutHorizontal ContentHorizontal;
    private bool ContentVisible;
    private int ContentWidth;
    private int ContentHeight;
    private int ContentMinimumWidth;
    private int ContentMinimumHeight;
    private int ContentMaximumWidth;
    private int ContentMaximumHeight;
  }

  internal static class AndroidLayoutFoundation
  {
    public static void Arrange(this View AndroidView)
    {
      var AndroidCurrent = AndroidView;
      while (AndroidCurrent != null)
      {
        AndroidCurrent.RequestLayout();

        AndroidCurrent = AndroidCurrent.Parent as View;
      }
    }
  }
}