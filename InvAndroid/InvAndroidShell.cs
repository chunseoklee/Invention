/*! 121 !*/
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Inv.Support;

namespace Inv
{
  public abstract class AndroidActivity : Android.App.Activity, Android.Locations.ILocationListener
  {
    public abstract Inv.Application CreateApplication();

    protected override void OnCreate(Android.OS.Bundle bundle)
    {
      // check the required attribute parameters are set.
#if DEBUG
      var AndroidActivityAttribute = GetType().CustomAttributes.Find(A => A.AttributeType == typeof(Android.App.ActivityAttribute));

      var LabelParameter = AndroidActivityAttribute.NamedArguments.Find(A => A.MemberName == "Label");
      Debug.Assert(LabelParameter != null && LabelParameter.TypedValue.Value != null, "[Activity] attribute must include Label parameter");
      
      var MainLauncherParameter = AndroidActivityAttribute.NamedArguments.Find(A => A.MemberName == "MainLauncher");
      Debug.Assert(MainLauncherParameter != null && MainLauncherParameter.TypedValue.Value != null, "[Activity] attribute must include MainLauncher parameter");

      var ThemeParameter = AndroidActivityAttribute.NamedArguments.Find(A => A.MemberName == "Theme");
      Debug.Assert(ThemeParameter != null && ThemeParameter.TypedValue.Value != null, "[Activity] attribute must include Theme parameter");

      var IconParameter = AndroidActivityAttribute.NamedArguments.Find(A => A.MemberName == "Icon");
      Debug.Assert(IconParameter != null && IconParameter.TypedValue.Value != null, "[Activity] attribute must include Icon parameter");

      var ConfigurationChangesParameter = AndroidActivityAttribute.NamedArguments.Find(A => A.MemberName == "ConfigurationChanges");
      Debug.Assert(ConfigurationChangesParameter != null && (Android.Content.PM.ConfigChanges)(ConfigurationChangesParameter.TypedValue.Value) == (Android.Content.PM.ConfigChanges.Orientation | Android.Content.PM.ConfigChanges.ScreenSize), "[Activity] attribute must specify ConfigurationChanges parameter = Android.Content.PM.ConfigChanges.Orientation | Android.Content.PM.ConfigChanges.ScreenSize");
#endif

      // NOTE: any exceptions in the constructor will terminate the application without any trace.

      RequestWindowFeature(Android.Views.WindowFeatures.NoTitle);
      Window.AddFlags(Android.Views.WindowManagerFlags.Fullscreen | Android.Views.WindowManagerFlags.HardwareAccelerated);

      base.OnCreate(bundle);

      this.ContentViewList = new DistinctList<Android.Views.View>();

      this.Engine = new Inv.AndroidEngine(CreateApplication(), this);
      Engine.Start();

      Window.DecorView.SystemUiVisibilityChange += (Sender, Event) =>
      {
        if (((Android.Views.SystemUiFlags)Event.Visibility & Android.Views.SystemUiFlags.Fullscreen) == 0)
          Engine.Immersive();
      };

      // TODO: this is not re-entrant.
      AppDomain.CurrentDomain.UnhandledException += (Sender, Event) =>
      {
        var Exception = Event.ExceptionObject as Exception;
        if (Exception != null)
          Engine.InvApplication.HandleExceptionInvoke(Exception);
      };

      // TODO: this is not re-entrant.
      Android.Runtime.AndroidEnvironment.UnhandledExceptionRaiser += (Sender, Event) =>
      {
        var Exception = Event.Exception;
        if (Exception != null)
          Engine.InvApplication.HandleExceptionInvoke(Exception);
        Event.Handled = true;
      };

      this.AudioManager = (Android.Media.AudioManager)GetSystemService(Android.Content.Context.AudioService);
    }
    protected override void OnDestroy()
    {
      base.OnDestroy();

      Engine.Stop();
    }
    protected override void OnPause()
    {
      base.OnPause();

      Engine.Pause();
    }
    protected override void OnResume()
    {
      base.OnResume();

      Engine.Resume();
    }
    public override void OnLowMemory()
    {
      Engine.MemoryWarning();

      base.OnLowMemory();
    }
    public override bool OnKeyDown(Android.Views.Keycode Keycode, Android.Views.KeyEvent Event)
    {
      switch (Keycode)
      {
        case Android.Views.Keycode.VolumeUp:
          AudioManager.AdjustStreamVolume(Android.Media.Stream.Music, Android.Media.Adjust.Raise, Android.Media.VolumeNotificationFlags.ShowUi);
          return true;

        case Android.Views.Keycode.VolumeDown:
          AudioManager.AdjustStreamVolume(Android.Media.Stream.Music, Android.Media.Adjust.Lower, Android.Media.VolumeNotificationFlags.ShowUi);
          return true;

        default:
          break;
      }

      if (!IsTransitioning)
      {
        Engine.Guard(() =>
        {
          var InvSurface = Engine.InvApplication.Window.ActiveSurface;
          if (InvSurface != null)
          {
            var InvKey = Engine.TranslateKey(Keycode);
            if (InvKey != null)
            {
              InvSurface.KeystrokeInvoke(new Keystroke()
              {
                Key = InvKey.Value,
                Modifier = Engine.GetModifier()
              });
            }
          }
        });
      }

      return base.OnKeyDown(Keycode, Event);
    }
    public override void OnBackPressed()
    {
      Engine.Guard(() =>
      {
        var InvSurface = Engine.InvApplication.Window.ActiveSurface;

        if (InvSurface != null && InvSurface.GestureBackwardQuery())
        {
          InvSurface.GestureBackwardInvoke();
        }
        else if (Engine.InvApplication.ExitQueryInvoke())
        {
          // TODO: this returns to the splash activity which effectively ends this activity.
          base.OnBackPressed(); 
        }
      });
    }
    public override void OnConfigurationChanged(Android.Content.Res.Configuration newConfig)
    {
      base.OnConfigurationChanged(newConfig);

      Engine.Guard(() =>
      {
        Engine.Resize();

        var InvSurface = Engine.InvApplication.Window.ActiveSurface;
        if (InvSurface != null)
          InvSurface.ArrangeInvoke();
      });
    }
    public override void AddContentView(Android.Views.View view, Android.Views.ViewGroup.LayoutParams @params)
    {
      base.AddContentView(view, @params);

      ContentViewList.Add(view);
    }

    private bool LeftEdgeSwipe;
    private bool RightEdgeSwipe;

    public override bool DispatchTouchEvent(Android.Views.MotionEvent ev)
    {
      if (IsTransitioning)
      {
        LeftEdgeSwipe = false;
        RightEdgeSwipe = false;

        return true; // ignore interaction while transitioning.
      }
      else if (ev.Action == Android.Views.MotionEventActions.Down && ev.RawX <= 10)
      {
        LeftEdgeSwipe = true;
        RightEdgeSwipe = false;

        return true;
      }
      else if (ev.Action == Android.Views.MotionEventActions.Down && ev.RawX >= Engine.DisplayMetrics.WidthPixels - 10)
      {
        LeftEdgeSwipe = false;
        RightEdgeSwipe = true;

        return true;
      }
      else if (ev.Action == Android.Views.MotionEventActions.Move && (LeftEdgeSwipe || RightEdgeSwipe))
      {
        var InvSurface = Engine.InvApplication.Window.ActiveSurface;

        if (InvSurface != null)
        {
          if (LeftEdgeSwipe && ev.RawX >= 0)
          {
            if (ev.RawX >= 20)
            {
              InvSurface.GestureBackwardInvoke();
              LeftEdgeSwipe = false;
            }
          }
          else if (RightEdgeSwipe && ev.RawX <= Engine.DisplayMetrics.WidthPixels)
          {
            if (ev.RawX <= Engine.DisplayMetrics.WidthPixels - 20)
            {
              InvSurface.GestureForwardInvoke();
              RightEdgeSwipe = false;
            }
          }
          else
          {
            LeftEdgeSwipe = false;
            RightEdgeSwipe = false;
          }
        }
        else
        {
          LeftEdgeSwipe = false;
          RightEdgeSwipe = false;
        }

        return true;
      }
      else
      {
        LeftEdgeSwipe = false;
        RightEdgeSwipe = false;

        return base.DispatchTouchEvent(ev);
      }
    }
    public void OnLocationChanged(Android.Locations.Location location)
    {
      Engine.Guard(() => Engine.InvApplication.Location.ChangeInvoke(new Inv.Coordinate(location.Latitude, location.Longitude, location.Altitude)));
    }
    public void OnProviderDisabled(string provider)
    {
    }
    public void OnProviderEnabled(string provider)
    {
    }
    public void OnStatusChanged(string provider, Android.Locations.Availability status, Android.OS.Bundle extras)
    {
    }
    public void RemoveContentView(Android.Views.View view)
    {
      ((Android.Views.ViewGroup)view.Parent).RemoveView(view);

      ContentViewList.Remove(view);
    }
    public bool HasContentView(Android.Views.View view)
    {
      return ContentViewList.Contains(view);
    }
    public Android.Views.View GetContentView()
    {
      return ContentViewList.Count > 0 ? ContentViewList[0] : null;
    }

    internal bool IsTransitioning { get; set; }

    private Inv.AndroidEngine Engine;
    private Inv.DistinctList<Android.Views.View> ContentViewList;
    private Android.Media.AudioManager AudioManager;
  }

  internal sealed class AndroidPlatform : Inv.Platform
  {
    public AndroidPlatform(AndroidEngine Engine)
    {
      this.Engine = Engine;
      this.PersonalPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
      this.DownloadsPath = Android.OS.Environment.GetExternalStoragePublicDirectory(Android.OS.Environment.DirectoryDownloads).AbsolutePath;
      this.SoundPool = new Android.Media.SoundPool(20, Android.Media.Stream.Music, 0);
      this.SoundIdDictionary = new Dictionary<Inv.Sound, int>();
      this.CurrentProcess = System.Diagnostics.Process.GetCurrentProcess();
      this.Vault = new AndroidVault(Engine.AndroidActivity, Engine.InvApplication.Title ?? "Invention");
    }

    int Platform.ThreadAffinity()
    {
      return System.Threading.Thread.CurrentThread.ManagedThreadId;
    }
    string Platform.CalendarTimeZoneName()
    {
      return TimeZoneInfo.Local.DisplayName;
    }
    void Platform.CalendarShowPicker(CalendarPicker CalendarPicker)
    {
      // TODO: Android date/time picker.
      // http://developer.xamarin.com/guides/android/user_interface/date_picker/
    }
    bool Platform.EmailSendMessage(EmailMessage EmailMessage)
    {
      var EmailIntent = new Android.Content.Intent(Android.Content.Intent.ActionSendMultiple);
      //EmailIntent.SetType("text/plain");
      EmailIntent.SetType("message/rfc822");
      EmailIntent.PutExtra(Android.Content.Intent.ExtraEmail, EmailMessage.GetTos().Select(T => T.Address).ToArray());
      EmailIntent.PutExtra(Android.Content.Intent.ExtraSubject, EmailMessage.Subject);
      EmailIntent.PutExtra(Android.Content.Intent.ExtraText, EmailMessage.Body);

      var AttachmentUris = new List<Android.OS.IParcelable>();

      // NOTE: we have to copy all attachment files from the internal storage to external storage so the email app can actually access the file.
      foreach (var Attachment in EmailMessage.GetAttachments())
      {
        var AttachmentFile = Path.Combine(DownloadsPath, Attachment.Name);
        System.IO.File.Copy(SelectFilePath(Attachment.File), AttachmentFile, true);

        var JavaFile = new Java.IO.File(AttachmentFile);
        AttachmentUris.Add(Android.Net.Uri.FromFile(JavaFile));
      }

      if (AttachmentUris.Count > 0)
        EmailIntent.PutParcelableArrayListExtra(Android.Content.Intent.ExtraStream, AttachmentUris);

      if (!IsIntentSupported(EmailIntent))
        return false;

      Engine.AndroidActivity.StartActivity(EmailIntent);
      return true;
    }
    bool Platform.PhoneIsSupported
    {
      get { return IsIntentSupported(new Android.Content.Intent(Android.Content.Intent.ActionDial)); }
    }
    bool Platform.PhoneDial(string PhoneNumber)
    {
      var DialIntent = new Android.Content.Intent(Android.Content.Intent.ActionDial, Android.Net.Uri.Parse("tel:" + PhoneNumber));
      if (!IsIntentSupported(DialIntent))
        return false;

      Engine.AndroidActivity.StartActivity(DialIntent);
      return true;
    }
    bool Platform.PhoneSMS(string PhoneNumber)
    {
      var SmsIntent = new Android.Content.Intent(Android.Content.Intent.ActionSendto, Android.Net.Uri.Parse("smsto:" + PhoneNumber));
      if (!IsIntentSupported(SmsIntent))
        return false;

      Engine.AndroidActivity.StartActivity(SmsIntent);
      return true;
    }
    long Platform.DirectoryGetLengthFile(File File)
    {
      return new System.IO.FileInfo(SelectFilePath(File)).Length;
    }
    DateTime Platform.DirectoryGetLastWriteTimeUtcFile(File File)
    {
      return new System.IO.FileInfo(SelectFilePath(File)).LastWriteTimeUtc;
    }
    void Platform.DirectorySetLastWriteTimeUtcFile(File File, DateTime Timestamp)
    {
      System.IO.File.SetLastWriteTimeUtc(SelectFilePath(File), Timestamp);
    }
    Stream Platform.DirectoryCreateFile(File File)
    {
      return new FileStream(SelectFilePath(File), FileMode.Create, FileAccess.Write, FileShare.Read);

    }
    Stream Platform.DirectoryAppendFile(File File)
    {
      return new System.IO.FileStream(SelectFilePath(File), FileMode.Append, FileAccess.Write, FileShare.Read);
    }
    Stream Platform.DirectoryOpenFile(File File)
    {
      return new System.IO.FileStream(SelectFilePath(File), FileMode.Open, FileAccess.Read, FileShare.Read);
    }
    bool Platform.DirectoryExistsFile(File File)
    {
      return System.IO.File.Exists(SelectFilePath(File));
    }
    void Platform.DirectoryDeleteFile(File File)
    {
      System.IO.File.Delete(SelectFilePath(File));
    }
    void Platform.DirectoryCopyFile(File SourceFile, File TargetFile)
    {
      System.IO.File.Copy(SelectFilePath(SourceFile), SelectFilePath(TargetFile));
    }
    void Platform.DirectoryMoveFile(File SourceFile, File TargetFile)
    {
      System.IO.File.Move(SelectFilePath(SourceFile), SelectFilePath(TargetFile));
    }
    IEnumerable<File> Platform.DirectoryGetFolderFiles(Folder Folder, string FileMask)
    {
      return new DirectoryInfo(SelectFolderPath(Folder)).GetFiles(FileMask).Select(F => Folder.NewFile(F.Name));
    }
    Stream Platform.DirectoryOpenAsset(Asset Asset)
    {
      return Engine.AndroidActivity.Assets.Open(Asset.Name);
    }
    bool Platform.DirectoryExistsAsset(Asset Asset)
    {
      return Engine.AndroidActivity.Assets.List("").Contains(Asset.Name);
    }
    bool Platform.LocationIsSupported
    {
      get { return Engine.AndroidLocationProvider != null; }
    }
    void Platform.LocationLookup(LocationLookup LocationLookup)
    {
      var AndroidGeocoder = new Android.Locations.Geocoder(Engine.AndroidActivity);
      AndroidGeocoder.GetFromLocationAsync(LocationLookup.Coordinate.Latitude, LocationLookup.Coordinate.Longitude, 10).ContinueWith(Task =>
      {
        Debug.WriteLine(Task.Result.ToArray().Select(R => R.ToString()).AsSeparatedText(Environment.NewLine));

        LocationLookup.SetPlacemarks(Task.Result.Select(P => new Inv.Placemark
        (
          Name: P.Premises ?? P.GetAddressLine(0),
          Locality: P.Locality,
          SubLocality: P.SubLocality,
          PostalCode: P.PostalCode,
          AdministrativeArea: P.AdminArea,
          SubAdministrativeArea: P.SubAdminArea,
          CountryName: P.CountryName,
          CountryCode : P.CountryCode,
          Latitude: P.HasLatitude ? P.Latitude : LocationLookup.Coordinate.Latitude,
          Longitude: P.HasLongitude ? P.Longitude : LocationLookup.Coordinate.Longitude
        )));
      });
    }
    void Platform.AudioPlaySound(Sound Sound, float Volume)
    {
      // NOTE: android will inconsistently play a sound with a volume greater than 1.0F.
      //       this requirement is asserted in the Inv API, but this is code is to ensure sound is played in RELEASE, despite a mistake with the volume.
      if (Volume > 1.0F)
        Volume = 1.0F;
      else if (Volume < 0.0F)
        Volume = 0.0F;

      var SoundId = SoundIdDictionary.GetOrAdd(Sound, S =>
      {
        var Buffer = S.GetBuffer();

        var FilePath = Path.Combine(PersonalPath, SoundIdDictionary.Count + ".mp3");

        using (var FileStream = new FileStream(FilePath, FileMode.Create))
          FileStream.Write(Buffer, 0, Buffer.Length);

        var Result = SoundPool.Load(FilePath, 1);

        return Result;
      });

      // NOTE: this is required because the first time a sound is loaded, it doesn't work straight away.
      int StreamID;
      var Retry = 0;
      do
      {
        StreamID = SoundPool.Play(SoundId, Volume, Volume, 1, 0, 1.0F);
        if (StreamID == 0)
          System.Threading.Thread.Sleep(10);
      }
      while (StreamID == 0 && Retry++ < 100);

      if (StreamID == 0)
        Debug.WriteLine("Play sound failed");
    }
    void Platform.WindowAsynchronise(Action Action)
    {
      Engine.AndroidActivity.RunOnUiThread(Action);
    }
    Modifier Platform.WindowGetModifier()
    {
      return Engine.GetModifier();
    }
    long Platform.ProcessMemoryBytes()
    {
      // NOTE: CurrentProcess.Refresh() doesn't seem to be required here on Android.
      return CurrentProcess.PrivateMemorySize64;
    }
    void Platform.WebSocketConnect(WebSocket Socket)
    {
      var TcpClient = new Inv.Tcp.Client(Socket.Host, Socket.Port);
      TcpClient.Connect();

      Socket.Node = TcpClient;
      Socket.SetStreams(TcpClient.Stream, TcpClient.Stream);
    }
    void Platform.WebSocketDisconnect(WebSocket Socket)
    {
      var TcpClient = (Inv.Tcp.Client)Socket.Node;
      if (TcpClient != null)
      {
        Socket.Node = null;
        Socket.SetStreams(null, null);

        TcpClient.Disconnect();
      }
    }
    void Platform.WebLaunchUri(Uri Uri)
    {
      var BrowserIntent = new Android.Content.Intent(Android.Content.Intent.ActionView, Android.Net.Uri.Parse(Uri.AbsoluteUri));
      Engine.AndroidActivity.StartActivity(BrowserIntent);
    }
    void Platform.MarketBrowse(string AppleiTunesID, string GooglePlayID, string WindowsStoreID)
    {
      var MarketIntent = new Android.Content.Intent(Android.Content.Intent.ActionView, Android.Net.Uri.Parse("market://details?id=" + GooglePlayID));
      MarketIntent.AddFlags(Android.Content.ActivityFlags.NoHistory | Android.Content.ActivityFlags.ClearWhenTaskReset | Android.Content.ActivityFlags.MultipleTask | Android.Content.ActivityFlags.NewTask);
      Engine.AndroidActivity.StartActivity(MarketIntent);
    }
    void Platform.VaultLoadSecret(Secret Secret)
    {
      Vault.Load(Secret);
    }
    void Platform.VaultSaveSecret(Secret Secret)
    {
      Vault.Save(Secret);
    }
    void Platform.VaultDeleteSecret(Secret Secret)
    {
      Vault.Delete(Secret);
    }

    private bool IsIntentSupported(Android.Content.Intent AndroidIntent)
    {
      var AvailableActivities = Engine.AndroidActivity.PackageManager.QueryIntentActivities(AndroidIntent, Android.Content.PM.PackageInfoFlags.MatchDefaultOnly);

      return AvailableActivities != null && AvailableActivities.Count > 0;
    }
    private string SelectFilePath(File File)
    {
      return Path.Combine(SelectFolderPath(File.Folder), File.Name);
    }
    private string SelectFolderPath(Folder Folder)
    {
      string Result;

      if (Folder.Name != null)
        Result = System.IO.Path.Combine(PersonalPath, Folder.Name);
      else
        Result = PersonalPath;

      System.IO.Directory.CreateDirectory(Result);

      return Result;
    }

    private AndroidEngine Engine;
    private AndroidVault Vault;
    private string PersonalPath;
    private string DownloadsPath;
    private Android.Media.SoundPool SoundPool;
    private Dictionary<Inv.Sound, int> SoundIdDictionary;
    private Process CurrentProcess;
  }

  internal sealed class AndroidFrameCallback : Android.Views.View, Android.Views.Choreographer.IFrameCallback
  {
    public AndroidFrameCallback(Android.Content.Context Context, Action Action)
      : base(Context)
    {
      this.Action = Action;
    }

    void Android.Views.Choreographer.IFrameCallback.DoFrame(long frameTimeNanos)
    {
      Action();
    }

    private Action Action;
  }

  internal sealed class AndroidEngine 
  {
    public AndroidEngine(Inv.Application InvApplication, AndroidActivity AndroidActivity)
    {
      this.InvApplication = InvApplication;
      this.AndroidActivity = AndroidActivity;
      this.ImageBitmapDictionary = new Dictionary<Inv.Image, Android.Graphics.Bitmap>();
      this.ColourDictionary = new Dictionary<Colour, Android.Graphics.Color>();
      this.ColourPaintDictionary = new Dictionary<Inv.Colour, Android.Graphics.Paint>();
      this.StrokePaintDictionary = new Dictionary<StrokePaintKey, Android.Graphics.Paint>();
      this.ImagePaintDictionary = new Dictionary<ImagePaintKey, Android.Graphics.Paint>();
      this.TextPaintDictionary = new Dictionary<TextPaintKey, Android.Graphics.Paint>();
      this.LookupStrokePaintKey = new StrokePaintKey();
      this.LookupImagePaintKey = new ImagePaintKey();
      this.LookupTextPaintKey = new TextPaintKey();

      this.RouteDictionary = new Dictionary<Type, Func<Inv.Panel, AndroidNode>>()
      {
        { typeof(Inv.Button), TranslateButton },
        { typeof(Inv.Canvas), TranslateCanvas },
        { typeof(Inv.Dock), TranslateDock },
        { typeof(Inv.Edit), TranslateEdit },
        { typeof(Inv.Graphic), TranslateGraphic },
        { typeof(Inv.Label), TranslateLabel },
        { typeof(Inv.Memo), TranslateMemo },
        { typeof(Inv.Overlay), TranslateOverlay },
        { typeof(Inv.Render), TranslateRender },
        { typeof(Inv.Scroll), TranslateScroll },
        { typeof(Inv.Frame), TranslateFrame },
        { typeof(Inv.Stack), TranslateStack },
        { typeof(Inv.Table), TranslateTable },
      };

      #region Key mapping.
      this.KeyDictionary = new Dictionary<Android.Views.Keycode, Inv.Key>()
      {
        { Android.Views.Keycode.Num0, Key.n0 },
        { Android.Views.Keycode.Num1, Key.n1 },
        { Android.Views.Keycode.Num2, Key.n2 },
        { Android.Views.Keycode.Num3, Key.n3 },
        { Android.Views.Keycode.Num4, Key.n4 },
        { Android.Views.Keycode.Num5, Key.n5 },
        { Android.Views.Keycode.Num6, Key.n6 },
        { Android.Views.Keycode.Num7, Key.n7 },
        { Android.Views.Keycode.Num8, Key.n8 },
        { Android.Views.Keycode.Num9, Key.n9 },

        { Android.Views.Keycode.A, Key.A },
        { Android.Views.Keycode.B, Key.B },
        { Android.Views.Keycode.C, Key.C },
        { Android.Views.Keycode.D, Key.D },
        { Android.Views.Keycode.E, Key.E },
        { Android.Views.Keycode.F, Key.F },
        { Android.Views.Keycode.G, Key.G },
        { Android.Views.Keycode.H, Key.H },
        { Android.Views.Keycode.I, Key.I },
        { Android.Views.Keycode.J, Key.J },
        { Android.Views.Keycode.K, Key.K },
        { Android.Views.Keycode.L, Key.L },
        { Android.Views.Keycode.M, Key.M },
        { Android.Views.Keycode.N, Key.N },
        { Android.Views.Keycode.O, Key.O },
        { Android.Views.Keycode.P, Key.P },
        { Android.Views.Keycode.Q, Key.Q },
        { Android.Views.Keycode.R, Key.R },
        { Android.Views.Keycode.S, Key.S },
        { Android.Views.Keycode.T, Key.T },
        { Android.Views.Keycode.U, Key.U },
        { Android.Views.Keycode.V, Key.V },
        { Android.Views.Keycode.W, Key.W },
        { Android.Views.Keycode.X, Key.X },
        { Android.Views.Keycode.Y, Key.Y },
        { Android.Views.Keycode.Z, Key.Z },
        { Android.Views.Keycode.F1, Key.F1 },
        { Android.Views.Keycode.F2, Key.F2 },
        { Android.Views.Keycode.F3, Key.F3 },
        { Android.Views.Keycode.F4, Key.F4 },
        { Android.Views.Keycode.F5, Key.F5 },
        { Android.Views.Keycode.F6, Key.F6 },
        { Android.Views.Keycode.F7, Key.F7 },
        { Android.Views.Keycode.F8, Key.F8 },
        { Android.Views.Keycode.F9, Key.F9 },
        { Android.Views.Keycode.F10, Key.F10 },
        { Android.Views.Keycode.F11, Key.F11 },
        { Android.Views.Keycode.F12, Key.F12 },
        { Android.Views.Keycode.Escape, Key.Escape },
        { Android.Views.Keycode.Enter, Key.Enter },
        { Android.Views.Keycode.Tab, Key.Tab },
        { Android.Views.Keycode.Space, Key.Space },
        { Android.Views.Keycode.Period, Key.Period },
        { Android.Views.Keycode.Comma, Key.Comma },
        { Android.Views.Keycode.Grave, Key.BackQuote },
        { Android.Views.Keycode.DpadUp, Key.Up },
        { Android.Views.Keycode.DpadDown, Key.Down },
        { Android.Views.Keycode.DpadLeft, Key.Left },
        { Android.Views.Keycode.DpadRight, Key.Right },
        { Android.Views.Keycode.MoveHome, Key.Home },
        { Android.Views.Keycode.MoveEnd, Key.End },
        { Android.Views.Keycode.PageUp, Key.PageUp },
        { Android.Views.Keycode.PageDown, Key.PageDown },
        { Android.Views.Keycode.Clear, Key.Clear },
        { Android.Views.Keycode.Insert, Key.Insert },
        { Android.Views.Keycode.Del, Key.Delete },
        { Android.Views.Keycode.Backslash, Key.Backslash },
        { Android.Views.Keycode.Slash, Key.Slash },
      };
      #endregion

      InvApplication.SetPlatform(new AndroidPlatform(this));

      InvApplication.Device.Name = Environment.MachineName;
      InvApplication.Device.Model = GetDeviceModel();
      InvApplication.Device.System = "Android " + Android.OS.Build.VERSION.Release + " (" + Android.OS.Build.VERSION.Sdk + ")";

      this.Choreographer = Android.Views.Choreographer.Instance;
      this.FrameCallback = new AndroidFrameCallback(AndroidActivity, FrameLoop);

      this.AndroidSdkVersion = (int)Android.OS.Build.VERSION.SdkInt;
#if DEBUG
      //this.AndroidSdkVersion = 19; // for compatibility testing.
#endif

      this.AndroidSurfaceLayoutParams = new Android.Widget.FrameLayout.LayoutParams(Android.Widget.FrameLayout.LayoutParams.MatchParent, Android.Widget.FrameLayout.LayoutParams.MatchParent);

      this.AndroidFontArray = new Inv.EnumArray<FontWeight, Android.Graphics.Typeface>();
      AndroidFontArray.Fill(W => Android.Graphics.Typeface.Create(W == FontWeight.Regular ? "sans-serif" : "sans-serif-" + W.ToString().ToLower(), Android.Graphics.TypefaceStyle.Normal));

      this.InputManager = (Android.Views.InputMethods.InputMethodManager)AndroidActivity.GetSystemService(Android.Content.Context.InputMethodService);
    }

    public Inv.Application InvApplication { get; private set; }

    public void Start()
    {
      try
      {
        Resize();

        InvApplication.StartInvoke();

        AndroidActivity.Title = InvApplication.Title;
      }
      catch (Exception Exception)
      {
        InvApplication.HandleExceptionInvoke(Exception);

        // NOTE: can't let the application start if the StartEvent failed (it will hang on a black screen) so have the application crash instead.
        throw Exception;
      }

      FrameLoop();
    }
    public void Stop()
    {
      Guard(() => 
      {
        InvApplication.StopInvoke();
      });
    }
    public void Resume()
    {
      Immersive();

      if (IsProcessPaused)
      {
        this.IsProcessPaused = false;

        Guard(() =>
        {
          if (InvApplication.Location.IsRequired)
          {
            this.AndroidLocationManager = AndroidActivity.GetSystemService(Android.Content.Context.LocationService) as Android.Locations.LocationManager;
            
            var LocationCriteria = new Android.Locations.Criteria()
            {
              Accuracy = Android.Locations.Accuracy.Fine,
              PowerRequirement = Android.Locations.Power.NoRequirement
            };

            AndroidLocationProvider = AndroidLocationManager != null ? AndroidLocationManager.GetBestProvider(LocationCriteria, true) : null;

            if (AndroidLocationProvider != null)
            {
              // 2000L = the minimum time between updates (in seconds), 
              // 1.0F = the minimum distance the user needs to move to generate an update (in meters),

              AndroidLocationManager.RequestLocationUpdates(AndroidLocationProvider, 2000L, 1.0F, AndroidActivity);
            }
          }
        });

        Guard(() =>
        {
          InvApplication.ResumeInvoke();
        });

        FrameLoop();
      }
    }
    public void Pause()
    {
      if (!IsProcessPaused)
      {
        this.IsProcessPaused = true;

        Guard(() =>
        {
          InvApplication.SuspendInvoke();
        });

        Guard(() =>
        {
          if (AndroidLocationManager != null)
          {
            AndroidLocationManager.RemoveUpdates(AndroidActivity);
            AndroidLocationManager = null;
          }
        });
      }
    }

    internal AndroidActivity AndroidActivity { get; private set; }
    internal string AndroidLocationProvider { get; set; }
    internal Android.Util.DisplayMetrics DisplayMetrics { get; private set; }

    internal Modifier GetModifier()
    {
      return new Modifier();
    }
    internal Inv.Key? TranslateKey(Android.Views.Keycode AndroidKey)
    {
      Inv.Key Result;
      if (KeyDictionary.TryGetValue(AndroidKey, out Result))
        return Result;
      else
        return null;
    }
    internal void Resize()
    {
      var Display = AndroidActivity.WindowManager.DefaultDisplay;

      this.DisplayMetrics = new Android.Util.DisplayMetrics();
      Display.GetRealMetrics(DisplayMetrics);
      this.ConvertPtToPxFactor = DisplayMetrics.Density;

      InvApplication.Window.Width = (int)(DisplayMetrics.WidthPixels / ConvertPtToPxFactor);
      InvApplication.Window.Height = (int)(DisplayMetrics.HeightPixels / ConvertPtToPxFactor);
    }
    internal void Immersive()
    {
      // NOTE: the typecast a deliberate workaround, we need to use a different enum and cast it to get the desired behaviour.
      // NOTE: needs to happen 'OnResume' so it re-applies when your return from another activity.
      var UiFlags = Android.Views.SystemUiFlags.Fullscreen | Android.Views.SystemUiFlags.HideNavigation | Android.Views.SystemUiFlags.ImmersiveSticky;// | Android.Views.SystemUiFlags.LayoutStable | Android.Views.SystemUiFlags.LayoutHideNavigation | Android.Views.SystemUiFlags.HideNavigation;
      AndroidActivity.Window.DecorView.SystemUiVisibility = (Android.Views.StatusBarVisibility)UiFlags;
    }
    internal void MemoryWarning()
    {
      // dispose everything cached.
#if DEBUG
      foreach (var Bitmap in ImageBitmapDictionary.Values)
        Bitmap.Dispose();
      ImageBitmapDictionary.Clear();

      ColourDictionary.Clear();
      ColourPaintDictionary.Clear();
      StrokePaintDictionary.Clear();
      ImagePaintDictionary.Clear();
      TextPaintDictionary.Clear();
#endif
    }
    internal void Guard(Action Action)
    {
      try
      {
        Action();
      }
      catch (Exception Exception)
      {
        InvApplication.HandleExceptionInvoke(Exception);
      }
    }

    private void FrameLoop()
    {
      Process();

      if (!InvApplication.IsExit && !IsProcessPaused)
        Choreographer.PostFrameCallback(FrameCallback);
    }
    private void Process()
    {
      if (InvApplication.IsExit)
      {
        AndroidActivity.Finish();
        // NOTE: this won't actually end the application, that's the job of the Android OS (apparently).

        // TODO: there doesn't appear to be a way to actually terminate the process in code.
        //Android.OS.Process.KillProcess(Android.OS.Process.MyPid());
        //System.Environment.Exit(0);
      }
      else
      {
        Guard(() =>
        {
          var InvWindow = InvApplication.Window;

          if (InvWindow.ActiveTimerSet.Count > 0)
          {
            foreach (var InvTimer in InvWindow.ActiveTimerSet)
            {
              var AndroidTimer = AccessTimer(InvTimer, S =>
              {
                var Result = new System.Timers.Timer();
                Result.Elapsed += (Sender, Event) => InvWindow.Application.Platform.WindowAsynchronise(() => Guard(() => InvTimer.IntervalInvoke()));
                return Result;
              });


              if (AndroidTimer.Interval != InvTimer.IntervalTime.TotalMilliseconds)
                AndroidTimer.Interval = InvTimer.IntervalTime.TotalMilliseconds;

              if (InvTimer.IsEnabled && !AndroidTimer.Enabled)
                AndroidTimer.Start();
              else if (!InvTimer.IsEnabled && AndroidTimer.Enabled)
                AndroidTimer.Stop();
            }

            InvWindow.ActiveTimerSet.RemoveWhere(T => !T.IsEnabled);
          }

          var InvSurface = InvWindow.ActiveSurface;

          if (InvSurface != null)
          {
            var AndroidSurface = AccessSurface(InvSurface, S =>
            {
              var Result = new AndroidSurfaceView(AndroidActivity);
              return Result;
            });

            if (!AndroidActivity.HasContentView(AndroidSurface) && InvWindow.ActiveTransition != null)
              InvSurface.ArrangeInvoke();

            ProcessTransition(AndroidSurface);

            InvSurface.ComposeInvoke();

            if (InvSurface.Render())
            {
              AndroidSurface.SetBackgroundColor(TranslateColor(InvSurface.Background.Colour ?? InvWindow.Background.Colour ?? Inv.Colour.Black));

              var AndroidPanel = TranslatePanel(InvSurface.Content);
              AndroidSurface.SetContentView(AndroidPanel);
            }

            InvSurface.ProcessChanges(P => TranslatePanel(P));

            if (InvSurface.Focus != null)
            {
              var AndroidFocus = InvSurface.Focus.Node as AndroidNode;

              InvSurface.Focus = null;

              if (AndroidFocus != null && AndroidFocus.PanelView.RequestFocus())
                AndroidFocus.PanelView.PostDelayed(() => InputManager.ShowSoftInput(AndroidFocus.PanelView, Android.Views.InputMethods.ShowFlags.Implicit), 0);
            }
          }

          if (InvWindow.Render())
          {
            // InvWindow.DefaultFont is used as a fallback in each label/edit/memo.
          }

          if (InvSurface != null)
            ProcessAnimation(InvSurface);

          InvWindow.DisplayRate.Calculate();
        });
      }
    }
    private void ProcessTransition(AndroidSurfaceView AndroidSurface)
    {
      var InvWindow = InvApplication.Window;
      var InvTransition = InvWindow.ActiveTransition;
      if (InvTransition == null)
      {
      }
      else if (!AndroidActivity.IsTransitioning)
      {
        var AndroidContent = AndroidActivity.GetContentView();

        if (AndroidSurface == AndroidContent)
        {
          Debug.WriteLine("Transition to self");
        }
        else
        {
          var AnimateDuration = (long)InvTransition.Duration.TotalMilliseconds;

          switch (InvTransition.Animation)
          {
            case TransitionAnimation.None:
              if (AndroidContent != null)
                AndroidActivity.RemoveContentView(AndroidContent);

              AndroidActivity.AddContentView(AndroidSurface, AndroidSurfaceLayoutParams);
              break;

            case TransitionAnimation.Fade:
              AndroidActivity.IsTransitioning = true;

              if (AndroidContent != null)
              {
                var HalfTime = AnimateDuration / 2;

                var FadeOut = new Android.Views.Animations.AlphaAnimation(1, 0);
                FadeOut.Interpolator = new Android.Views.Animations.AccelerateDecelerateInterpolator();
                FadeOut.Duration = HalfTime;
                FadeOut.AnimationEnd += (Sender, Event) =>
                {
                  AndroidActivity.RemoveContentView(AndroidContent);
                  AndroidContent.Alpha = 1.0F;
                  AndroidActivity.AddContentView(AndroidSurface, AndroidSurfaceLayoutParams);

                  var FadeIn = new Android.Views.Animations.AlphaAnimation(0, 1);
                  FadeIn.Interpolator = new Android.Views.Animations.AccelerateDecelerateInterpolator();
                  FadeIn.Duration = HalfTime;
                  FadeIn.AnimationEnd += (S, E) => AndroidActivity.IsTransitioning = false;
                  AndroidSurface.StartAnimation(FadeIn);
                };
                AndroidContent.StartAnimation(FadeOut);
              }
              else
              {
                AndroidActivity.AddContentView(AndroidSurface, AndroidSurfaceLayoutParams);

                var FadeIn = new Android.Views.Animations.AlphaAnimation(0, 1);
                FadeIn.Interpolator = new Android.Views.Animations.AccelerateDecelerateInterpolator();
                FadeIn.Duration = AnimateDuration;
                FadeIn.AnimationEnd += (Sender, Event) => AndroidActivity.IsTransitioning = false;
                AndroidSurface.StartAnimation(FadeIn);
              }
              break;

            case TransitionAnimation.CarouselNext:
              AndroidActivity.IsTransitioning = true;

              if (AndroidContent != null)
              {
                var NextSlideOut = new Android.Views.Animations.TranslateAnimation(0.0F, -DisplayMetrics.WidthPixels, 0.0F, 0.0F);
                NextSlideOut.Duration = AnimateDuration;
                NextSlideOut.AnimationEnd += (Sender, Event) => AndroidActivity.RemoveContentView(AndroidContent);
                AndroidContent.StartAnimation(NextSlideOut);
              }

              AndroidActivity.AddContentView(AndroidSurface, AndroidSurfaceLayoutParams);

              var NextSlideIn = new Android.Views.Animations.TranslateAnimation(DisplayMetrics.WidthPixels, 0.00F, 0.0F, 0.0F);
              NextSlideIn.Duration = AnimateDuration;
              NextSlideIn.AnimationEnd += (Sender, Event) => AndroidActivity.IsTransitioning = false;
              AndroidSurface.StartAnimation(NextSlideIn);
              break;

            case TransitionAnimation.CarouselBack:
              AndroidActivity.IsTransitioning = true;

              if (AndroidContent != null)
              {
                var BackSlideOut = new Android.Views.Animations.TranslateAnimation(0.00F, DisplayMetrics.WidthPixels, 0.0F, 0.0F);
                BackSlideOut.Duration = AnimateDuration;
                BackSlideOut.AnimationEnd += (Sender, Event) => AndroidActivity.RemoveContentView(AndroidContent);
                AndroidContent.StartAnimation(BackSlideOut);
              }

              AndroidActivity.AddContentView(AndroidSurface, AndroidSurfaceLayoutParams);

              var BackSlideIn = new Android.Views.Animations.TranslateAnimation(-DisplayMetrics.WidthPixels, 0.00F, 0.0F, 0.0F);
              BackSlideIn.Duration = AnimateDuration;
              BackSlideIn.AnimationEnd += (Sender, Event) => AndroidActivity.IsTransitioning = false;
              AndroidSurface.StartAnimation(BackSlideIn);
              break;

            default:
              throw new Exception("TransitionAnimation not handled: " + InvTransition.Animation);
          }
        }

        InvWindow.ActiveTransition = null;
      }
    }
    private void ProcessAnimation(Inv.Surface InvSurfaceActive)
    {
      if (InvSurfaceActive.StopAnimationSet.Count > 0)
      {
        foreach (var InvAnimation in InvSurfaceActive.StopAnimationSet)
        {
          if (InvAnimation.Node != null)
          {
            var AndroidAnimationGroupList = (Inv.DistinctList<AnimationGroup>)InvAnimation.Node;
            InvAnimation.Node = null;

            foreach (var AndroidAnimationGroup in AndroidAnimationGroupList)
            {
              AndroidAnimationGroup.AnimationSet.Cancel();
              AndroidAnimationGroup.AndroidPanel.ClearAnimation();
            }
          }
        }
        InvSurfaceActive.StopAnimationSet.Clear();
      }
      else if (InvSurfaceActive.StartAnimationSet.Count > 0)
      {
        foreach (var InvAnimation in InvSurfaceActive.StartAnimationSet)
        {
          var AndroidAnimationGroupList = new Inv.DistinctList<AnimationGroup>();
          InvAnimation.Node = AndroidAnimationGroupList;

          var InvLastTarget = InvAnimation.GetTargets().LastOrDefault();

          foreach (var InvTarget in InvAnimation.GetTargets())
          {
            var InvPanel = InvTarget.Panel;
            var AndroidPanel = TranslatePanel(InvPanel);

            // TODO: switch to ValueAnimator?

            // NOTE: AnimationSet does not support animations on different views.
            var AndroidAnimationSet = new Android.Views.Animations.AnimationSet(false);

            var InvLastCommand = InvTarget.GetCommands().LastOrDefault();
            
            foreach (var InvCommand in InvTarget.GetCommands())
            {
              var InvOpacityCommand = (Inv.AnimationOpacityCommand)InvCommand;

              var AndroidAlphaAnimation = new Android.Views.Animations.AlphaAnimation(InvOpacityCommand.From, InvOpacityCommand.To);

              if (InvOpacityCommand.Offset != null)
                AndroidAlphaAnimation.StartOffset = (long)InvOpacityCommand.Offset.Value.TotalMilliseconds;

              AndroidAlphaAnimation.Interpolator = new Android.Views.Animations.AccelerateDecelerateInterpolator();
              AndroidAlphaAnimation.Duration = (long)InvOpacityCommand.Duration.TotalMilliseconds;
              AndroidAlphaAnimation.AnimationStart += (S, E) =>
              {
                if (InvAnimation.IsActive)
                {
                  if (InvOpacityCommand.From <= InvOpacityCommand.To)
                  {
                    AndroidPanel.Alpha = InvOpacityCommand.To;
                    InvPanel.Opacity.BypassSet(InvOpacityCommand.To);
                  }
                  else
                  {
                    AndroidPanel.Alpha = InvOpacityCommand.From;
                    InvPanel.Opacity.BypassSet(InvOpacityCommand.From);
                  }
                }
              };
              AndroidAlphaAnimation.AnimationEnd += (S, E) =>
              {
                if (InvAnimation.IsActive)
                {
                  AndroidPanel.Alpha = InvOpacityCommand.To;
                  InvPanel.Opacity.BypassSet(InvOpacityCommand.To);

                  InvCommand.Complete();

                  if (InvTarget == InvLastTarget && InvCommand == InvLastCommand)
                    InvAnimation.Complete();
                }
              };

              AndroidAnimationSet.AddAnimation(AndroidAlphaAnimation);
            }

            AndroidPanel.StartAnimation(AndroidAnimationSet);

            var AndroidAnimationGroup = new AnimationGroup(AndroidPanel, AndroidAnimationSet);
            AndroidAnimationGroupList.Add(AndroidAnimationGroup);
          }
        }
        InvSurfaceActive.StartAnimationSet.Clear();
      }
    }

    private AndroidContainerLayout TranslatePanel(Inv.Panel InvPanel)
    {
      if (InvPanel == null)
        return null;
      else
        return RouteDictionary[InvPanel.GetType()](InvPanel).LayoutView;
    }
    private AndroidNode TranslateButton(Inv.Panel InvPanel)
    {
      var InvButton = (Inv.Button)InvPanel;

      var AndroidNode = AccessPanel(InvPanel, () =>
      {
        var AndroidButton = new AndroidButtonView(AndroidActivity);
        AndroidButton.Click += (Sender, Event) => Guard(() => InvButton.SingleTapInvoke());
        AndroidButton.LongClick += (Sender, Event) => Guard(() => InvButton.ContextTapInvoke());
        AndroidButton.Touch += (Sender, Event) =>
        {
          switch (Event.Event.Action)
          {
            case Android.Views.MotionEventActions.Down:
            case Android.Views.MotionEventActions.PointerDown:
              TranslateBackground(TranslatePressColour(InvButton.Background.Colour), AndroidButton.Background);
              break;

            case Android.Views.MotionEventActions.Up:
            case Android.Views.MotionEventActions.PointerUp:
              if (AndroidButton.Hovered)
                TranslateBackground(TranslateHoverColour(InvButton.Background.Colour), AndroidButton.Background);
              else
                TranslateBackground(InvButton.Background.Colour, AndroidButton.Background);
              break;

            case Android.Views.MotionEventActions.Cancel:
              TranslateBackground(InvButton.Background.Colour, AndroidButton.Background);
              break;

            default:
              break;
          }

          Event.Handled = false;
        };
        AndroidButton.Hover += (Sender, Event) =>
        {
          switch (Event.Event.Action)
          {
            case Android.Views.MotionEventActions.HoverEnter:
              TranslateBackground(TranslateHoverColour(InvButton.Background.Colour), AndroidButton.Background);
              break;

            case Android.Views.MotionEventActions.HoverExit:
              TranslateBackground(InvButton.Background.Colour, AndroidButton.Background);
              break;

            default:
              break;
          }
        };
        return AndroidButton;
      });

      RenderPanel(InvButton, AndroidNode, () =>
      {
        var AndroidLayout = AndroidNode.LayoutView;
        var AndroidButton = AndroidNode.PanelView;

        AndroidLayout.Alpha = InvButton.IsEnabled ? 1.0F : 0.5F;

        AndroidButton.Enabled = InvButton.IsEnabled;
        AndroidButton.Focusable = InvButton.IsFocusable;
        TranslateLayout(InvButton, AndroidLayout, AndroidButton);
        TranslateBackground(InvButton, AndroidButton.Background);

        if (InvButton.ContentSingleton.Render())
        {
          AndroidButton.SetContentView(null); // detach previous content in case it has moved.
          var AndroidContent = TranslatePanel(InvButton.Content);
          AndroidButton.SetContentView(AndroidContent);
        }
      });

      return AndroidNode;
    }
    private AndroidNode TranslateCanvas(Inv.Panel InvPanel)
    {
      var InvCanvas = (Inv.Canvas)InvPanel;

      var AndroidNode = AccessPanel(InvCanvas, () =>
      {
        var AndroidCanvas = new AndroidCanvasView(AndroidActivity);
        return AndroidCanvas;
      });

      RenderPanel(InvCanvas, AndroidNode, () =>
      {
        var AndroidLayout = AndroidNode.LayoutView;
        var AndroidCanvas = AndroidNode.PanelView;

        TranslateLayout(InvCanvas, AndroidLayout, AndroidCanvas);
        TranslateBackground(InvCanvas, AndroidCanvas.Background);

        if (InvCanvas.PieceCollection.Render())
        {
          AndroidCanvas.RemoveAllViews();

          foreach (var InvElement in InvCanvas.PieceCollection)
          {
            var AndroidPanel = TranslatePanel(InvElement.Panel);

            var ElementLP = new AndroidCanvasView.LayoutParams(AndroidCanvasView.LayoutParams.WrapContent, AndroidCanvasView.LayoutParams.WrapContent);
            ElementLP.LeftMargin = PtToPx(InvElement.Rect.Left);
            ElementLP.TopMargin = PtToPx(InvElement.Rect.Top);
            ElementLP.Width = PtToPx(InvElement.Rect.Width);
            ElementLP.Height = PtToPx(InvElement.Rect.Height);
            AndroidPanel.LayoutParameters = ElementLP;

            AndroidCanvas.AddView(AndroidPanel);
          }
        }
      });

      return AndroidNode;
    }
    private AndroidNode TranslateDock(Inv.Panel InvPanel)
    {
      var InvDock = (Inv.Dock)InvPanel;

      var IsHorizontal = InvDock.Orientation == DockOrientation.Horizontal;

      var AndroidNode = AccessPanel(InvDock, () =>
      {
        var AndroidDock = new AndroidDockView(AndroidActivity);
        AndroidDock.Orientation = IsHorizontal ? Android.Widget.Orientation.Horizontal : Android.Widget.Orientation.Vertical;
        return AndroidDock;
      });

      RenderPanel(InvDock, AndroidNode, () =>
      {
        var AndroidLayout = AndroidNode.LayoutView;
        var AndroidDock = AndroidNode.PanelView;

        TranslateLayout(InvDock, AndroidLayout, AndroidDock);
        TranslateBackground(InvDock, AndroidDock.Background);

        if (InvDock.CollectionRender())
          AndroidDock.ComposeElements(InvDock.HeaderCollection.Select(H => TranslatePanel(H)), InvDock.ClientCollection.Select(C => TranslatePanel(C)), InvDock.FooterCollection.Select(F => TranslatePanel(F)));
      });

      return AndroidNode;
    }
    private AndroidNode TranslateEdit(Inv.Panel InvPanel)
    {
      var InvEdit = (Inv.Edit)InvPanel;

      var AndroidNode = AccessPanel(InvEdit, () =>
      {
        var AndroidEdit = new AndroidEditView(AndroidActivity);

        switch (InvEdit.Input)
        {
          case EditInput.Text:
            AndroidEdit.InputType = Android.Text.InputTypes.ClassText;
            break;

          case EditInput.Number:
            AndroidEdit.InputType = Android.Text.InputTypes.ClassNumber;
            break;

          case EditInput.Integer:
            AndroidEdit.InputType = Android.Text.InputTypes.ClassNumber | Android.Text.InputTypes.NumberFlagSigned;
            break;

          case EditInput.Decimal:
            AndroidEdit.InputType = Android.Text.InputTypes.ClassNumber | Android.Text.InputTypes.NumberFlagSigned | Android.Text.InputTypes.NumberFlagDecimal;
            break;

          case EditInput.Password:
            AndroidEdit.InputType = Android.Text.InputTypes.ClassText | Android.Text.InputTypes.TextVariationPassword;
            break;

          case EditInput.Name:
            AndroidEdit.InputType = Android.Text.InputTypes.ClassText | Android.Text.InputTypes.TextVariationPersonName;
            break;

          case EditInput.Email:
            AndroidEdit.InputType = Android.Text.InputTypes.ClassText | Android.Text.InputTypes.TextVariationEmailAddress;
            break;

          case EditInput.Uri:
            AndroidEdit.InputType = Android.Text.InputTypes.ClassText | Android.Text.InputTypes.TextVariationUri;
            break;

          default:
            throw new Exception("EditInput not handled: " + InvEdit.Input);
        }

        AndroidEdit.FocusableInTouchMode = true;
        AndroidEdit.AfterTextChanged += (Sender, Event) => Guard(() => InvEdit.ChangeInvoke(AndroidEdit.Text));
        return AndroidEdit;
      });

      RenderPanel(InvEdit, AndroidNode, () =>
      {
        var AndroidLayout = AndroidNode.LayoutView;
        var AndroidEdit = AndroidNode.PanelView;

        TranslateLayout(InvEdit, AndroidLayout, AndroidEdit);
        TranslateBackground(InvEdit, AndroidEdit.Background);

        TranslateFont(InvEdit.Font, (FontTypeface, FontColor, FontSize) =>
        {
          AndroidEdit.SetTypeface(FontTypeface, Android.Graphics.TypefaceStyle.Normal);
          AndroidEdit.SetTextColor(FontColor);
          AndroidEdit.SetTextSize(Android.Util.ComplexUnitType.Sp, FontSize);
        });

        AndroidEdit.Enabled = !InvEdit.IsReadOnly;
        AndroidEdit.Text = InvEdit.Text;
      });

      return AndroidNode;
    }
    private AndroidNode TranslateGraphic(Inv.Panel InvPanel)
    {
      var InvGraphic = (Inv.Graphic)InvPanel;

      var AndroidNode = AccessPanel(InvGraphic, () =>
      {
        var AndroidGraphic = new AndroidGraphicView(AndroidActivity);
        return AndroidGraphic;
      });

      RenderPanel(InvGraphic, AndroidNode, () =>
      {
        var AndroidLayout = AndroidNode.LayoutView;
        var AndroidGraphic = AndroidNode.PanelView;

        TranslateLayout(InvGraphic, AndroidLayout, AndroidGraphic);
        TranslateBackground(InvGraphic, AndroidGraphic.Background);

        if (InvGraphic.ImageSingleton.Render())
        {
          var Bitmap = TranslateBitmap(InvGraphic.Image);
          AndroidGraphic.SetImageBitmap(Bitmap);
        }
      });

      return AndroidNode;
    }
    private AndroidNode TranslateLabel(Inv.Panel InvPanel)
    {
      var InvLabel = (Inv.Label)InvPanel;

      var AndroidNode = AccessPanel(InvLabel, () =>
      {
        var AndroidLabel = new AndroidLabelView(AndroidActivity);
        return AndroidLabel;
      });

      RenderPanel(InvLabel, AndroidNode, () =>
      {
        var AndroidLayout = AndroidNode.LayoutView;
        var AndroidLabel = AndroidNode.PanelView;

        TranslateLayout(InvLabel, AndroidLayout, AndroidLabel);
        TranslateBackground(InvLabel, AndroidLabel.Background);
        TranslateFont(InvLabel.Font, (FontTypeface, FontColor, FontSize) =>
        {
          AndroidLabel.SetTypeface(FontTypeface, Android.Graphics.TypefaceStyle.Normal);
          AndroidLabel.SetTextColor(FontColor);
          AndroidLabel.SetTextSize(Android.Util.ComplexUnitType.Sp, FontSize);
        });

        if (InvLabel.Justification != null)
          AndroidLabel.Gravity = (InvLabel.Justification == Justification.Center ? Android.Views.GravityFlags.CenterHorizontal : InvLabel.Justification == Justification.Left ? Android.Views.GravityFlags.Left : Android.Views.GravityFlags.Right) | Android.Views.GravityFlags.CenterVertical;
        else
          AndroidLabel.Gravity = Android.Views.GravityFlags.CenterVertical;

        AndroidLabel.SetSingleLine(!InvLabel.LineWrapping);
        AndroidLabel.Text = InvLabel.Text;
      });

      return AndroidNode;
    }
    private AndroidNode TranslateMemo(Inv.Panel InvPanel)
    {
      var InvMemo = (Inv.Memo)InvPanel;

      var AndroidNode = AccessPanel(InvMemo, () =>
      {
        var AndroidMemo = new AndroidMemoView(AndroidActivity);
        AndroidMemo.AfterTextChanged += (Sender, Event) => Guard(() => InvMemo.ChangeInvoke(AndroidMemo.Text));
        return AndroidMemo;
      });

      RenderPanel(InvMemo, AndroidNode, () =>
      {
        var AndroidLayout = AndroidNode.LayoutView;
        var AndroidMemo = AndroidNode.PanelView;

        TranslateLayout(InvMemo, AndroidLayout, AndroidMemo);
        TranslateBackground(InvMemo, AndroidMemo.Background);

        TranslateFont(InvMemo.Font, (FontTypeface, FontColor, FontSize) =>
        {
          AndroidMemo.SetTypeface(FontTypeface, Android.Graphics.TypefaceStyle.Normal);
          AndroidMemo.SetTextColor(FontColor);
          AndroidMemo.SetTextSize(Android.Util.ComplexUnitType.Sp, FontSize);
        });

        AndroidMemo.IsReadOnly = InvMemo.IsReadOnly;
        AndroidMemo.Text = InvMemo.Text;
      });

      return AndroidNode;
    }
    private AndroidNode TranslateOverlay(Inv.Panel InvPanel)
    {
      var InvOverlay = (Inv.Overlay)InvPanel;

      var AndroidNode = AccessPanel(InvOverlay, () =>
      {
        var AndroidOverlay = new AndroidOverlayView(AndroidActivity);
        return AndroidOverlay;
      });

      RenderPanel(InvOverlay, AndroidNode, () =>
      {
        var AndroidLayout = AndroidNode.LayoutView;
        var AndroidOverlay = AndroidNode.PanelView;

        TranslateLayout(InvOverlay, AndroidLayout, AndroidOverlay);
        TranslateBackground(InvOverlay, AndroidOverlay.Background);

        if (InvOverlay.PanelCollection.Render())
        {
          AndroidOverlay.RemoveAllViews();

          foreach (var InvElement in InvOverlay.GetPanels())
          {
            var AndroidPanel = TranslatePanel(InvElement);

            var LP = new AndroidOverlayView.LayoutParams(AndroidOverlayView.LayoutParams.MatchParent, AndroidOverlayView.LayoutParams.MatchParent);
            AndroidPanel.LayoutParameters = LP;

            AndroidOverlay.AddView(AndroidPanel);
          }
        }
      });

      return AndroidNode;
    }
    private AndroidNode TranslateRender(Inv.Panel InvPanel)
    {
      var InvRender = (Inv.Render)InvPanel;

      var AndroidNode = AccessPanel(InvRender, () =>
      {
        var Result = new AndroidRenderView(AndroidActivity);
        Result.PressEvent += (TouchX, TouchY) => Guard(() => InvRender.PressInvoke(new Point(PxToPt(TouchX), PxToPt(TouchY))));
        Result.MoveEvent += (TouchX, TouchY) => Guard(() => InvRender.MoveInvoke(new Point(PxToPt(TouchX), PxToPt(TouchY))));
        Result.ReleaseEvent += (TouchX, TouchY) => Guard(() => InvRender.ReleaseInvoke(new Point(PxToPt(TouchX), PxToPt(TouchY))));
        Result.SingleTapEvent += (TouchX, TouchY) => Guard(() => InvRender.SingleTapInvoke(new Point(PxToPt(TouchX), PxToPt(TouchY))));
        Result.DoubleTapEvent += (TouchX, TouchY) => Guard(() => InvRender.DoubleTapInvoke(new Point(PxToPt(TouchX), PxToPt(TouchY))));
        Result.LongPressEvent += (TouchX, TouchY) => Guard(() => InvRender.ContextTapInvoke(new Point(PxToPt(TouchX), PxToPt(TouchY))));
        Result.ZoomEvent += (Delta) => Guard(() => InvRender.ZoomInvoke(Delta));

        var EllipseRect = new Android.Graphics.RectF();
        var BitmapRect = new Android.Graphics.Rect();
        var TextRect = new Android.Graphics.Rect();

        Result.DrawEvent += (AndroidContext) =>
        {
          foreach (var InvRenderElement in InvRender.GetCommands())
          {
            switch (InvRenderElement.Type)
            {
              case RenderType.Rectangle:
                var RectangleFillColour = InvRenderElement.RectangleFillColour;
                var RectangleStrokeColour = InvRenderElement.RectangleStrokeColour;
                var RectangleStrokeThickness = InvRenderElement.RectangleStrokeThickness;
                var RectangleRect = InvRenderElement.RectangleRect;
                var RectangleLeft = PtToPx(RectangleRect.Left);
                var RectangleTop = PtToPx(RectangleRect.Top);
                var RectangleRight = PtToPx(RectangleRect.Right + 1);
                var RectangleBottom = PtToPx(RectangleRect.Bottom + 1);

                if (RectangleFillColour != null)
                  AndroidContext.DrawRect(RectangleLeft, RectangleTop, RectangleRight, RectangleBottom, TranslateFillPaint(RectangleFillColour));

                if (RectangleStrokeColour != null && RectangleStrokeThickness > 0)
                {
                  RectangleLeft += RectangleStrokeThickness / 2;
                  RectangleTop += RectangleStrokeThickness / 2;

                  if (RectangleLeft + RectangleStrokeThickness < RectangleRight)
                    RectangleRight -= RectangleStrokeThickness;

                  if (RectangleTop + RectangleStrokeThickness < RectangleBottom)
                    RectangleBottom -= RectangleStrokeThickness;

                  AndroidContext.DrawRect(RectangleLeft, RectangleTop, RectangleRight, RectangleBottom, TranslateStrokePaint(RectangleStrokeColour, RectangleStrokeThickness));
                }
                break;

              case RenderType.Ellipse:
                var EllipseFillColour = InvRenderElement.EllipseFillColour;
                var EllipseStrokeColour = InvRenderElement.EllipseStrokeColour;
                var EllipseStrokeThickness = InvRenderElement.EllipseStrokeThickness;
                var EllipseCenter = InvRenderElement.EllipseCenter;
                var EllipseRadius = InvRenderElement.EllipseRadius;
                EllipseRect.Left = PtToPx(EllipseCenter.X - EllipseRadius.X);
                EllipseRect.Top = PtToPx(EllipseCenter.Y - EllipseRadius.Y);
                EllipseRect.Right = PtToPx(EllipseCenter.X + EllipseRadius.X);
                EllipseRect.Bottom = PtToPx(EllipseCenter.Y + EllipseRadius.Y);

                if (EllipseFillColour != null)
                  AndroidContext.DrawOval(EllipseRect, TranslateFillPaint(EllipseFillColour));

                if (EllipseStrokeColour != null && EllipseStrokeThickness > 0)
                  AndroidContext.DrawOval(EllipseRect, TranslateStrokePaint(EllipseStrokeColour, EllipseStrokeThickness));

                // NOTE: had a problem with a 5.0 emulator, so decided to just not use this supposed 21+ API call.
                /*if (IsAPILevel21) 
                {
                  if (EllipseFillColour != null)
                    AndroidContext.DrawOval(EllipseLeft, EllipseTop, EllipseRight, EllipseBottom, TranslateFillPaint(EllipseFillColour));

                  if (EllipseStrokeColour != null && EllipseStrokeThickness > 0)
                    AndroidContext.DrawOval(EllipseLeft, EllipseTop, EllipseRight, EllipseBottom, TranslateStrokePaint(EllipseStrokeColour, EllipseStrokeThickness));
                }*/
                break;

              case RenderType.Text:
                var TextPoint = InvRenderElement.TextPoint;
                var TextFragment = InvRenderElement.TextFragment;
                var TextHorizontal = InvRenderElement.TextHorizontal;
                var TextVertical = InvRenderElement.TextVertical;

                var TextPaint = TranslateTextPaint(InvRenderElement.TextFontName.EmptyAsNull() ?? InvApplication.Window.DefaultFont.Name ?? "", InvRenderElement.TextFontSize, InvRenderElement.TextFontWeight, InvRenderElement.TextFontColour);
                var TextPointX = PtToPx(TextPoint.X);
                var TextPointY = PtToPx(TextPoint.Y);

                TextPaint.GetTextBounds(TextFragment, 0, TextFragment.Length, TextRect);

                if (TextHorizontal != HorizontalPosition.Left)
                {
                  var TextWidth = TextRect.Width();

                  if (TextHorizontal == HorizontalPosition.Right)
                    TextPointX -= TextWidth;
                  else if (TextHorizontal == HorizontalPosition.Center)
                    TextPointX -= TextWidth / 2;
                }

                if (TextVertical != VerticalPosition.Bottom)
                {
                  var TextHeight = TextRect.Height();

                  if (TextVertical == VerticalPosition.Top)
                    TextPointY += TextHeight;
                  else if (TextVertical == VerticalPosition.Center)
                    TextPointY += TextHeight / 2;
                }

                AndroidContext.DrawText(TextFragment, TextPointX, TextPointY, TextPaint);
                break;

              case RenderType.Image:
                var ImageRect = InvRenderElement.ImageRect;
                var ImageOpacity = InvRenderElement.ImageOpacity;
                var ImageTint = InvRenderElement.ImageTint;
                var ImageMirror = InvRenderElement.ImageMirror;

                var ImageBitmap = TranslateBitmap(InvRenderElement.ImageSource);

                if (ImageBitmap != null)
                {
                  var ImagePaint = TranslateImagePaint(ImageOpacity, ImageTint);
                  BitmapRect.Left = PtToPx(ImageRect.Left);
                  BitmapRect.Top = PtToPx(ImageRect.Top);
                  BitmapRect.Right = PtToPx(ImageRect.Right + 1);
                  BitmapRect.Bottom = PtToPx(ImageRect.Bottom + 1);

                  if (ImageMirror == null)
                  {
                    AndroidContext.DrawBitmap(ImageBitmap, null, BitmapRect, ImagePaint);
                  }
                  else
                  {
                    var Matrix = new Android.Graphics.Matrix();

                    if (ImageMirror.Value == Mirror.Horizontal)
                    {
                      Matrix.SetScale(-1, +1);
                      Matrix.PostTranslate(BitmapRect.Left + ImageBitmap.Width, BitmapRect.Top);
                    }
                    else if (ImageMirror.Value == Mirror.Vertical)
                    {
                      Matrix.SetScale(+1, -1);
                      Matrix.PostTranslate(BitmapRect.Left, BitmapRect.Top + ImageBitmap.Height);
                    }

                    AndroidContext.DrawBitmap(ImageBitmap, Matrix, ImagePaint);
                  }
                }
                break;

              default:
                throw new ApplicationException("RenderType not handled: " + InvRenderElement.Type);
            }
          }
        };
        return Result;
      });

      RenderPanel(InvRender, AndroidNode, () =>
      {
        var AndroidLayout = AndroidNode.LayoutView;
        var AndroidRender = AndroidNode.PanelView;

        TranslateLayout(InvRender, AndroidLayout, AndroidRender);
        TranslateBackground(InvRender, AndroidRender.Background);

        InvRender.ContextSingleton.Data.Width = PxToPt(AndroidRender.Width);
        InvRender.ContextSingleton.Data.Height = PxToPt(AndroidRender.Height);
        if (InvRender.ContextSingleton.Render())
          AndroidRender.Render();
      });

      return AndroidNode;
    }
    private AndroidNode TranslateScroll(Inv.Panel InvPanel)
    {
      var InvScroll = (Inv.Scroll)InvPanel;

      var AndroidNode = AccessPanel(InvScroll, () =>
      {
        var AndroidScroll = new AndroidScrollView(AndroidActivity);
        //AndroidScroll.Orientation = InvScroll.Orientation == ScrollOrientation.Horizontal ? Android.Widget.Orientation.Horizontal : Android.Widget.Orientation.Vertical;
        return AndroidScroll;
      });

      RenderPanel(InvScroll, AndroidNode, () =>
      {
        var AndroidLayout = AndroidNode.LayoutView;
        var AndroidScroll = AndroidNode.PanelView;

        TranslateLayout(InvScroll, AndroidLayout, AndroidScroll);
        TranslateBackground(InvScroll, AndroidScroll.Background);

        if (InvScroll.ContentSingleton.Render())
        {
          var AndroidContent = TranslatePanel(InvScroll.Content);
          AndroidScroll.SetContentView(AndroidContent);
        }
      });

      return AndroidNode;
    }
    private AndroidNode TranslateFrame(Inv.Panel InvPanel)
    {
      var InvFrame = (Inv.Frame)InvPanel;

      var AndroidNode = AccessPanel(InvPanel, () =>
      {
        var Result = new AndroidFrameView(AndroidActivity);
        return Result;
      });

      RenderPanel(InvFrame, AndroidNode, () =>
      {
        var AndroidLayout = AndroidNode.LayoutView;
        var AndroidFrame = AndroidNode.PanelView;

        TranslateLayout(InvFrame, AndroidLayout, AndroidFrame);
        TranslateBackground(InvFrame, AndroidFrame.Background);

        if (InvFrame.ContentSingleton.Render())
        {
          AndroidFrame.SetContentView(null); // detach previous content in case it has moved.
          var AndroidPanel = TranslatePanel(InvFrame.Content);
          AndroidFrame.SetContentView(AndroidPanel);
        }
      });

      return AndroidNode;
    }
    private AndroidNode TranslateStack(Inv.Panel InvPanel)
    {
      var InvStack = (Inv.Stack)InvPanel;

      var IsVertical = InvStack.Orientation == StackOrientation.Vertical;

      var AndroidNode = AccessPanel(InvStack, () =>
      {
        var AndroidStack = new AndroidStackView(AndroidActivity);
        AndroidStack.Orientation = IsVertical ? Android.Widget.Orientation.Vertical : Android.Widget.Orientation.Horizontal;
        return AndroidStack;
      });

      RenderPanel(InvStack, AndroidNode, () =>
      {
        var AndroidLayout = AndroidNode.LayoutView;
        var AndroidStack = AndroidNode.PanelView;

        TranslateLayout(InvStack, AndroidLayout, AndroidStack);
        TranslateBackground(InvStack, AndroidStack.Background);

        if (InvStack.PanelCollection.Render())
          AndroidStack.ComposeElements(InvStack.PanelCollection.Select(E => TranslatePanel(E)));
      });

      return AndroidNode;
    }
    private AndroidNode TranslateTable(Inv.Panel InvPanel)
    {
      var InvTable = (Inv.Table)InvPanel;

      var AndroidNode = AccessPanel(InvTable, () =>
      {
        var AndroidTable = new AndroidTableView(AndroidActivity);
        return AndroidTable;
      });

      RenderPanel(InvTable, AndroidNode, () =>
      {
        var AndroidLayout = AndroidNode.LayoutView;
        var AndroidTable = AndroidNode.PanelView;

        TranslateLayout(InvTable, AndroidLayout, AndroidTable);
        TranslateBackground(InvTable, AndroidTable.Background);

        if (InvTable.CollectionRender())
        {
          // TODO: this is API21 only.
          // TODO: the fixed column width/row height is not working.
          // TODO: do we need a trailing column/row to fill the remaining space, when aligned stretch?

          AndroidTable.RemoveElements();
          AndroidTable.ColumnCount = InvTable.ColumnCollection.Count;
          AndroidTable.RowCount = InvTable.RowCollection.Count;

          foreach (var InvTableColumn in InvTable.ColumnCollection)
          {
            var AndroidColumn = TranslatePanel(InvTableColumn.Content);
            if (AndroidColumn == null)
              AndroidColumn = new AndroidContainerLayout(AndroidActivity);

            AndroidTableView.Spec AndroidColumnSpec;
            if (InvTableColumn.Length.Type == TableLengthType.Star)
              AndroidColumnSpec = AndroidTableView.InvokeSpec(InvTableColumn.Index, AndroidTableView.Fill, InvTableColumn.Length.Value / 2.0F);
            else
              AndroidColumnSpec = AndroidTableView.InvokeSpec(InvTableColumn.Index, AndroidTableView.Fill);

            var AndroidRowSpec = AndroidTableView.InvokeSpec(0, InvTable.RowCollection.Count, AndroidTableView.Fill);

            var AndroidColumnParams = new AndroidTableView.LayoutParams(AndroidColumnSpec, AndroidRowSpec);
            if (InvTableColumn.Length.Type == TableLengthType.Fixed)
              AndroidColumnParams.Width = PtToPx(InvTableColumn.Length.Value);
            AndroidColumn.LayoutParameters = AndroidColumnParams;

            AndroidTable.AddView(AndroidColumn);
          }

          foreach (var InvTableRow in InvTable.RowCollection)
          {
            var AndroidRow = TranslatePanel(InvTableRow.Content);
            if (AndroidRow == null)
              AndroidRow = new AndroidContainerLayout(AndroidActivity);

            var AndroidColumnSpec = AndroidTableView.InvokeSpec(0, InvTable.ColumnCollection.Count, AndroidTableView.Fill);

            AndroidTableView.Spec AndroidRowSpec;
            if (InvTableRow.Length.Type == TableLengthType.Star)
              AndroidRowSpec = AndroidTableView.InvokeSpec(InvTableRow.Index, AndroidTableView.Fill, InvTableRow.Length.Value / 2.0F);
            else
              AndroidRowSpec = AndroidTableView.InvokeSpec(InvTableRow.Index, AndroidTableView.Fill);

            var AndroidRowParams = new AndroidTableView.LayoutParams(AndroidColumnSpec, AndroidRowSpec);
            if (InvTableRow.Length.Type == TableLengthType.Fixed)
              AndroidRowParams.Height = PtToPx(InvTableRow.Length.Value);
            AndroidRow.LayoutParameters = AndroidRowParams;

            AndroidTable.AddView(AndroidRow);
          }

          foreach (var InvTableCell in InvTable.CellCollection)
          {
            var AndroidCell = TranslatePanel(InvTableCell.Content);

            if (AndroidCell != null)
            {
              var AndroidColumnSpec = AndroidTableView.InvokeSpec(InvTableCell.Column.Index, AndroidTableView.Fill);
              var AndroidRowSpec = AndroidTableView.InvokeSpec(InvTableCell.Row.Index, AndroidTableView.Fill);

              var AndroidCellParams = new AndroidTableView.LayoutParams(AndroidColumnSpec, AndroidRowSpec);
              AndroidCell.LayoutParameters = AndroidCellParams;

              //if (InvTableCell.Column.Length.Type == TableLengthType.Fixed)
              //  AndroidCellParams.Width = PtToPx(InvTableCell.Column.Length.Value);
              //
              //if (InvTableCell.Row.Length.Type == TableLengthType.Fixed)
              //  AndroidCellParams.Height = PtToPx(InvTableCell.Row.Length.Value);

              AndroidTable.AddView(AndroidCell);
            }
          }
        }
      });

      return AndroidNode;
    }

    private AndroidSurfaceView AccessSurface(Inv.Surface InvSurface, Func<Inv.Surface, AndroidSurfaceView> BuildFunction)
    {
      if (InvSurface.Node == null)
      {
        var Result = BuildFunction(InvSurface);

        InvSurface.Node = Result;

        return Result;
      }
      else
      {
        return (AndroidSurfaceView)InvSurface.Node;
      }
    }
    private System.Timers.Timer AccessTimer(Inv.Timer InvTimer, Func<Inv.Timer, System.Timers.Timer> BuildFunction)
    {
      if (InvTimer.Node == null)
      {
        var Result = BuildFunction(InvTimer);

        InvTimer.Node = Result;

        return Result;
      }
      else
      {
        return (System.Timers.Timer)InvTimer.Node;
      }
    }
    private AndroidNode<T> AccessPanel<T>(Panel InvPanel, Func<T> BuildFunction)
      where T : Android.Views.View
    {
      if (InvPanel.Node == null)
      {
        var Result = new AndroidNode<T>();

        Result.LayoutView = new AndroidContainerLayout(AndroidActivity);
        Result.PanelView = BuildFunction();

        Result.LayoutView.SetContentView(Result.PanelView);

        InvPanel.Node = Result;

        return Result;
      }
      else
      {
        return (AndroidNode<T>)InvPanel.Node;
      }
    }
    private void RenderPanel(Inv.Panel InvPanel, AndroidNode AndroidNode, Action Action)
    {
      if (InvPanel.Render())
      {
        // TODO: this should be unnecessary due to the Visibility GONE status, but it doesn't work as expected.
        if (InvPanel.Visibility.Get())
        {
          AndroidNode.LayoutView.SetContentView(AndroidNode.PanelView);

          Action();
        }
        else
        {
          AndroidNode.LayoutView.SetContentView(null);
        }
      }
    }
    private void TranslateBackground(Inv.Panel InvPanel, AndroidBackground AndroidBackground)
    {
      if (InvPanel.Background.Render())
        AndroidBackground.SetColor(TranslateColor(InvPanel.Background.Colour));

      if (InvPanel.CornerRadius.Render())
        AndroidBackground.SetCornerRadius(PtToPx(InvPanel.CornerRadius.TopLeft), PtToPx(InvPanel.CornerRadius.TopRight), PtToPx(InvPanel.CornerRadius.BottomRight), PtToPx(InvPanel.CornerRadius.BottomLeft));

      if (InvPanel.Border.Render())
        AndroidBackground.SetBorderStroke(TranslateColor(InvPanel.Border.Colour), PtToPx(InvPanel.Border.Thickness.Left));
    }
    private void TranslateBackground(Inv.Colour? InvColour, AndroidBackground AndroidBackground)
    {
      AndroidBackground.SetColor(TranslateColor(InvColour));
    }
    private void TranslateLayout(Inv.Panel InvPanel, AndroidContainerLayout AndroidContainer, Android.Views.View AndroidPanel)
    {
      if (InvPanel.Opacity.Render())
        AndroidPanel.Alpha = InvPanel.Opacity.Get();

      var InvVisibility = InvPanel.Visibility;
      if (InvVisibility.Render())
      {
        if (InvVisibility.Get())
          AndroidPanel.Visibility = Android.Views.ViewStates.Visible;
        else
          AndroidPanel.Visibility = Android.Views.ViewStates.Gone;
      }

      var InvPadding = InvPanel.Padding;

      if (InvPadding.Render() || InvPanel.Border.IsChanged)
      {
        var InvBorderThickness = InvPanel.Border.Thickness;

        AndroidPanel.SetPadding(PtToPx(InvPadding.Left + InvBorderThickness.Left), PtToPx(InvPadding.Top + InvBorderThickness.Top), PtToPx(InvPadding.Right + InvBorderThickness.Right), PtToPx(InvPadding.Bottom + InvBorderThickness.Bottom));
      }

      TranslateAlignment(InvPanel.Alignment, AndroidContainer);
      TranslateMargin(InvPanel.Margin, AndroidContainer);
      TranslateSize(InvPanel.Size, AndroidContainer);

      // ClipToOutline, OutlineProvider, Elevation introduced in API 21 (TranslateElevation needs to be a separate method to avoid breaking the runtime).
      if (AndroidSdkVersion >= 21)
        TranslateElevation(AndroidPanel, InvPanel);

      AndroidContainer.ResetContent();
    }
    private void TranslateElevation(Android.Views.View AndroidPanel, Panel InvPanel)
    {
      var InvElevation = InvPanel.Elevation;
      if (InvElevation.Render())
      {
        if (InvElevation.Depth > 0)
        {
          AndroidPanel.ClipToOutline = true;
          AndroidPanel.OutlineProvider = new AndroidPanelOutlineProvider(this, InvPanel);

          AndroidPanel.Elevation = PtToPx(InvElevation.Depth);
        }
        else
        {
          AndroidPanel.Elevation = 0;
        }
      }
    }
    private void TranslateMargin(Inv.Edge InvMargin, AndroidContainerLayout AndroidContainer)
    {
      if (InvMargin.Render())
      {
        if (InvMargin.IsSet)
          AndroidContainer.SetContentMargins(PtToPx(InvMargin.Left), PtToPx(InvMargin.Top), PtToPx(InvMargin.Right), PtToPx(InvMargin.Bottom));
        else
          AndroidContainer.SetContentMargins(0, 0, 0, 0);
      }
    }
    private void TranslateAlignment(Inv.Alignment InvAlignment, AndroidContainerLayout AndroidContainer)
    {
      if (InvAlignment.Render())
      {
        switch (InvAlignment.Get())
        {
          case Placement.BottomStretch:
            AndroidContainer.SetContentVerticalBottom();
            AndroidContainer.SetContentHorizontalStretch();
            break;

          case Placement.BottomLeft:
            AndroidContainer.SetContentVerticalBottom();
            AndroidContainer.SetContentHorizontalLeft();
            break;

          case Placement.BottomCenter:
            AndroidContainer.SetContentVerticalBottom();
            AndroidContainer.SetContentHorizontalCenter();
            break;

          case Placement.BottomRight:
            AndroidContainer.SetContentVerticalBottom();
            AndroidContainer.SetContentHorizontalRight();
            break;

          case Placement.TopStretch:
            AndroidContainer.SetContentVerticalTop();
            AndroidContainer.SetContentHorizontalStretch();
            break;

          case Placement.TopLeft:
            AndroidContainer.SetContentVerticalTop();
            AndroidContainer.SetContentHorizontalLeft();
            break;

          case Placement.TopCenter:
            AndroidContainer.SetContentVerticalTop();
            AndroidContainer.SetContentHorizontalCenter();
            break;

          case Placement.TopRight:
            AndroidContainer.SetContentVerticalTop();
            AndroidContainer.SetContentHorizontalRight();
            break;

          case Placement.CenterStretch:
            AndroidContainer.SetContentVerticalCenter();
            AndroidContainer.SetContentHorizontalStretch();
            break;

          case Placement.CenterLeft:
            AndroidContainer.SetContentVerticalCenter();
            AndroidContainer.SetContentHorizontalLeft();
            break;

          case Placement.Center:
            AndroidContainer.SetContentVerticalCenter();
            AndroidContainer.SetContentHorizontalCenter();
            break;

          case Placement.CenterRight:
            AndroidContainer.SetContentVerticalCenter();
            AndroidContainer.SetContentHorizontalRight();
            break;

          case Placement.Stretch:
            AndroidContainer.SetContentVerticalStretch();
            AndroidContainer.SetContentHorizontalStretch();
            break;

          case Placement.StretchLeft:
            AndroidContainer.SetContentVerticalStretch();
            AndroidContainer.SetContentHorizontalLeft();
            break;

          case Placement.StretchCenter:
            AndroidContainer.SetContentVerticalStretch();
            AndroidContainer.SetContentHorizontalCenter();
            break;

          case Placement.StretchRight:
            AndroidContainer.SetContentVerticalStretch();
            AndroidContainer.SetContentHorizontalRight();
            break;

          default:
            throw new ApplicationException("Placement not handled: " + InvAlignment.Get());
        }
      }
    }
    private void TranslateSize(Inv.Size InvSize, AndroidContainerLayout AndroidLayout)
    {
      if (InvSize.Render())
      {
        if (InvSize.Width != null)
          AndroidLayout.SetContentWidth(PtToPx(InvSize.Width.Value));
        else
          AndroidLayout.ClearContentWidth();

        if (InvSize.Height != null)
          AndroidLayout.SetContentHeight(PtToPx(InvSize.Height.Value));
        else
          AndroidLayout.ClearContentHeight();

        if (InvSize.MinimumWidth != null)
          AndroidLayout.SetContentMinimumWidth(PtToPx(InvSize.MinimumWidth.Value));
        else
          AndroidLayout.ClearContentMinimumWidth();

        if (InvSize.MinimumHeight != null)
          AndroidLayout.SetContentMinimumHeight(PtToPx(InvSize.MinimumHeight.Value));
        else
          AndroidLayout.ClearContentMinimumHeight();

        if (InvSize.MaximumWidth != null)
          AndroidLayout.SetContentMaximumWidth(PtToPx(InvSize.MaximumWidth.Value));
        else
          AndroidLayout.ClearContentMaximumWidth();

        if (InvSize.MaximumHeight != null)
          AndroidLayout.SetContentMaximumHeight(PtToPx(InvSize.MaximumHeight.Value));
        else
          AndroidLayout.ClearContentMaximumHeight();
      }
    }
    private void TranslateFont(Inv.Font InvFont, Action<Android.Graphics.Typeface, Android.Graphics.Color, int> Action)
    {
      if (InvFont.Render())
      {
        var FontTypeface = TranslateTypeface(InvFont.Name ?? InvApplication.Window.DefaultFont.Name, InvFont.Weight);
        var FontColor = TranslateColor(InvFont.Colour ?? InvApplication.Window.DefaultFont.Colour ?? Inv.Colour.Black);
        var FontSize = InvFont.Size ?? InvApplication.Window.DefaultFont.Size ?? 14; // TODO: what is the Inv default font size?

        Action(FontTypeface, FontColor, FontSize);
      }
    }

    private Android.Graphics.Typeface TranslateTypeface(string FontName, Inv.FontWeight InvFontWeight)
    {
      // TODO: custom font names.
      return AndroidFontArray[InvFontWeight];
    }
    private Android.Graphics.Bitmap TranslateBitmap(Inv.Image? Image)
    {
      if (Image == null)
      {
        return null;
      }
      else
      {
        return ImageBitmapDictionary.GetOrAdd(Image.Value, K =>
        {
          var ImageBuffer = K.GetBuffer();

          var Result = Android.Graphics.BitmapFactory.DecodeByteArray(ImageBuffer, 0, ImageBuffer.Length);

          return Result;
        });
      }
    }
    private Android.Graphics.Color TranslateColor(Inv.Colour? InvColour)
    {
      if (InvColour == null)
      {
        return Android.Graphics.Color.Transparent;
      }
      else
      {
        return ColourDictionary.GetOrAdd(InvColour.Value, C =>
        {
          var Result = new Android.Graphics.Color(C.RawValue);

          return Result;
        });
      }
    }
    private Inv.Colour? TranslateHoverColour(Inv.Colour? InvColour)
    {
      if (InvColour == null)
        return null;
      else
        return InvColour.Value.Lighten(0.25F);
    }
    private Inv.Colour? TranslatePressColour(Inv.Colour? InvColour)
    {
      if (InvColour == null)
        return null;
      else
        return InvColour.Value.Darken(0.25F);
    }
    private Android.Graphics.Paint TranslateFillPaint(Inv.Colour? InvColour)
    {
      if (InvColour == null)
        return null;

      return ColourPaintDictionary.GetOrAdd(InvColour.Value, B =>
      {
        var Result = new Android.Graphics.Paint()
        {
          AntiAlias = true,
          Color = TranslateColor(InvColour)
        };
        Result.SetStyle(Android.Graphics.Paint.Style.Fill);

        return Result;
      });
    }
    private Android.Graphics.Paint TranslateStrokePaint(Inv.Colour? Colour, int Thickness)
    {
      if (Colour == null || Thickness <= 0)
        return null;

      LookupStrokePaintKey.Colour = Colour.Value;
      LookupStrokePaintKey.Thickness = Thickness;
      
      return StrokePaintDictionary.GetOrAdd(LookupStrokePaintKey, B =>
      {
        var Result = new Android.Graphics.Paint()
        {
          AntiAlias = true,
          Color = TranslateColor(Colour),
          StrokeWidth = (float)PtToPx(Thickness)
        };
        Result.SetStyle(Android.Graphics.Paint.Style.Stroke);

        return Result;
      });
    }
    private Android.Graphics.Paint TranslateImagePaint(float Opacity, Colour? Tint)
    {
      if (Opacity == 1.0F && Tint == null)
        return null;

      LookupImagePaintKey.Opacity = Opacity;
      LookupImagePaintKey.Tint = Tint;
      
      return ImagePaintDictionary.GetOrAdd(LookupImagePaintKey, B =>
      {
        var Result = new Android.Graphics.Paint()
        {
          AntiAlias = true
        };

        if (Opacity != 1.0F)
          Result.Alpha = (int)(Opacity * 255);

        if (Tint != null)
          Result.SetColorFilter(new Android.Graphics.PorterDuffColorFilter(TranslateColor(Tint), Android.Graphics.PorterDuff.Mode.SrcAtop));

        return Result;
      });
    }
    private Android.Graphics.Paint TranslateTextPaint(string FontName, int FontSize, Inv.FontWeight FontWeight, Inv.Colour? FontColour)
    {
      if (FontColour == null || FontSize == 0)
        return null;

      LookupTextPaintKey.Name = FontName;
      LookupTextPaintKey.Size = FontSize;
      LookupTextPaintKey.Weight = FontWeight;
      LookupTextPaintKey.Colour = FontColour.Value;
      
      return TextPaintDictionary.GetOrAdd(LookupTextPaintKey, B =>
      {
        var Result = new Android.Graphics.Paint()
        {
          Color = TranslateColor(FontColour),
          TextSize = PtToPx(FontSize),
          AntiAlias = true
        };
        Result.SetTypeface(TranslateTypeface(FontName, FontWeight));

        return Result;
      });
    }
    private int PtToPx(int Points)
    {
      return (int)(Points * ConvertPtToPxFactor + 0.5F);
    }
    private int PxToPt(int Pixels)
    {
      return (int)(Pixels / ConvertPtToPxFactor + 0.5F);
    }
    private int CalculateInSampleSize(Android.Graphics.BitmapFactory.Options options, int reqWidth, int reqHeight)
    {
      // Raw height and width of image
      var height = options.OutHeight;
      var width = options.OutWidth;
      var inSampleSize = 1D;

      if (height > reqHeight || width > reqWidth)
      {
        var halfHeight = (int)(height / 2);
        var halfWidth = (int)(width / 2);

        // Calculate a inSampleSize that is a power of 2 - the decoder will use a value that is a power of two anyway.
        while ((halfHeight / inSampleSize) > reqHeight && (halfWidth / inSampleSize) > reqWidth)
          inSampleSize *= 2;
      }

      return (int)inSampleSize;
    }
    private string GetDeviceModel()
    {
      var manufacturer = Android.OS.Build.Manufacturer;
      var model = Android.OS.Build.Model;
      if (model.StartsWith(manufacturer))
        return Capitalize(model);
      else
        return Capitalize(manufacturer) + " " + model;
    }
    private string Capitalize(string s)
    {
      if (s == null || s.Length == 0)
        return "";

      var first = s[0];
      if (char.IsUpper(first))
        return s;
      else
        return char.ToUpper(first) + s.Substring(1);
    }

    private int AndroidSdkVersion;
    private float ConvertPtToPxFactor;
    private bool IsProcessPaused;
    private StrokePaintKey LookupStrokePaintKey;
    private ImagePaintKey LookupImagePaintKey;
    private TextPaintKey LookupTextPaintKey;
    private AndroidFrameCallback FrameCallback;
    private Android.Views.Choreographer Choreographer;
    private Android.Locations.LocationManager AndroidLocationManager;
    private Android.Views.ViewGroup.LayoutParams AndroidSurfaceLayoutParams;
    private Android.Views.InputMethods.InputMethodManager InputManager;
    private Dictionary<Type, Func<Inv.Panel, AndroidNode>> RouteDictionary;
    private Dictionary<Android.Views.Keycode, Inv.Key> KeyDictionary;
    private Inv.EnumArray<Inv.FontWeight, Android.Graphics.Typeface> AndroidFontArray;
    private Dictionary<Inv.Image, Android.Graphics.Bitmap> ImageBitmapDictionary;
    private Dictionary<Inv.Colour, Android.Graphics.Color> ColourDictionary;
    private Dictionary<Inv.Colour, Android.Graphics.Paint> ColourPaintDictionary;
    private Dictionary<StrokePaintKey, Android.Graphics.Paint> StrokePaintDictionary;
    private Dictionary<ImagePaintKey, Android.Graphics.Paint> ImagePaintDictionary;
    private Dictionary<TextPaintKey, Android.Graphics.Paint> TextPaintDictionary;
    
    private sealed class AndroidPanelOutlineProvider : Android.Views.ViewOutlineProvider
    {
      public AndroidPanelOutlineProvider(AndroidEngine Engine, Inv.Panel Panel)
      {
        this.Engine = Engine;
        this.Panel = Panel;
      }

      public override void GetOutline(Android.Views.View view, Android.Graphics.Outline outline)
      {
        outline.SetRoundRect(0, 0, view.Width, view.Height, Engine.PtToPx(Panel.CornerRadius.TopLeft));

        outline.SetOval(0, 0, view.Width, view.Height);
      }

      private Panel Panel;
      private AndroidEngine Engine;
    }
    
    private struct TextPaintKey
    {
      public string Name;
      public int Size;
      public Inv.FontWeight Weight;
      public Inv.Colour Colour;

      public override int GetHashCode()
      {
        return Name.GetHashCode() ^ Colour.GetHashCode() ^ Size.GetHashCode() ^ Weight.GetHashCode();
      }
      public override bool Equals(object obj)
      {
        var Key = (TextPaintKey)obj;

        return Key.Name == Name && Key.Size == Size && Key.Weight == Weight && Key.Colour == Colour;
      }
    }
    
    private struct ImagePaintKey
    {
      public Inv.Colour? Tint;
      public float Opacity;

      public override int GetHashCode()
      {
        if (Tint != null)
          return Tint.GetHashCode() ^ Opacity.GetHashCode();
        else
          return Opacity.GetHashCode();
      }
      public override bool Equals(object obj)
      {
        var Key = (ImagePaintKey)obj;

        return Key.Opacity == Opacity && Key.Tint == Tint;
      }
    }

    private struct StrokePaintKey
    {
      public Inv.Colour Colour;
      public int Thickness;

      public override int GetHashCode()
      {
        return Colour.GetHashCode() ^ Thickness.GetHashCode();
      }
      public override bool Equals(object obj)
      {
        var Key = (StrokePaintKey)obj;

        return Key.Thickness == Thickness && Key.Colour == Colour;
      }
    }

    private struct AnimationGroup
    {
      public AnimationGroup(Android.Views.View AndroidPanel, Android.Views.Animations.AnimationSet AnimationSet)
      {
        this.AndroidPanel = AndroidPanel;
        this.AnimationSet = AnimationSet;
      }

      public readonly Android.Views.View AndroidPanel;
      public readonly Android.Views.Animations.AnimationSet AnimationSet;
    }
  }

  internal abstract class AndroidNode
  {
    public AndroidContainerLayout LayoutView { get; set; }
    public Android.Views.View PanelView { get; set; }
  }

  internal sealed class AndroidNode<T> : AndroidNode
    where T : Android.Views.View
  {
    public new T PanelView
    {
      get { return (T)base.PanelView; }
      set { base.PanelView = value; }
    }
  }
}