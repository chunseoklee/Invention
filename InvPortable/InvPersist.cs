﻿/*! 23 !*/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Reflection;
using System.Diagnostics;
using Inv.Support;

namespace Inv.Persist
{
  public sealed class Save : IDisposable
  {
    internal Save(Governor Governor, Stream Stream)
    {
      this.Governor = Governor;
      this.Writer = new BinaryWriter(Stream, System.Text.Encoding.UTF8, true);
      this.HandleDictionary = new Dictionary<object, uint>(256 * 1024); // 256 * 1024 * 8 = 2MB (the 8 is the key-value entry).
    }
    public void Dispose()
    {
      Writer.Dispose();
    }

    public Governor Governor { get; private set; }

    public void WriteBoolean(bool Value)
    {
      Writer.Write(Value);
    }
    public void WriteChar(char Value)
    {
      Writer.Write(Value);
    }
    public void WriteDateTimeOffset(DateTimeOffset Value)
    {
      Writer.Write(Value.DateTime.ToBinary());
      Writer.Write(Value.Offset.Ticks);
    }
    public void WriteUInt8(byte Value)
    {
      Writer.Write(Value);
    }
    public void WriteUInt16(ushort Value)
    {
      Writer.Write(Value);
    }
    public void WriteUInt32(uint Value)
    {
      Writer.Write(Value);
    }
    public void WriteInt32(int Value)
    {
      Writer.Write(Value);
    }
    public void WriteInt64(long Value)
    {
      Writer.Write(Value);
    }
    public void WriteDecimal(decimal Value)
    {
      Writer.Write(Value);
    }
    public void WriteFloat(float Value)
    {
      Writer.Write(Value);
    }
    public void WriteDouble(double Value)
    {
      Writer.Write(Value);
    }
    public void WriteString(string Value)
    {
      Writer.Write(Value);
    }
    public void WriteBinary(Inv.Binary Value)
    {
      var Buffer = Value.GetBuffer();

      Writer.Write(Value.GetFormat());
      Writer.Write(Buffer.Length);
      Writer.Write(Buffer);
    }
    public void WriteImage(Inv.Image Value)
    {
      var Buffer = Value.GetBuffer();

      Writer.Write(Value.GetFormat());
      Writer.Write(Buffer.Length);
      Writer.Write(Buffer);
    }
    public void WriteSound(Inv.Sound Value)
    {
      var Buffer = Value.GetBuffer();

      Writer.Write(Value.GetFormat());
      Writer.Write(Buffer.Length);
      Writer.Write(Buffer);
    }
    public void WriteByteArray(byte[] Value)
    {
      Writer.Write(Value.Length);
      Writer.Write(Value);
    }
    public void WriteColour(Inv.Colour Value)
    {
      Writer.Write(Value.RawValue);
    }

    internal Dictionary<object, uint> HandleDictionary { get; private set; }

    internal void WriteRoot(Register Register, object Record)
    {
      HandleDictionary.Clear();

      var Handle = (uint)HandleDictionary.Count;
      HandleDictionary.Add(Record, Handle);

      WriteHandle(Handle);
      WriteRecord(Register, Record);
    }
    internal void WriteRecord(Register Register, object Record)
    {
      //Debug.WriteLine(Register.Name);

      foreach (var Serial in Register.SerialList)
      {
        var DataValue = Serial.GetFunction(Record);

        if (Serial.IsNullable)
          WriteBoolean(DataValue == null);

        if (DataValue != null)
        {
          if (Serial.IsDistinctList)
          {
            var List = (Inv.IDistinctList)DataValue;
            WriteInt32(List.Count);

            if (Serial.Primitive != null)
            {
              foreach (var Item in List)
                Serial.Primitive.SaveAction(this, Item);
            }
            else if (Serial.IsEnum)
            {
              foreach (var Item in List)
                WriteInt32((int)Item);
            }
            else
            {
              var ItemRegister = Governor.GetRegister(Serial.ItemType);

              foreach (var Item in List)
                WriteRegister(ItemRegister, Item);
            }
          }
          else if (Serial.IsHashSet)
          {
            var HashSet = ((System.Collections.IEnumerable)DataValue).ToEnumerableArray();

            WriteInt32(HashSet.Length);

            if (Serial.Primitive != null)
            {
              foreach (var Item in HashSet)
                Serial.Primitive.SaveAction(this, Item);
            }
            else if (Serial.IsEnum)
            {
              foreach (var Item in HashSet)
                WriteInt32((int)Item);
            }
            else
            {
              var ItemRegister = Governor.GetRegister(Serial.ItemType);

              foreach (var HashItem in HashSet)
                WriteRegister(ItemRegister, HashItem);
            }
          }
          else if (Serial.IsGrid)
          {
            var Grid = (Inv.IGrid)DataValue;

            WriteInt32(Grid.Width);
            WriteInt32(Grid.Height);

            var ItemRegister = Governor.GetRegister(Serial.ItemType);

            for (var Y = 0; Y < Grid.Height; Y++)
            {
              for (var X = 0; X < Grid.Width; X++)
              {
                var ItemValue = Grid[X, Y];
                var ItemIsNull = ItemValue == null;

                WriteBoolean(ItemIsNull);

                if (!ItemIsNull)
                  WriteRegister(ItemRegister, ItemValue);
              }
            }
          }
          else if (Serial.IsEnumArray)
          {
            var EnumArray = (Inv.IEnumArray)DataValue;

            if (Serial.Primitive != null)
            {
              foreach (var Tuple in EnumArray.GetTuples())
                Serial.Primitive.SaveAction(this, Tuple.Value);
            }
            else
            {
              var ItemRegister = Governor.GetRegister(Serial.ItemType);

              foreach (var Tuple in EnumArray.GetTuples())
              {
                var ItemIsNull = Tuple.Value == null;

                WriteBoolean(ItemIsNull);

                if (!ItemIsNull)
                  WriteRegister(ItemRegister, Tuple.Value);
              }
            }
          }
          else if (Serial.IsArray)
          {
            var Array = (Array)DataValue;

            WriteInt32(Array.Length);

            if (Serial.Primitive != null)
            {
              foreach (var Item in Array)
                Serial.Primitive.SaveAction(this, Item);
            }
            else if (Serial.IsEnum)
            {
              foreach (var Item in Array)
                WriteInt32((int)Item);
            }
            else
            {
              var ItemRegister = Governor.GetRegister(Serial.ItemType);

              foreach (var Item in Array)
                WriteRegister(ItemRegister, Item);
            }
          }
          else if (Serial.Primitive != null)
          {
            Serial.Primitive.SaveAction(this, DataValue);
          }
          else if (Serial.IsEnum)
          {
            WriteInt32((int)DataValue);
          }
          else
          {
            var FieldRegister = Governor.GetRegister(Serial.ItemType);
            WriteRegister(FieldRegister, DataValue);
          }
        }
      }
    }
    internal void WriteRegister(Register Register, object DataValue)
    {
      if (Register.IsCustom)
      {
        Register.Custom.SaveAction(this, DataValue);
      }
      else
      {
        uint Handle;

        if (HandleDictionary.TryGetValue(DataValue, out Handle))
        {
          WriteHandle(Handle);
        }
        else
        {
          Handle = (uint)HandleDictionary.Count;
          HandleDictionary.Add(DataValue, Handle);

          WriteHandle(Handle);
          WriteRecord(Register, DataValue);
        }
      }
    }
    internal void WriteHandle(uint Handle)
    {
      // TODO: optimise to byte -> ushort -> uint -> ulong?

      WriteUInt32(Handle);
    }

    private BinaryWriter Writer;
  }

  public sealed class Load : IDisposable
  {
    internal Load(Governor Governor, Stream Stream)
    {
      this.Governor = Governor;
      this.Reader = new BinaryReader(Stream, System.Text.Encoding.UTF8, true);
      this.LookupDictionary = new Dictionary<uint, object>(256 * 1024);
    }
    public void Dispose()
    {
      Reader.Dispose();
    }

    public Governor Governor { get; private set; }

    public bool ReadBoolean()
    {
      return Reader.ReadBoolean();
    }
    public char ReadChar()
    {
      return Reader.ReadChar();
    }
    public DateTimeOffset ReadDateTimeOffset()
    {
      var DateTimeBinary = Reader.ReadInt64();
      var OffsetTicks = Reader.ReadInt64();

      return new DateTimeOffset(DateTime.FromBinary(DateTimeBinary), TimeSpan.FromTicks(OffsetTicks));
    }
    public byte ReadUInt8()
    {
      return Reader.ReadByte();
    }
    public ushort ReadUInt16()
    {
      return Reader.ReadUInt16();
    }
    public uint ReadUInt32()
    {
      return Reader.ReadUInt32();
    }
    public int ReadInt32()
    {
      return Reader.ReadInt32();
    }
    public long ReadInt64()
    {
      return Reader.ReadInt64();
    }
    public decimal ReadDecimal()
    {
      return Reader.ReadDecimal();
    }
    public float ReadFloat()
    {
      return Reader.ReadSingle();
    }
    public double ReadDouble()
    {
      return Reader.ReadDouble();
    }
    public string ReadString()
    {
      return Reader.ReadString();
    }
    public Inv.Colour ReadColour()
    {
      return new Inv.Colour(Reader.ReadInt32());
    }
    public Inv.Binary ReadBinary()
    {
      var Format = Reader.ReadString();
      var Count = Reader.ReadInt32();
      var Bytes = Reader.ReadBytes(Count);

      return new Inv.Binary(Bytes, Format);
    }
    public Inv.Image ReadImage()
    {
      var Format = Reader.ReadString();
      var Count = Reader.ReadInt32();
      var Bytes = Reader.ReadBytes(Count);

      return new Inv.Image(Bytes, Format);
    }
    public Inv.Sound ReadSound()
    {
      var Format = Reader.ReadString();
      var Count = Reader.ReadInt32();
      var Bytes = Reader.ReadBytes(Count);

      return new Inv.Sound(Bytes, Format);
    }
    public byte[] ReadByteArray()
    {
      var Count = Reader.ReadInt32();
      var Bytes = Reader.ReadBytes(Count);

      return Bytes;
    }

    internal Dictionary<uint, object> LookupDictionary { get; private set; }

    internal void ReadRoot(Register Register, object Record)
    {
      LookupDictionary.Clear();

      var Handle = ReadHandle(Register);
      LookupDictionary.Add(Handle, Record);
      ReadRecord(Register, Record);
    }
    internal void ReadRecord(Register Register, object Record)
    {
      //Debug.WriteLine(Register.Name);

      foreach (var Serial in Register.SerialList)
      {
        bool IsNull;

        if (Serial.IsNullable)
          IsNull = ReadBoolean();
        else
          IsNull = false;

        object DataValue;

        if (IsNull)
        {
          DataValue = null;
        }
        else if (Serial.IsDistinctList)
        {
          var ListCount = ReadInt32();

          var List = (Inv.IDistinctList)Serial.Type.ReflectionCreate();
          DataValue = List;

          if (ListCount > 0)
          {
            List.Capacity = ListCount;

            if (Serial.Primitive != null)
            {
              foreach (var Index in ListCount.NumberSeries())
              {
                var Item = Serial.Primitive.LoadFunction(this);
                List.Add(Item);
              }
            }
            else if (Serial.IsEnum)
            {
              foreach (var Index in ListCount.NumberSeries())
              {
                var Item = ReadInt32();
                List.Add(Item);
              }
            }
            else
            {
              var ItemRegister = Governor.GetRegister(Serial.ItemType);

              foreach (var Index in ListCount.NumberSeries())
              {
                var ListItem = ReadRegister(ItemRegister);
                List.Add(ListItem);
              }
            }
          }
        }
        else if (Serial.IsHashSet)
        {
          var HashCount = ReadInt32();

          var DistinctListType = typeof(Inv.DistinctList<>).MakeGenericType(Serial.ItemType);

          var HashList = (Inv.IDistinctList)DistinctListType.ReflectionCreate();

          if (HashCount > 0)
          {
            HashList.Capacity = HashCount;

            if (Serial.Primitive != null)
            {
              foreach (var Index in HashCount.NumberSeries())
              {
                var Item = Serial.Primitive.LoadFunction(this);
                HashList.Add(Item);
              }
            }
            else if (Serial.IsEnum)
            {
              foreach (var Index in HashCount.NumberSeries())
              {
                var Item = ReadInt32();
                HashList.Add(Item);
              }
            }
            else
            {
              var ItemRegister = Governor.GetRegister(Serial.ItemType);

              foreach (var Index in HashCount.NumberSeries())
              {
                var HashItem = ReadRegister(ItemRegister);
                HashList.Add(HashItem);
              }
            }
          }

          var HashType = typeof(IEnumerable<>).MakeGenericType(Serial.ItemType);
          DataValue = Serial.Type.ReflectionCreate(HashType, HashList);
        }
        else if (Serial.IsGrid)
        {
          var Width = ReadInt32();
          var Height = ReadInt32();

          var Grid = (Inv.IGrid)Serial.Type.ReflectionCreate();
          DataValue = Grid;
          Grid.Width = Width;
          Grid.Height = Height;

          var ItemRegister = Governor.GetRegister(Serial.ItemType);

          for (var Y = 0; Y < Grid.Height; Y++)
          {
            for (var X = 0; X < Grid.Width; X++)
            {
              var ItemIsNull = ReadBoolean();

              if (!ItemIsNull)
                Grid[X, Y] = ReadRegister(ItemRegister);
            }
          }
        }
        else if (Serial.IsArray)
        {
          var ArrayLength = ReadInt32();

          var ItemArray = System.Array.CreateInstance(Serial.ItemType, ArrayLength);
          DataValue = ItemArray;

          if (ArrayLength > 0)
          {
            var ItemList = new List<object>(ArrayLength);

            if (Serial.Primitive != null)
            {
              foreach (var Index in ArrayLength.NumberSeries())
              {
                var Item = Serial.Primitive.LoadFunction(this);
                ItemList.Add(Item);
              }
            }
            else if (Serial.IsEnum)
            {
              foreach (var Index in ArrayLength.NumberSeries())
              {
                var Ordinal = ReadInt32();
                var Item = Enum.ToObject(Serial.ItemType, Ordinal);
                ItemList.Add(Item);
              }
            }
            else
            {
              var ItemRegister = Governor.GetRegister(Serial.ItemType);

              foreach (var Index in ArrayLength.NumberSeries())
              {
                var Item = ReadRegister(ItemRegister);
                ItemList.Add(Item);
              }
            }

            ((System.Collections.IList)ItemList).CopyTo(ItemArray, 0);
          }
        }
        else if (Serial.IsEnumArray)
        {
          var EnumArray = (Inv.IEnumArray)Serial.Type.ReflectionCreate();
          DataValue = EnumArray;

          if (Serial.Primitive != null)
          {
            foreach (var Tuple in EnumArray.GetTuples())
            {
              var Item = Serial.Primitive.LoadFunction(this);
              EnumArray.SetValue(Tuple.Key, Item);
            }
          }
          else
          {
            var ItemRegister = Governor.GetRegister(Serial.ItemType);

            foreach (var Tuple in EnumArray.GetTuples())
            {
              var ItemIsNull = ReadBoolean();

              if (!ItemIsNull)
                EnumArray.SetValue(Tuple.Key, ReadRegister(ItemRegister));
            }
          }
        }
        else if (Serial.Primitive != null)
        {
          DataValue = Serial.Primitive.LoadFunction(this);
        }
        else if (Serial.IsEnum)
        {
          DataValue = Enum.ToObject(Serial.ItemType, ReadInt32()); 
        }
        else
        {
          var FieldRegister = Governor.GetRegister(Serial.ItemType);
          DataValue = ReadRegister(FieldRegister);
        }

        if (DataValue != null)
          Serial.SetAction(Record, DataValue);
      }
    }
    internal object ReadRegister(Register Register)
    {
      object DataValue;

      if (Register.IsCustom)
      {
        DataValue = Register.Custom.LoadFunction(this);
      }
      else
      {
        var Handle = ReadHandle(Register);

        if (!LookupDictionary.TryGetValue(Handle, out DataValue))
        {
          DataValue = Register.RegisterType.ReflectionCreate();
          LookupDictionary.Add(Handle, DataValue);

          ReadRecord(Register, DataValue);
        }
      }

      return DataValue;
    }
    internal uint ReadHandle(Register Register)
    {
      // TODO: optimise to byte -> ushort -> uint -> ulong?

      try
      {
        return ReadUInt32();
      }
      catch (Exception Exception)
      {
        // NOTE: this is here to improve the error message in case of an 'end of stream' bug.
        throw new Exception("Register '" + Register.RegisterType.Name + "': " + Exception.Message, Exception);
      }
    }

    private BinaryReader Reader;
  }

  public sealed class Governor
  {
    public Governor()
    {
      this.RegisterList = new DistinctList<Register>();
      this.RegisterDictionary = new Dictionary<Type, Register>();
    }

    public Register<TRegister> Register<TRegister>()
    {
      return new Register<TRegister>(AddRegister(typeof(TRegister)));
    }
    public void Validate<TRoot>()
    {
      var Root = GetRegister(typeof(TRoot));

      var VisitSet = new HashSet<Register>();
      Validate(Root, VisitSet);
    }
    public void Save<TRegister>(TRegister Record, Stream Stream)
    {
      var Register = GetRegister(typeof(TRegister));
      if (Register.IsCustom)
        throw new Exception("Cannot save a custom register as the root object.");

      using (var Context = new Save(this, Stream))
      {
        Context.WriteInt32(Register.RegisterIndex);

        Context.WriteRoot(Register, Record);
      }
    }
    public void Load<TRegister>(TRegister Record, Stream Stream)
    {
      var Register = GetRegister(typeof(TRegister));
      if (Register.IsCustom)
        throw new Exception("Cannot load a custom register as the root object.");

      using (var Context = new Load(this, Stream))
      {
        var RootIndex = Context.ReadInt32();

        if (RootIndex != Register.RegisterIndex)
          throw new Exception("Root object load known index mismatch.");

        Context.ReadRoot(Register, Record);
      }
    }
    public object Load(Stream Stream)
    {
      using (var Context = new Load(this, Stream))
      {
        var RootIndex = Context.ReadInt32();

        if (RootIndex < 1 || RootIndex > RegisterList.Count)
          throw new Exception("Root object register list mismatch.");

        var Register = RegisterList[RootIndex - 1];
        if (Register.RegisterIndex != RootIndex)
          throw new Exception("Root object load anonymous index mismatch.");

        if (Register.IsCustom)
          throw new Exception("Cannot load a custom register as the root object.");

        return Context.ReadRegister(Register);
      }
    }

    private void Validate(Register Register, HashSet<Register> VisitSet)
    {
      if (VisitSet.Add(Register))
      {
        if (!Register.IsCustom)
        {
          if (Register.RegisterType.GetReflectionInfo().IsClass && Register.RegisterType.ReflectionDefaultConstructor() == null)
            throw new Exception(Register.RegisterType.FullName + " must have a default constructor.");

          foreach (var Serial in Register.SerialList)
          {
            if (Serial.Primitive == null && !Serial.IsEnum)
            {
              var FieldRegister = GetRegisterOrDefault(Serial.ItemType);
              if (FieldRegister == null)
                throw new Exception(Register.RegisterType.FullName + " serialises " + Serial.ItemType.FullName + " but there is no register.");

              Validate(FieldRegister, VisitSet);
            }
          }
        }

        if (Register.PolymorphList != null)
        {
          foreach (var Polymorph in Register.PolymorphList)
            Validate(Polymorph, VisitSet);
        }
      }
    }

    internal Register AddRegister(Type RegisterType)
    {
      var Result = new Register(this, RegisterType, RegisterDictionary.Count + 1);

      RegisterList.Add(Result);
      RegisterDictionary.Add(RegisterType, Result);

      return Result;
    }
    internal Register GetRegisterOrDefault(Type Type)
    {
      return RegisterDictionary.GetValueOrDefault(Type);
    }
    internal Register GetRegister(Type Type)
    {
      var Result = GetRegisterOrDefault(Type);

      if (Result == null)
        throw new Exception("Register not found for type: " + Type);

      return Result;
    }

    private Dictionary<Type, Register> RegisterDictionary;
    private Inv.DistinctList<Register> RegisterList;
  }

  public sealed class Register
  {
    internal Register(Governor Governor, Type RegisterType, int RegisterIndex)
    {
      this.Governor = Governor;
      this.RegisterType = RegisterType;
      this.RegisterIndex = RegisterIndex;

      var RegisterInfo = RegisterType.GetReflectionInfo();

      this.SerialList = new DistinctList<Serial>();

      // NOTE: all data properties have backing fields - don't need to double up.
      //foreach (var ReflectionProperty in RegisterInfo.GetReflectionProperties().Where(P => P.GetReflectionGetMethod() != null && !P.GetReflectionGetMethod().IsStatic && P.GetReflectionSetMethod() != null && !P.GetReflectionSetMethod().IsStatic))
      //  SerialList.Add(new Serial(ReflectionProperty));

      // all non-static fields.
      foreach (var ReflectionField in RegisterInfo.GetReflectionFields().Where(F => !F.IsStatic))
        SerialList.Add(new Serial(ReflectionField));

      this.PolymorphList = null;
    }

    internal string Name
    {
      get { return RegisterType.Name; }
    }
    internal Type RegisterType { get; private set; }
    internal int RegisterIndex { get; private set; }
    internal bool IsCustom
    {
      get { return Custom != null; }
    }
    internal Custom Custom { get; private set; }
    internal Inv.DistinctList<Serial> SerialList { get; private set; }
    internal Inv.DistinctList<Register> PolymorphList { get; private set; }

    public void AsCustom(Action<Save, object> SaveAction, Func<Load, object> LoadFunction)
    {
      this.Custom = new Custom(SaveAction, LoadFunction);
    }
    public void AsPolymorph()
    {
      var TypeList = RegisterType.GetReflectionInfo().Assembly.GetReflectionTypes().Where(T => T.GetReflectionInfo().BaseType == RegisterType).ToDistinctList();
      this.PolymorphList = TypeList.Select(P =>
      {
        var PolymorphRegister = Governor.AddRegister(P);

        // inherited fields.
        if (SerialList.Count > 0)
          PolymorphRegister.SerialList.InsertRange(0, SerialList);

        return PolymorphRegister;
      }).ToArray().ToDistinctList();

      AsCustom
      (
        (Save, R) =>
        {
          var PolymorphIndex = TypeList.IndexOf(R.GetType());
          Save.WriteInt32(PolymorphIndex);

          var PolymorphRegister = PolymorphList[PolymorphIndex];
          Save.WriteRecord(PolymorphRegister, R);
        },
        (Load) =>
        {
          var PolymorphIndex = Load.ReadInt32();
          var PolymorphInstance = TypeList[PolymorphIndex].ReflectionCreate();

          var PolymorphRegister = PolymorphList[PolymorphIndex];
          Load.ReadRecord(PolymorphRegister, PolymorphInstance);

          return PolymorphInstance;
        }
      );
    }

    private Governor Governor;
  }

  public sealed class Register<TRegister>
  {
    internal Register(Register Base)
    {
      this.Base = Base;
    }

    public void AsCustom(Action<Save, TRegister> SaveAction, Func<Load, TRegister> LoadFunction)
    {
      Base.AsCustom((S, R) => SaveAction(S, (TRegister)R), (L) => LoadFunction(L));
    }
    public void AsLookup(IEnumerable<TRegister> Registers, Func<TRegister, string> HandleFunction)
    {
      var Dictionary = new Dictionary<string, TRegister>(StringComparer.OrdinalIgnoreCase);
      foreach (var Register in Registers)
        Dictionary.Add(HandleFunction(Register), Register);

      AsCustom
      (
        (Save, R) =>
        {
          var Handle = HandleFunction(R);
          Save.WriteString(Handle);
        },
        (Load) =>
        {
          var Handle = Load.ReadString();
          var Result = Dictionary.GetValueOrDefault(Handle);

          if (Result == null)
            throw new Exception(Base.Name + " lookup for '" + Handle + "' had no entry.");

          return Result;
        }
      );
    }
    public void AsPolymorph()
    {
      Base.AsPolymorph();
    }

    private Register Base;
  }

  internal sealed class Serial
  {
    internal Serial(PropertyInfo PropertyInfo)
      : this(PropertyInfo.Name, PropertyInfo.PropertyType)
    {
      this.PropertyInfo = PropertyInfo;
    }
    internal Serial(FieldInfo FieldInfo)
      : this (FieldInfo.Name, FieldInfo.FieldType)
    {
      this.FieldInfo = FieldInfo;
    }
    internal Serial(string Name, Type Type)
    {
      this.Name = Name;
      this.Type = Type;
      this.IsNullable = Type.IsNullable();
      this.IsGrid = Type.IsSubclassOfRawGeneric(typeof(Inv.Grid<>));
      this.IsHashSet = Type.IsSubclassOfRawGeneric(typeof(HashSet<>));
      //this.IsDictionary = SerialType.IsSubclassOfRawGeneric(typeof(Dictionary<,>));
      this.IsDistinctList = Type.IsSubclassOfRawGeneric(typeof(Inv.DistinctList<>));
      this.IsEnumArray = Type.IsSubclassOfRawGeneric(typeof(Inv.EnumArray<,>));
      this.IsArray = Type.IsArray && Type != typeof(byte[]);

      if (IsDistinctList)
        this.ItemType = Type.GetReflectionGenericTypeArguments()[0];
      else if (IsGrid)
        this.ItemType = Type.GetReflectionGenericTypeArguments()[0];
      else if (IsHashSet)
        this.ItemType = Type.GetReflectionGenericTypeArguments()[0];
      else if (IsEnumArray)
        this.ItemType = Type.GetReflectionGenericTypeArguments()[1];
      else if (IsArray)
        this.ItemType = Type.GetElementType();
      else if (Type.GetReflectionInfo().IsGenericType && Type.GetGenericTypeDefinition() == typeof(Nullable<>))
        this.ItemType = Nullable.GetUnderlyingType(Type);
      else
        this.ItemType = Type;

      this.Primitive = Foundation.PrimitiveCustomDictionary.GetValueOrDefault(ItemType);
      this.IsEnum = ItemType.GetReflectionInfo().IsEnum;
    }

    internal readonly string Name;
    internal readonly Type Type;
    internal readonly bool IsNullable;
    internal readonly bool IsDistinctList;
    internal readonly bool IsArray;
    internal readonly bool IsEnum;
    internal readonly bool IsGrid;
    internal readonly bool IsHashSet;
    internal readonly bool IsEnumArray;
    internal readonly Type ItemType;
    internal readonly Custom Primitive;

    internal object GetFunction(object Self)
    {
      if (PropertyInfo != null)
        return PropertyInfo.GetReflectionValue(Self);
      else
        return FieldInfo.GetReflectionValue(Self);
    }
    internal void SetAction(object Self, object Value)
    {
      if (PropertyInfo != null)
        PropertyInfo.SetReflectionValue(Self, Value);
      else
        FieldInfo.SetReflectionValue(Self, Value);
    }

    private PropertyInfo PropertyInfo;
    private FieldInfo FieldInfo;
  }

  internal sealed class Custom
  {
    internal Custom(Action<Save, object> SaveAction, Func<Load, object> LoadFunction)
    {
      this.SaveAction = SaveAction;
      this.LoadFunction = LoadFunction;
    }

    internal readonly Action<Save, object> SaveAction;
    internal readonly Func<Load, object> LoadFunction;
  }

  internal static class Foundation
  {
    static Foundation()
    {
      PrimitiveCustomDictionary = new Dictionary<Type, Custom>();

      AddPrimitiveCustom<string>((S, R) => S.WriteString(R), (L) => L.ReadString());
      AddPrimitiveCustom<int>((S, R) => S.WriteInt32(R), (L) => L.ReadInt32());
      AddPrimitiveCustom<long>((S, R) => S.WriteInt64(R), (L) => L.ReadInt64());
      AddPrimitiveCustom<bool>((S, R) => S.WriteBoolean(R), (L) => L.ReadBoolean());
      AddPrimitiveCustom<char>((S, R) => S.WriteChar(R), (L) => L.ReadChar());
      AddPrimitiveCustom<decimal>((S, R) => S.WriteDecimal(R), (L) => L.ReadDecimal());
      AddPrimitiveCustom<float>((S, R) => S.WriteFloat(R), (L) => L.ReadFloat());
      AddPrimitiveCustom<double>((S, R) => S.WriteDouble(R), (L) => L.ReadDouble());
      AddPrimitiveCustom<byte[]>((S, R) => S.WriteByteArray(R), (L) => L.ReadByteArray());
      AddPrimitiveCustom<DateTimeOffset>((S, R) => S.WriteDateTimeOffset(R), (L) => L.ReadDateTimeOffset());
      AddPrimitiveCustom<Inv.Binary>((S, R) => S.WriteBinary(R), (L) => L.ReadBinary());
      AddPrimitiveCustom<Inv.Colour>((S, R) => S.WriteColour(R), (L) => L.ReadColour());
      AddPrimitiveCustom<Inv.Image>((S, R) => S.WriteImage(R), (L) => L.ReadImage());
      AddPrimitiveCustom<Inv.Sound>((S, R) => S.WriteSound(R), (L) => L.ReadSound());
    }

    internal static readonly Dictionary<Type, Custom> PrimitiveCustomDictionary;

    internal static bool IsSubclassOfRawGeneric(this Type toCheck, Type generic)
    {
      while (toCheck != null && toCheck != typeof(object))
      {
        var cur = toCheck.GetReflectionInfo().IsGenericType ? toCheck.GetGenericTypeDefinition() : toCheck;

        if (generic == cur)
          return true;

        toCheck = toCheck.GetReflectionInfo().BaseType;
      }

      return false;
    }
    internal static bool IsNullable(this Type Type)
    {
      if (!Type.GetReflectionInfo().IsValueType)
        return true; // ref-type

      if (Nullable.GetUnderlyingType(Type) != null)
        return true; // Nullable<T>

      return false; // value-type
    }

    private static void AddPrimitiveCustom<TPrimitive>(Action<Save, TPrimitive> SaveAction, Func<Load, TPrimitive> LoadFunction)
    {
      var Custom = new Custom((S, R) => SaveAction(S, (TPrimitive)R), (L) => LoadFunction(L));

      PrimitiveCustomDictionary.Add(typeof(TPrimitive), Custom);
    }
  }
}