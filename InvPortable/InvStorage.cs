﻿/*! 2 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inv.Support;
using System.Diagnostics;

namespace Inv.Storage
{
  // Invention ORM.
  public interface Contract<TBase>
  {
    TBase Base { get; }
  }

  public sealed class Manifest
  {
    public Manifest()
    {
      this.RootList = new DistinctList<Root>();
    }

    public void AddRoot<TRoot>()
    {
      RootList.Add(new Root(typeof(TRoot)));
    }
    public IEnumerable<Root> GetRoots()
    {
      return RootList;
    }
    public Mapping CompileMapping()
    {
      return new Mapping(this);
    }

    private Inv.DistinctList<Root> RootList;
  }

  public sealed class Mapping
  {
    public Mapping(Manifest Manifest)
    {
      this.Manifest = Manifest;
      this.Schematic = new Inv.Database.Schematic();
      this.EntityList = new DistinctList<Entity>();

      foreach (var Root in Manifest.GetRoots())
        CompileTable(Root.Type);
    }

    public Inv.Database.Schematic Schematic { get; private set; }

    public IEnumerable<Entity> GetEntities()
    {
      return EntityList;
    }

    private Inv.Database.Table CompileTable(Type RecordType)
    {
      var Result = Schematic.ForceTable(RecordType.FullName);
      Result.SetPrimaryKeyColumn(KeyID);

      var Entity = new Entity(RecordType, Result);
      EntityList.Add(Entity);

      foreach (var RecordField in RecordType.GetReflectionInfo().DeclaredFields)
      {
        var FieldName = RecordField.Name;
        var ColumnName = FieldName;
        if (ColumnName[0] == '<')
        {
          Debug.Assert(ColumnName.EndsWith("k__BackingField"), "Field name was expected to be a backing field.");

          ColumnName = ColumnName.Substring(1, ColumnName.IndexOf('>') - 1);
        }

        // Avoid collisions with primary key.
        if (ColumnName == KeyID)
          ColumnName = "_" + ColumnName;

        var FieldType = RecordField.FieldType;

        var FieldNullable = FieldType.IsGenericType && FieldType.GetReflectionInfo().GetGenericTypeDefinition() == typeof(Nullable<>);
        if (FieldNullable)
          FieldType = FieldType.GetReflectionInfo().GetGenericArguments()[0];

        if (FieldType.IsGenericType && FieldType.GetReflectionInfo().GetGenericTypeDefinition() == typeof(Inv.DistinctList<>))
        {
          var ItemType = FieldType.GetReflectionInfo().GetGenericArguments()[0];

          var ItemRoot = Manifest.GetRoots().Find(R => R.Type == ItemType);

          if (ItemRoot == null)
          {
            // aggregation collection.
            var AggregationTable = CompileTable(ItemType);
            AggregationTable.AddForeignKeyColumn(RecordType.Name + KeyID, Result, true); // may not parented to this root (consider same aggregate used from two parent roots).
          }
          else
          {
            // association collection.
            var AssocationSelf = RecordType == ItemRoot.Type;
            var ItemTable = Schematic.ForceTable(ItemRoot.Type.FullName); // this is definitely a root table, but we may not have built it yet.

            var AssociationTable = Schematic.AddTable(RecordType.FullName + "-" + ColumnName);
            AssociationTable.AddForeignKeyColumn((AssocationSelf ? "Parent" : RecordType.Name) + KeyID, Result, false);
            AssociationTable.AddForeignKeyColumn((AssocationSelf ? "Child" : ColumnName) + KeyID, ItemTable, false);
          }
        }
        else if (FieldType.IsClass && !FieldType.IsPrimitive && FieldType != typeof(string))
        {
          ColumnName += KeyID;

          var FieldRoot = Manifest.GetRoots().Find(R => R.Type == FieldType);

          if (FieldRoot == null)
          {
            // aggregation singleton.
            var AggregationTable = CompileTable(FieldType);
            Result.AddForeignKeyColumn(ColumnName, AggregationTable, FieldNullable);
          }
          else
          {
            // association singleton.
            var AssociationTable = Schematic.ForceTable(FieldType.FullName);
            Result.AddForeignKeyColumn(ColumnName, AssociationTable, FieldNullable);
          }

          if (FieldName != ColumnName)
            Entity.RouteFieldColumn(FieldName, ColumnName);
        }
        else
        {
          Inv.Database.DataType ColumnType;
          if (FieldType == typeof(string))
            ColumnType = Inv.Database.DataType.String;
          else if (FieldType == typeof(int))
            ColumnType = Inv.Database.DataType.Integer32;
          else if (FieldType == typeof(long))
            ColumnType = Inv.Database.DataType.Integer64;
          else if (FieldType == typeof(bool))
            ColumnType = Inv.Database.DataType.Boolean;
          else if (FieldType == typeof(DateTimeOffset))
            ColumnType = Inv.Database.DataType.DateTimeOffset;
          else if (FieldType == typeof(DateTime))
            ColumnType = Inv.Database.DataType.DateTime;
          else if (FieldType == typeof(TimeSpan))
            ColumnType = Inv.Database.DataType.TimeSpan;
          else
            throw new Exception("Field type not mapped to a database type: " + FieldType.FullName);

          Result.AddColumn(ColumnName, ColumnType, FieldNullable);

          if (FieldName != ColumnName)
            Entity.RouteFieldColumn(FieldName, ColumnName);
        }
      }

      return Result;
    }

    private const string KeyID = "Key";

    private Manifest Manifest;
    private Inv.DistinctList<Entity> EntityList;
  }

  public sealed class Root
  {
    internal Root(Type Type)
    {
      this.Type = Type;
    }

    public Type Type { get; private set; }
  }

  public sealed class Entity
  {
    internal Entity(Type Type, Inv.Database.Table Table)
    {
      this.Type = Type;
      this.Table = Table;
      this.FieldDictionary = new Dictionary<string, string>();
    }

    public Type Type { get; private set; }
    public Inv.Database.Table Table { get; private set; }

    internal void RouteFieldColumn(string FieldName, string ColumnName)
    {
      FieldDictionary.Add(FieldName, ColumnName);
    }

    private Dictionary<string, string> FieldDictionary;
  }
}