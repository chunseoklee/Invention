﻿/*! 2 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.IO;
using Inv.Support;

namespace Inv.Syntax
{
  public static class Foundation
  {
    /*
    public static void AppendTextFile(Grammar SyntaxGrammar, string FilePath, Action<Formatter> Action)
    {
      using (var FileStream = new FileStream(FilePath, FileMode.Append, FileAccess.Write, FileShare.Read))
        WriteTextStream(SyntaxGrammar, FileStream, Action);
    }
    public static void WriteTextFile(Grammar SyntaxGrammar, string FilePath, Action<Formatter> Action)
    {
      using (var FileStream = new FileStream(FilePath, FileMode.Create, FileAccess.Write, FileShare.Read))
        WriteTextStream(SyntaxGrammar, FileStream, Action);
    }
    public static void ReadTextFile(Grammar SyntaxGrammar, string FilePath, Action<Extractor> Action)
    {
      using (var FileStream = new FileStream(FilePath, FileMode.Open, FileAccess.Read, FileShare.Read))
        ReadTextStream(SyntaxGrammar, FileStream, Action);
    }
    */
    public static void WriteTextString(Grammar SyntaxGrammar, out string TextString, Action<Formatter> Action)
    {
      using (var StringWriter = new StringWriter())
      {
        var SyntaxFormatter = new Formatter(new TextWriteService(StringWriter, SyntaxGrammar));

        SyntaxFormatter.Start();

        Action(SyntaxFormatter);

        SyntaxFormatter.Stop();

        StringWriter.Flush();

        TextString = StringWriter.ToString();
      }
    }
    public static void WriteTextStream(Grammar SyntaxGrammar, Stream Stream, Action<Formatter> Action)
    {
      using (var StreamWriter = new StreamWriter(Stream))
      {
        var SyntaxFormatter = new Formatter(new TextWriteService(StreamWriter, SyntaxGrammar));

        SyntaxFormatter.Start();

        Action(SyntaxFormatter);

        SyntaxFormatter.Stop();

        StreamWriter.Flush();
      }
    }
    public static void ReadTextStream(Grammar SyntaxGrammar, Stream Stream, Action<Extractor> Action)
    {
      using (var StreamReader = new StreamReader(Stream))
      {
        var SyntaxExtractor = new Extractor(new TextReadService(StreamReader, SyntaxGrammar));

        SyntaxExtractor.Start();

        Action(SyntaxExtractor);

        SyntaxExtractor.Stop();
      }
    }
  }

  public interface WriteContract
  {
    void Start();
    void Stop();
    void WriteToken(Token Token);
    string Publish();
  }

  public interface ReadContract
  {
    void Start();
    void Stop();
    bool More();
    Token PeekToken();
    Token ReadToken();
    ExtractException Interrupt(string Message);
  }

  public sealed class TextWriteService : WriteContract
  {
    public TextWriteService(TextWriter ActiveWriter, Grammar SyntaxGrammar)
    {
      this.ActiveWriter = ActiveWriter;
      this.SyntaxGrammar = SyntaxGrammar;
    }

    void WriteContract.Start()
    {
      GrammarManager = new GrammarManager(SyntaxGrammar);

      LineCount = 1;
    }
    void WriteContract.Stop()
    {
      GrammarManager = null;

      LineCount = 0;
    }
    void WriteContract.WriteToken(Token Token)
    {
      if (Token.Type == TokenType.Newline)
      {
        for (var NewlineCount = 0; NewlineCount < Token.Newline; NewlineCount++)
        {
          ActiveWriter.WriteLine();
          LineCount++;
        }
      }
      else
      {
        string Representation;

        switch (Token.Type)
        {
          case Syntax.TokenType.Whitespace:
            Representation = Token.Whitespace;
            break;

          case Syntax.TokenType.Keyword:
            Representation = Token.Keyword;
            break;

          case Syntax.TokenType.Identifier:
            Representation = Token.Identifier;

            if (Representation.Length > 0 && (!SymbolTable.IdentifierCharacterSet.Conforms(Representation) || GrammarManager.ContainsKeyword(Representation) || SymbolTable.NumberCharacterSet.Contains(Representation[0])))
              Representation = SyntaxGrammar.IdentifierOpenQuote + Representation + SyntaxGrammar.IdentifierCloseQuote;
            break;

          case Syntax.TokenType.Comment:
            Representation = "//" + Token.Comment;
            break;

          case Syntax.TokenType.String:
            bool StringVerbatim;
            var TokenString = Token.String;

            if (TokenString == "")
            {
              Representation = "";
              StringVerbatim = false;
            }
            else
            {
              if (TokenString.Contains('\n') || TokenString.Contains('\r') || TokenString.Contains('\t') || TokenString.Contains(SyntaxGrammar.StringAroundQuote))
              {
                var StringBuilder = new StringBuilder();

                foreach (var TokenCharacter in TokenString)
                {
                  switch (TokenCharacter)
                  {
                    case '\r':
                      StringBuilder.Append(SyntaxGrammar.StringEscape + "r");
                      break;

                    case '\n':
                      StringBuilder.Append(SyntaxGrammar.StringEscape + "n");
                      break;

                    case '\t':
                      StringBuilder.Append(SyntaxGrammar.StringEscape + "t");
                      break;

                    default:
                      if (TokenCharacter == SyntaxGrammar.StringAroundQuote)
                        StringBuilder.Append(new string(SyntaxGrammar.StringEscape, 1) + new string(SyntaxGrammar.StringAroundQuote, 1));
                      else if (TokenCharacter == SyntaxGrammar.StringEscape)
                        StringBuilder.Append(new string(SyntaxGrammar.StringEscape, 2));
                      else
                        StringBuilder.Append(TokenCharacter);
                      break;
                  }
                }

                Representation = StringBuilder.ToString();

                StringVerbatim = false;
              }
              else
              {
                Representation = TokenString;
                StringVerbatim = Representation.Contains(SyntaxGrammar.StringEscape);

                if (!StringVerbatim)
                  Representation = Representation.Replace(SyntaxGrammar.StringEscape.ToString(), SyntaxGrammar.StringEscape.ToString() + SyntaxGrammar.StringEscape.ToString());
              }
            }

            Representation = SyntaxGrammar.StringAroundQuote + Representation + SyntaxGrammar.StringAroundQuote;

            if (StringVerbatim)
              Representation = SyntaxGrammar.StringVerbatim.ToString() + Representation;
            break;

          case Syntax.TokenType.Number:
            Representation = Token.Number;
            break;

          case TokenType.DateTime:
            Representation = SyntaxGrammar.IdentifierOpenQuote + SyntaxGrammar.DateTimePrefix + Token.DateTime.ToString("yyyy-MM-dd HH:mm:ss.ffffff zzz") + SyntaxGrammar.IdentifierCloseQuote;
            break;

          case TokenType.TimeSpan:
            Representation = SyntaxGrammar.IdentifierOpenQuote + SyntaxGrammar.TimeSpanPrefix + Token.TimeSpan.ToString() + SyntaxGrammar.IdentifierCloseQuote;
            break;

          case Syntax.TokenType.Base64:
            var Base64String = Convert.ToBase64String(Token.Base64.GetBuffer());

            if (SyntaxGrammar.UseBase64Prefix)
              Representation = "0b" + Base64String;
            else
              Representation = Base64String;
            break;

          case Syntax.TokenType.Hexadecimal:
            if (SyntaxGrammar.UseHexadecimalPrefix)
              Representation = Token.Hexadecimal.GetBuffer().ToHexadecimal("0x");
            else
              Representation = Token.Hexadecimal.GetBuffer().ToHexadecimal();
            break;

          default:
            Representation = SymbolTable.CharacterTokenTypeIndex.GetByRight(Token.Type).ToString();
            break;
        }

        ActiveWriter.Write(Representation);
      }
    }
    string WriteContract.Publish()
    {
      return string.Format("Line {0}", LineCount);
    }

    private int LineCount;
    private Grammar SyntaxGrammar;
    private GrammarManager GrammarManager;
    private TextWriter ActiveWriter;
  }

  public sealed class TextReadService : ReadContract
  {
    public TextReadService(TextReader ActiveReader, Grammar SyntaxGrammar, string FilePath = null)
    {
      this.ActiveReader = ActiveReader;
      this.SyntaxGrammar = SyntaxGrammar;
      this.FilePath = FilePath;
    }

    void ReadContract.Start()
    {
      GrammarManager = new GrammarManager(SyntaxGrammar);
      TextExtractor = new Inv.TextExtractor(ActiveReader);
      LineCount = 1;

      AdvanceToken();
    }
    void ReadContract.Stop()
    {
      GrammarManager = null;
      TextExtractor.Dispose();
      TextExtractor = null;
      NextToken = null;
      LineCount = 0;
    }
    bool ReadContract.More()
    {
      return NextToken != null;
    }
    ExtractException ReadContract.Interrupt(string Message)
    {
      if (FilePath != null)
        return new ExtractException(string.Format("{0} Line {1}: {2}", Path.GetFileName(FilePath), LineCount, Message), LineCount);
      else
        return new ExtractException(string.Format("Line {0}: {1}", LineCount, Message), LineCount);
    }
    Token ReadContract.PeekToken()
    {
      return NextToken;
    }
    Token ReadContract.ReadToken()
    {
      var Result = NextToken;

      AdvanceToken();

      return Result;
    }

    private void AdvanceToken()
    {
      NextToken = null;

      while (NextToken == null && TextExtractor.More())
      {
        var TokenCharacter = TextExtractor.NextCharacter();

        if (SymbolTable.WhitespaceCharacterSet.ContainsValue(TokenCharacter))
        {
          var WhitespaceString = TextExtractor.ConsumeWhileCharacterSet(SymbolTable.WhitespaceCharacterSet);

          foreach (var WhitespaceChar in WhitespaceString)
          {
            if (WhitespaceChar == '\n')
              LineCount++;
          }

          if (SyntaxGrammar.IgnoreWhitespace)
          {
            NextToken = null;
          }
          else
          {
            NextToken = new Token();
            NextToken.Type = TokenType.Whitespace;
            NextToken.Whitespace = WhitespaceString;
          }
        }
        else if (TextExtractor.NextIsString(SyntaxGrammar.SingleLineCommentPrefix))
        {
          TextExtractor.ConsumeString(SyntaxGrammar.SingleLineCommentPrefix);
          var SingleLineComment = TextExtractor.ConsumeUntilCharacterSet(SymbolTable.VerticalWhitespaceCharacterSet);

          if (SyntaxGrammar.IgnoreComment)
          {
            NextToken = null;
          }
          else
          {
            NextToken = new Token();
            NextToken.Type = TokenType.Comment;
            NextToken.Comment = SingleLineComment;
          }
        }
        else if (TextExtractor.NextIsString(SyntaxGrammar.MultiLineCommentStart))
        {
          TextExtractor.ConsumeString(SyntaxGrammar.MultiLineCommentStart);
          var MultiLineComment = TextExtractor.ConsumeUntilString(SyntaxGrammar.MultiLineCommentFinish);

          foreach (var MultiLineIndex in MultiLineComment)
          {
            if (MultiLineIndex == '\n')
              LineCount++;
          }

          if (SyntaxGrammar.IgnoreComment)
          {
            NextToken = null;
          }
          else
          {
            NextToken = new Token();
            NextToken.Type = TokenType.Comment;
            NextToken.Comment = MultiLineComment;
          }

          TextExtractor.ConsumeString(SyntaxGrammar.MultiLineCommentFinish);
        }
        else
        {
          NextToken = new Token();

          if (SymbolTable.NumberCharacterSet.ContainsValue(TokenCharacter))
          {
            var NumberValue = TextExtractor.ConsumeWhileCharacterSet(SymbolTable.NumberCharacterSet);

            if (SyntaxGrammar.UseHexadecimalPrefix && NumberValue == "0" && Char.ToLower(TextExtractor.NextCharacter()) == 'x')
            {
              TextExtractor.ConsumeCharacter();

              var HexadecimalString = TextExtractor.ConsumeWhileCharacterSet(SymbolTable.HexadecimalCharacterSet);
              var HexadecimalLength = HexadecimalString.Length;
              var HexadecimalBuffer = new byte[HexadecimalLength / 2];

              for (var HexadecimalIndex = 0; HexadecimalIndex < HexadecimalLength; HexadecimalIndex += 2)
                HexadecimalBuffer[HexadecimalIndex / 2] = Convert.ToByte(HexadecimalString.Substring(HexadecimalIndex, 2), 16);

              NextToken.Type = TokenType.Hexadecimal;
              NextToken.Hexadecimal = new Inv.Binary(HexadecimalBuffer, "");
            }
            else if (SyntaxGrammar.UseBase64Prefix && NumberValue == "0" && Char.ToLower(TextExtractor.NextCharacter()) == 'b')
            {
              TextExtractor.ConsumeCharacter();

              var Base64Buffer = Convert.FromBase64String(TextExtractor.ConsumeWhileCharacterSet(SymbolTable.Base64CharacterSet));

              NextToken.Type = TokenType.Base64;
              NextToken.Base64 = new Inv.Binary(Base64Buffer, "");
            }
            else
            {
              NextToken.Type = TokenType.Number;
              NextToken.Number = NumberValue;
            }
          }
          else if (SymbolTable.IdentifierCharacterSet.ContainsValue(TokenCharacter))
          {
            var IdentifierString = TextExtractor.ConsumeWhileCharacterSet(SymbolTable.IdentifierCharacterSet);

            if (GrammarManager.ContainsKeyword(IdentifierString))
            {
              NextToken.Type = TokenType.Keyword;

              if (SyntaxGrammar.KeywordCaseSensitive)
                NextToken.Keyword = IdentifierString;
              else
                NextToken.Keyword = IdentifierString.ToLower();
            }
            else
            {
              NextToken.Type = TokenType.Identifier;
              NextToken.Identifier = IdentifierString;
            }
          }
          else if (TokenCharacter == SyntaxGrammar.StringVerbatim)
          {
            TextExtractor.ConsumeCharacter(SyntaxGrammar.StringVerbatim);
            TextExtractor.ConsumeCharacter(SyntaxGrammar.StringAroundQuote);

            NextToken.Type = TokenType.String;
            NextToken.String = TextExtractor.ConsumeUntilCharacter(SyntaxGrammar.StringAroundQuote);

            TextExtractor.ConsumeCharacter(SyntaxGrammar.StringAroundQuote);
          }
          else if (TokenCharacter == SyntaxGrammar.StringAroundQuote)
          {
            TextExtractor.ConsumeCharacter(SyntaxGrammar.StringAroundQuote);

            var StringBuilder = new StringBuilder();

            while (TextExtractor.More() && TextExtractor.NextCharacter() != SyntaxGrammar.StringAroundQuote)
            {
              var StringEntry = TextExtractor.ConsumeCharacter();

              switch (StringEntry)
              {
                case '\r':
                case '\n':
                  if (!SyntaxGrammar.AllowMultiLineString)
                    throw new Exception(string.Format("String {0}{1}{0} was not correctly terminated.", SyntaxGrammar.StringAroundQuote, StringBuilder.ToString()));

                  if (StringEntry == '\n')
                    LineCount++;

                  StringBuilder.Append(StringEntry);
                  break;

                default:
                  if (StringEntry == SyntaxGrammar.StringEscape)
                  {
                    StringEntry = TextExtractor.ConsumeCharacter();

                    if (StringEntry == SyntaxGrammar.StringEscape)
                    {
                      StringBuilder.Append(StringEntry);
                    }
                    else
                    {
                      switch (StringEntry)
                      {
                        case 'r':
                          StringBuilder.Append('\r');
                          break;

                        case 'n':
                          StringBuilder.Append('\n');
                          break;

                        case 't':
                          StringBuilder.Append('\t');
                          break;

                        default:
                          if (StringEntry == SyntaxGrammar.StringAroundQuote)
                            StringBuilder.Append(StringEntry);
                          else
                            throw new Exception("Escaped character not supported: " + StringEntry);
                          break;
                      }
                    }
                  }
                  else
                  {
                    StringBuilder.Append(StringEntry);
                  }
                  break;
              }
            }

            NextToken.Type = TokenType.String;
            NextToken.String = StringBuilder.ToString();

            TextExtractor.ConsumeCharacter(SyntaxGrammar.StringAroundQuote);
          }
          else if (TokenCharacter == SyntaxGrammar.IdentifierOpenQuote)
          {
            TextExtractor.ConsumeCharacter(SyntaxGrammar.IdentifierOpenQuote);

            var IdentifierString = TextExtractor.ConsumeUntilCharacter(SyntaxGrammar.IdentifierCloseQuote);

            TextExtractor.ConsumeCharacter(SyntaxGrammar.IdentifierCloseQuote);

            if (SyntaxGrammar.DateTimePrefix != null && IdentifierString.StartsWith(SyntaxGrammar.DateTimePrefix))
            {
              NextToken.Type = TokenType.DateTime;
              NextToken.DateTime = DateTimeOffset.Parse(IdentifierString.Substring(SyntaxGrammar.DateTimePrefix.Length));
            }
            else if (SyntaxGrammar.TimeSpanPrefix != null && IdentifierString.StartsWith(SyntaxGrammar.TimeSpanPrefix))
            {
              NextToken.Type = TokenType.TimeSpan;
              NextToken.TimeSpan = TimeSpan.Parse(IdentifierString.Substring(SyntaxGrammar.TimeSpanPrefix.Length));
            }
            else
            {
              NextToken.Type = TokenType.Identifier;
              NextToken.Identifier = IdentifierString;
            }
          }
          else
          {
            TextExtractor.ConsumeCharacter(TokenCharacter);

            NextToken.Type = SymbolTable.CharacterTokenTypeIndex.GetByLeft(TokenCharacter);
          }
        }
      }
    }

    private GrammarManager GrammarManager;
    private Grammar SyntaxGrammar;
    private Inv.TextExtractor TextExtractor;
    private TextReader ActiveReader;
    private int LineCount;
    private Token NextToken;
    private string FilePath;
  }

  public sealed class Grammar
  {
    public Grammar()
    {
      this.Keyword = new Inv.DistinctList<string>();
    }

    public Inv.DistinctList<string> Keyword { get; private set; }
    public bool IgnoreWhitespace;
    public bool IgnoreComment;
    public bool KeywordCaseSensitive;
    public bool AllowMultiLineString;
    public bool UseHexadecimalPrefix;
    public bool UseBase64Prefix;
    public string SingleLineCommentPrefix;
    public string MultiLineCommentStart;
    public string MultiLineCommentFinish;
    public char StringAroundQuote;
    public char StringEscape;
    public char StringVerbatim;
    public char IdentifierOpenQuote;
    public char IdentifierCloseQuote;
    public string DateTimePrefix;
    public string TimeSpanPrefix;
  }

  public enum TokenType
  {
    Newline,
    Whitespace,
    Comment,
    Identifier,
    Keyword,
    Number,
    String,
    DateTime,
    TimeSpan,
    Hexadecimal,
    Base64,
    Colon, Semicolon, Comma, Period, EqualSign, Plus, Minus, Asterisk, DollarSign,
    Caret, Tilde, ExclamationPoint, QuestionMark, Pipe, Ampersand, Percent, ForwardSlash, BackSlash, At,
    OpenRound, CloseRound, OpenBrace, CloseBrace, OpenAngle, CloseAngle, Hash, SingleQuote, DoubleQuote, BackQuote,
  }

  public sealed class Token
  {
    public TokenType Type;
    public int Newline;
    public string Whitespace;
    public string Comment;
    public string Identifier;
    public string Keyword;
    public string Number;
    public string String;
    public DateTimeOffset DateTime;
    public TimeSpan TimeSpan;
    public Inv.Binary Hexadecimal;
    public Inv.Binary Base64;
  }

  public sealed class GrammarManager
  {
    public GrammarManager(Grammar SyntaxGrammar)
    {
      if (SyntaxGrammar.KeywordCaseSensitive)
        KeywordHashSet = new HashSet<string>(StringComparer.CurrentCulture);
      else
        KeywordHashSet = new HashSet<string>(StringComparer.CurrentCultureIgnoreCase);

      foreach (var Keyword in SyntaxGrammar.Keyword)
      {
        if (!KeywordHashSet.Add(Keyword))
          throw new Exception("Duplicate keyword: " + Keyword);
      }
    }

    public bool ContainsKeyword(string Keyword)
    {
      return KeywordHashSet.Contains(Keyword);
    }

    private HashSet<string> KeywordHashSet = null;
  }

  public sealed class Formatter
  {
    public Formatter()
    {
    }
    public Formatter(WriteContract WriteContract)
    {
      this.WriteContract = WriteContract;
    }

    public WriteContract WriteContract { get; set; }

    public void Start()
    {
      WriteContract.Start();

      IndentLevel = 0;
    }
    public void Stop()
    {
      Debug.Assert(IndentLevel == 0, "IndentLevel must return to zero on finish.");

      WriteContract.Stop();
    }
    public void IncreaseIndent()
    {
      IndentLevel++;
    }
    public void DecreaseIndent()
    {
      Debug.Assert(IndentLevel > 0, "IndentLevel must be greater than zero.");

      IndentLevel--;
    }
    public void ProduceToken(TokenType SyntaxTokenType)
    {
      WriteToken.Type = SyntaxTokenType;

      WriteContract.WriteToken(WriteToken);
    }
    public void ProduceIdentifierChainStringList(IList<string> IdentifierList, TokenType SeparatorTokenType)
    {
      var IdentifierCount = IdentifierList.Count;

      for (var IdentifierIndex = 0; IdentifierIndex < IdentifierCount; IdentifierIndex++)
      {
        ProduceIdentifier(IdentifierList[IdentifierIndex]);

        if (IdentifierIndex != IdentifierCount - 1)
          ProduceToken(SeparatorTokenType);
      }
    }
    public void ProduceIdentifier(string IdentifierValue)
    {
      Debug.Assert(IdentifierValue != null, "IdentifierValue must not be null.");

      WriteToken.Type = TokenType.Identifier;
      WriteToken.Identifier = IdentifierValue;
      WriteContract.WriteToken(WriteToken);
    }
    public void ProduceKeyword(string KeywordValue)
    {
      Debug.Assert(KeywordValue != null, "KeywordValue must not be null.");

      WriteToken.Type = TokenType.Keyword;
      WriteToken.Keyword = KeywordValue;
      WriteContract.WriteToken(WriteToken);
    }
    public void ProduceString(string StringValue)
    {
      Debug.Assert(StringValue != null, "StringValue must not be null.");

      WriteToken.Type = TokenType.String;
      WriteToken.String = StringValue;
      WriteContract.WriteToken(WriteToken);
    }
    public void ProduceCharacter(char CharacterValue)
    {
      ProduceString(CharacterValue.ToString());
    }
    public void ProduceInteger(long IntegerValue)
    {
      ProduceNumber(IntegerValue.ToString());
    }
    public void ProduceDecimal(decimal DecimalValue)
    {
      ProduceNumber(DecimalValue.ToString());
    }
    public void ProduceReal(double RealValue)
    {
      ProduceNumber(RealValue.ToString());
    }
    public void ProduceBoolean(bool BooleanValue)
    {
      switch (BooleanValue)
      {
        case true:
          ProduceIdentifier("true");
          break;

        case false:
          ProduceIdentifier("false");
          break;

        default:
          throw new FormatException("Unhandled boolean value.");
      }
    }
    public void ProduceDateTimeOffset(DateTimeOffset DateTimeValue)
    {
      WriteToken.Type = TokenType.DateTime;
      WriteToken.DateTime = DateTimeValue;
      WriteContract.WriteToken(WriteToken);
    }
    public void ProduceTimeSpan(TimeSpan TimeSpanValue)
    {
      WriteToken.Type = TokenType.TimeSpan;
      WriteToken.TimeSpan = TimeSpanValue;
      WriteContract.WriteToken(WriteToken);
    }
    public void ProduceBase64(byte[] BinaryValue)
    {
      Debug.Assert(BinaryValue != null, "BinaryValue must not be null.");

      WriteToken.Type = TokenType.Base64;
      WriteToken.Base64 = new Inv.Binary(BinaryValue, "");
      WriteContract.WriteToken(WriteToken);
    }
    public void ProduceHexadecimal(byte[] BinaryValue)
    {
      Debug.Assert(BinaryValue != null, "BinaryValue must not be null.");

      WriteToken.Type = TokenType.Hexadecimal;
      WriteToken.Hexadecimal = new Inv.Binary(BinaryValue, "");
      WriteContract.WriteToken(WriteToken);
    }
    public void ProduceColour(Inv.Colour Colour)
    {
      //var ShadeKnownName = Colour.ConvertToKnownName();

      //if (string.IsNullOrEmpty(ShadeKnownName))
      ProduceHexadecimal(BitConverter.GetBytes(Colour.RawValue));
      //else
      //  ProduceIdentifier(ShadeKnownName);
    }
    public void ProduceWhitespace(string WhitespaceValue)
    {
      Debug.Assert(WhitespaceValue != null, "WhitespaceValue must not be null.");

      WriteToken.Type = TokenType.Whitespace;
      WriteToken.Whitespace = WhitespaceValue;
      WriteContract.WriteToken(WriteToken);
    }
    public void ProduceOpenBrace()
    {
      ProduceToken(TokenType.OpenBrace);
    }
    public void ProduceCloseBrace()
    {
      ProduceToken(TokenType.CloseBrace);
    }
    public void ProduceOpenRound()
    {
      ProduceToken(TokenType.OpenRound);
    }
    public void ProduceCloseRound()
    {
      ProduceToken(TokenType.CloseRound);
    }
    public void ProducePercent()
    {
      ProduceToken(TokenType.Percent);
    }
    public void ProduceOpenAngle()
    {
      ProduceToken(TokenType.OpenAngle);
    }
    public void ProduceCloseAngle()
    {
      ProduceToken(TokenType.CloseAngle);
    }
    public void ProduceSemicolon()
    {
      ProduceToken(TokenType.Semicolon);
    }
    public void ProduceColon()
    {
      ProduceToken(TokenType.Colon);
    }
    public void ProduceComma()
    {
      ProduceToken(TokenType.Comma);
    }
    public void ProducePeriod()
    {
      ProduceToken(TokenType.Period);
    }
    public void ProduceEqualSign()
    {
      ProduceToken(TokenType.EqualSign);
    }
    public void ProduceExclamationPoint()
    {
      ProduceToken(TokenType.ExclamationPoint);
    }
    public void ProduceQuestionMark()
    {
      ProduceToken(TokenType.QuestionMark);
    }
    public void ProduceAt()
    {
      ProduceToken(TokenType.At);
    }
    public void ProducePlus()
    {
      ProduceToken(TokenType.Plus);
    }
    public void ProduceMinus()
    {
      ProduceToken(TokenType.Minus);
    }
    public void ProduceHash()
    {
      ProduceToken(TokenType.Hash);
    }
    public void ProduceAsterisk()
    {
      ProduceToken(TokenType.Asterisk);
    }
    public void ProduceAmpersand()
    {
      ProduceToken(TokenType.Ampersand);
    }
    public void ProducePipe()
    {
      ProduceToken(TokenType.Pipe);
    }
    public void ProduceForwardSlash()
    {
      ProduceToken(TokenType.ForwardSlash);
    }
    public void ProduceBackSlash()
    {
      ProduceToken(TokenType.BackSlash);
    }
    public void ProduceCaret()
    {
      ProduceToken(TokenType.Caret);
    }
    public void ProduceTilde()
    {
      ProduceToken(TokenType.Tilde);
    }
    public void ProduceDoubleQuote()
    {
      ProduceToken(TokenType.DoubleQuote);
    }
    public void ProduceSpace()
    {
      ProduceWhitespace(" ");
    }
    public void ProduceLine()
    {
      WriteToken.Type = TokenType.Newline;
      WriteToken.Newline = 1;
      WriteContract.WriteToken(WriteToken);
    }
    public void ProduceIndent()
    {
      Debug.Assert(IndentLevel >= 0, "IndentLevel must be greater than zero.");

      ProduceWhitespace(new string(' ', IndentLevel * 2));
    }
    public void ProduceLineAndIndent()
    {
      ProduceLine();
      ProduceIndent();
    }
    public void ProduceOpenScope()
    {
      ProduceIndent();
      ProduceOpenBrace();
      IncreaseIndent();
      ProduceLine();
    }
    public void ProduceCloseScope()
    {
      DecreaseIndent();
      ProduceIndent();
      ProduceCloseBrace();
      ProduceLine();
    }
    public void ProduceOpenScopeString(string Keyword, string Identifier)
    {
      ProduceIndent();
      ProduceKeyword(Keyword);
      ProduceSpace();
      ProduceString(Identifier);
      ProduceLine();
      ProduceOpenScope();
    }
    public void ProduceOpenScopeIdentifier(string Keyword, string Identifier)
    {
      ProduceIndent();
      ProduceKeyword(Keyword);
      ProduceSpace();
      ProduceIdentifier(Identifier);
      ProduceLine();
      ProduceOpenScope();
    }
    public void ProduceOpenScopeKeyword(string Keyword)
    {
      ProduceIndent();
      ProduceKeyword(Keyword);
      ProduceLine();
      ProduceOpenScope();
    }
    public void ProduceOpenCollectionKeyword(string Keyword)
    {
      ProduceIndent();
      ProduceKeyword(Keyword);
      ProduceLineAndIndent();
      ProduceOpenRound();
      IncreaseIndent();
      ProduceLine();
    }
    public void ProduceOpenCollectionIdentifier(string Keyword, string Identifier)
    {
      ProduceIndent();
      ProduceKeyword(Keyword);
      ProduceSpace();
      ProduceIdentifier(Identifier);
      ProduceLineAndIndent();
      ProduceOpenRound();
      IncreaseIndent();
      ProduceLine();
    }
    public void ProduceCloseCollection()
    {
      DecreaseIndent();
      ProduceIndent();
      ProduceCloseRound();
      ProduceLine();
    }
    public void ProduceOptionalStringAttribute(string Keyword, string StringValue)
    {
      if (StringValue != null)
      {
        ProduceIndent();
        ProduceKeyword(Keyword);
        ProduceSpace();
        ProduceEqualSign();
        ProduceSpace();
        ProduceString(StringValue);
        ProduceSemicolon();
        ProduceLine();
      }
    }
    public void ProduceOptionalDateTimeAttribute(string Keyword, DateTimeOffset? DateTimeValue)
    {
      if (DateTimeValue.HasValue)
      {
        ProduceIndent();
        ProduceKeyword(Keyword);
        ProduceSpace();
        ProduceEqualSign();
        ProduceSpace();
        ProduceDateTimeOffset(DateTimeValue.Value);
        ProduceSemicolon();
        ProduceLine();
      }
    }
    public void ProduceOptionalDecimalAttribute(string Keyword, decimal? DecimalValue)
    {
      if (DecimalValue.HasValue)
      {
        ProduceIndent();
        ProduceKeyword(Keyword);
        ProduceSpace();
        ProduceEqualSign();
        ProduceSpace();
        ProduceDecimal(DecimalValue.Value);
        ProduceSemicolon();
        ProduceLine();
      }
    }
    public void ProduceOptionalIntegerAttribute(string Keyword, long? IntegerValue)
    {
      if (IntegerValue.HasValue)
      {
        ProduceIndent();
        ProduceKeyword(Keyword);
        ProduceSpace();
        ProduceEqualSign();
        ProduceSpace();
        ProduceInteger(IntegerValue.Value);
        ProduceSemicolon();
        ProduceLine();
      }
    }
    public void ProduceOptionalBooleanAttribute(string Keyword, bool? BooleanValue)
    {
      if (BooleanValue.HasValue && BooleanValue.Value)
      {
        ProduceIndent();
        ProduceKeyword(Keyword);
        ProduceSpace();
        ProduceEqualSign();
        ProduceSpace();
        ProduceBoolean(BooleanValue.Value);
        ProduceSemicolon();
        ProduceLine();
      }
    }
    public void ProduceOptionalIdentifierAttribute(string Keyword, string IdentifierValue)
    {
      if (IdentifierValue != null)
      {
        ProduceIndent();
        ProduceKeyword(Keyword);
        ProduceSpace();
        ProduceEqualSign();
        ProduceSpace();
        ProduceIdentifier(IdentifierValue);
        ProduceSemicolon();
        ProduceLine();
      }
    }

    private void ProduceNumber(string NumberValue)
    {
      Debug.Assert(NumberValue != null, "NumberValue must not be null.");

      WriteToken.Type = TokenType.Number;
      WriteToken.Number = NumberValue;
      WriteContract.WriteToken(WriteToken);
    }

    private int IndentLevel;
    private Token WriteToken = new Token();
  }

  public sealed class Extractor
  {
    public Extractor()
    {
    }
    public Extractor(ReadContract ReadContract)
    {
      this.ReadContract = ReadContract;
    }

    public ReadContract ReadContract { get; set; }

    public void Start()
    {
      ReadContract.Start();
    }
    public void Stop()
    {
      if (ReadContract.More())
        throw ReadContract.Interrupt("The syntax document was longer than is valid.");

      ReadContract.Stop();
    }
    public bool More()
    {
      return ReadContract.More();
    }
    public ExtractException Interrupt(string Message)
    {
      return ReadContract.Interrupt(Message);
    }
    public TokenType? PeekTokenType()
    {
      var Token = ReadContract.PeekToken();

      if (Token == null)
        return null;
      else
        return Token.Type;
    }
    public bool NextIsTokenType(TokenType TokenType)
    {
      var SyntaxToken = ReadContract.PeekToken();

      return SyntaxToken != null && SyntaxToken.Type == TokenType;
    }
    public bool NextIsTokenTypeArray(params TokenType[] TokenTypeArray)
    {
      var SyntaxToken = ReadContract.PeekToken();

      return SyntaxToken != null && TokenTypeArray.Contains(SyntaxToken.Type);
    }
    public Token ConsumeToken()
    {
      return ReadContract.ReadToken();
    }
    public TokenType ConsumeTokenTypeArray(params TokenType[] TokenTypeArray)
    {
      var SyntaxToken = ReadContract.PeekToken();

      if (SyntaxToken == null)
        throw ReadContract.Interrupt(string.Format("expected token '{0}' but found the end of the document.", TokenTypeArray.Select(Index => Index.ToString()).AsSeparatedText(", ")));

      if (!TokenTypeArray.Contains(SyntaxToken.Type))
        throw ReadContract.Interrupt(string.Format("expected token '{0}' but found token '{1}'.", TokenTypeArray.Select(Index => Index.ToString()).AsSeparatedText(", "), SyntaxToken.Type));

      return ReadContract.ReadToken().Type;
    }
    public void ConsumeTokenType(TokenType TokenType)
    {
      var SyntaxToken = ReadContract.PeekToken();

      if (SyntaxToken == null)
        throw ReadContract.Interrupt(string.Format("expected token '{0}' but found the end of the document.", TokenType));

      if (TokenType != SyntaxToken.Type)
        throw ReadContract.Interrupt(string.Format("expected token '{0}' but found token '{1}'.", TokenType, SyntaxToken.Type));

      ReadContract.ReadToken();
    }
    public bool ConsumeOptionalTokenType(TokenType TokenType)
    {
      var Result = NextIsTokenType(TokenType);

      if (Result)
        ReadContract.ReadToken();

      return Result;
    }
    public bool NextIsIdentifierValue(string ExpectedIdentifier)
    {
      var SyntaxToken = ReadContract.PeekToken();

      return SyntaxToken != null && SyntaxToken.Type == TokenType.Identifier && SyntaxToken.Identifier == ExpectedIdentifier;
    }
    public bool NextIsIdentifier()
    {
      return NextIsTokenType(TokenType.Identifier);
    }
    public bool NextIsString()
    {
      return NextIsTokenType(TokenType.String);
    }
    public bool NextIsNumber()
    {
      return NextIsTokenType(TokenType.Number);
    }
    public bool NextIsBoolean()
    {
      if (NextIsIdentifier())
      {
        var SyntaxToken = ReadContract.PeekToken();
        bool result;
        return bool.TryParse(SyntaxToken.Identifier, out result);
      }
      else
        return false;
    }
    public string PeekIdentifier()
    {
      return PeekToken(TokenType.Identifier).Identifier;
    }
    public string ConsumeIdentifier()
    {
      var Result = PeekToken(TokenType.Identifier).Identifier;

      ReadContract.ReadToken();

      return Result;
    }
    public bool ConsumeOptionalIdentifierValue(string ExpectedIdentifier)
    {
      var Result = NextIsIdentifierValue(ExpectedIdentifier);

      if (Result)
        ReadContract.ReadToken();

      return Result;
    }
    public string ConsumeIdentifierValue(string ExpectedIdentifier)
    {
      return ConsumeIdentifierValueArray(ExpectedIdentifier);
    }
    public string ConsumeIdentifierValueArray(params string[] ExpectedIdentifierArray)
    {
      var Result = PeekToken(TokenType.Identifier).Identifier;

      if (!ExpectedIdentifierArray.Contains(Result))
        throw ReadContract.Interrupt(string.Format("expected identifier '{0}' but found identifier '{1}'.", ExpectedIdentifierArray.AsSeparatedText(" or "), Result));

      ReadContract.ReadToken();

      return Result;
    }
    public string ConsumeIdentifierChainString(TokenType SeparatorTokenType)
    {
      var Result = new StringBuilder(ConsumeIdentifier());
      var Separator = SymbolTable.CharacterTokenTypeIndex.GetByRight(SeparatorTokenType);

      while (NextIsTokenType(SeparatorTokenType))
      {
        ConsumeToken();

        Result.Append(Separator + ConsumeIdentifier());
      }

      return Result.ToString();
    }
    public List<string> ConsumeIdentifierChainStringList(TokenType SeparatorTokenType)
    {
      var Result = new List<string>();
      Result.Add(ConsumeIdentifier());

      while (NextIsTokenType(SeparatorTokenType))
      {
        ConsumeToken();

        Result.Add(ConsumeIdentifier());
      }

      return Result;
    }
    public string ConsumeIdentifierOrKeyword()
    {
      return (PeekTokenType() == TokenType.Identifier ? ConsumeIdentifier() : ConsumeKeyword());
    }
    public string ConsumeIdentifierOrNumber()
    {
      return (PeekTokenType() == TokenType.Identifier ? ConsumeIdentifier() : ConsumeNumber());
    }
    public string PeekKeyword()
    {
      return PeekToken(TokenType.Keyword).Keyword;
    }
    public bool NextIsKeywordValue(string ExpectedKeyword)
    {
      var SyntaxToken = ReadContract.PeekToken();

      return SyntaxToken != null && SyntaxToken.Type == TokenType.Keyword && SyntaxToken.Keyword == ExpectedKeyword;
    }
    public bool NextIsNumberValue(string ExpectedNumber)
    {
      var SyntaxToken = ReadContract.PeekToken();

      return SyntaxToken != null && SyntaxToken.Type == TokenType.Number && SyntaxToken.Number == ExpectedNumber;
    }
    public bool NextIsKeyword()
    {
      return NextIsTokenType(TokenType.Keyword);
    }
    public bool NextIsKeywordValueList(params string[] KeywordArray)
    {
      var NextKeywordFound = false;

      if (NextIsKeyword())
      {
        string Keyword = PeekKeyword();

        int KeywordIndex = 0;
        while (!NextKeywordFound && (KeywordIndex < KeywordArray.Count()))
        {
          NextKeywordFound = KeywordArray[KeywordIndex] == Keyword;
          KeywordIndex++;
        }
      }

      return NextKeywordFound;
    }
    public string ConsumeKeyword()
    {
      var Result = PeekToken(TokenType.Keyword).Keyword;

      ReadContract.ReadToken();

      return Result;
    }
    public void ConsumeKeywordValue(string ExpectedKeyword)
    {
      ConsumeKeywordValueArray(ExpectedKeyword);
    }
    public string ConsumeKeywordValueArray(params string[] ExpectedKeywordArray)
    {
      var Result = PeekToken(TokenType.Keyword).Keyword;

      if (!ExpectedKeywordArray.Contains(Result))
        throw ReadContract.Interrupt(string.Format("expected keyword '{0}' but found keyword '{1}'.", ExpectedKeywordArray.AsSeparatedText(" or "), Result));

      ReadContract.ReadToken();

      return Result;
    }
    public void ConsumeNumberValue(string ExpectedNumber)
    {
      ConsumeNumberValueArray(ExpectedNumber);
    }
    public string ConsumeNumberValueArray(params string[] ExpectedNumberArray)
    {
      var Result = PeekToken(TokenType.Number).Number;

      if (!ExpectedNumberArray.Contains(Result))
        throw ReadContract.Interrupt(string.Format("expected number '{0}' but found number '{1}'.", ExpectedNumberArray.AsSeparatedText(" or "), Result));

      ReadContract.ReadToken();

      return Result;
    }
    public bool ConsumeOptionalKeywordValue(string ExpectedKeyword)
    {
      var Result = NextIsKeywordValue(ExpectedKeyword);

      if (Result)
        ConsumeKeywordValue(ExpectedKeyword);

      return Result;
    }
    public bool ConsumeOptionalNumberValue(string ExpectedNumber)
    {
      var Result = NextIsNumberValue(ExpectedNumber);

      if (Result)
        ConsumeNumberValue(ExpectedNumber);

      return Result;
    }
    public string ConsumeString()
    {
      var Result = PeekToken(TokenType.String).String;

      ReadContract.ReadToken();

      return Result;
    }
    public char ConsumeCharacter()
    {
      return ConsumeString()[0];
    }
    public System.DateTimeOffset ConsumeDateTimeOffset()
    {
      var Result = PeekToken(TokenType.DateTime).DateTime;

      ReadContract.ReadToken();

      return Result;
    }
    public System.DateTime ConsumeDateTime()
    {
      var Result = PeekToken(TokenType.DateTime).DateTime.DateTime;

      ReadContract.ReadToken();

      return Result;
    }
    public System.TimeSpan ConsumeTimeSpan()
    {
      var Result = PeekToken(TokenType.TimeSpan).TimeSpan;

      ReadContract.ReadToken();

      return Result;
    }
    public bool ConsumeBoolean()
    {
      var Identifier = ConsumeIdentifier();

      if ("true".Equals(Identifier, StringComparison.OrdinalIgnoreCase))
        return true;

      if ("false".Equals(Identifier, StringComparison.OrdinalIgnoreCase))
        return false;

      throw ReadContract.Interrupt(string.Format("Identifier '{0}' is not an expected boolean value.", Identifier));
    }
    public string ConsumeNumber()
    {
      var Result = PeekToken(TokenType.Number).Number;

      ReadContract.ReadToken();

      return Result;
    }
    public double ConsumeReal()
    {
      var RealText = ConsumeNumber();

      if (ConsumeOptionalTokenType(TokenType.Period))
        RealText += "." + ConsumeNumber();

      return Convert.ToDouble(RealText);
    }
    public long ConsumeInteger64()
    {
      return Convert.ToInt64(ConsumeOptionalMinus() ? "-" + ConsumeNumber() : ConsumeNumber());
    }
    public int ConsumeInteger32()
    {
      return Convert.ToInt32(ConsumeOptionalMinus() ? "-" + ConsumeNumber() : ConsumeNumber());
    }
    public decimal ConsumeDecimal()
    {
      var Sign = 1;
      if (ConsumeOptionalTokenType(TokenType.Minus))
        Sign = -1;

      var DecimalText = ConsumeNumber();
      if (ConsumeOptionalTokenType(TokenType.Period))
        DecimalText += "." + ConsumeNumber();

      return Convert.ToDecimal(DecimalText) * Sign;
    }
    public byte[] ConsumeBase64()
    {
      var Result = PeekToken(TokenType.Base64).Base64.GetBuffer();

      ReadContract.ReadToken();

      return Result;
    }
    public byte[] ConsumeHexadecimal()
    {
      var Result = PeekToken(TokenType.Hexadecimal).Hexadecimal.GetBuffer();

      ReadContract.ReadToken();

      return Result;
    }
    public Inv.Colour ConsumeColour()
    {
      //if (NextIsIdentifier())
      //return Inv.Support.ColourHelper.ConvertToAdvColour(ConsumeIdentifier());
      //return Extractor.ConsumeIdentifier().ConvertToAdvColour(); // TODO: why does this demand assembly references while the above form does not?
      //else
      return new Inv.Colour(BitConverter.ToInt32(ConsumeHexadecimal(), 0));
    }
    public void ConsumeOpenScope()
    {
      ConsumeOpenBrace();
    }
    public void ConsumeCloseScope()
    {
      ConsumeCloseBrace();
    }
    public void ConsumeOpenBrace()
    {
      ConsumeTokenType(TokenType.OpenBrace);
    }
    public void ConsumeCloseBrace()
    {
      ConsumeTokenType(TokenType.CloseBrace);
    }
    public void ConsumeOpenAngle()
    {
      ConsumeTokenType(TokenType.OpenAngle);
    }
    public void ConsumeCloseAngle()
    {
      ConsumeTokenType(TokenType.CloseAngle);
    }
    public void ConsumeOpenRound()
    {
      ConsumeTokenType(TokenType.OpenRound);
    }
    public void ConsumeCloseRound()
    {
      ConsumeTokenType(TokenType.CloseRound);
    }
    public void ConsumeSemicolon()
    {
      ConsumeTokenType(TokenType.Semicolon);
    }
    public void ConsumeColon()
    {
      ConsumeTokenType(TokenType.Colon);
    }
    public void ConsumePercent()
    {
      ConsumeTokenType(TokenType.Percent);
    }
    public void ConsumeEqualSign()
    {
      ConsumeTokenType(TokenType.EqualSign);
    }
    public void ConsumeExclamationPoint()
    {
      ConsumeTokenType(TokenType.ExclamationPoint);
    }
    public void ConsumeQuestionMark()
    {
      ConsumeTokenType(TokenType.QuestionMark);
    }
    public void ConsumeAmpersand()
    {
      ConsumeTokenType(TokenType.Ampersand);
    }
    public void ConsumePipe()
    {
      ConsumeTokenType(TokenType.Pipe);
    }
    public void ConsumeComma()
    {
      ConsumeTokenType(TokenType.Comma);
    }
    public void ConsumePeriod()
    {
      ConsumeTokenType(TokenType.Period);
    }
    public void ConsumePlus()
    {
      ConsumeTokenType(TokenType.Plus);
    }
    public void ConsumeMinus()
    {
      ConsumeTokenType(TokenType.Minus);
    }
    public void ConsumeHash()
    {
      ConsumeTokenType(TokenType.Hash);
    }
    public void ConsumeForwardSlash()
    {
      ConsumeTokenType(TokenType.ForwardSlash);
    }
    public void ConsumeBackSlash()
    {
      ConsumeTokenType(TokenType.BackSlash);
    }
    public void ConsumeCaret()
    {
      ConsumeTokenType(TokenType.Caret);
    }
    public void ConsumeTilde()
    {
      ConsumeTokenType(TokenType.Tilde);
    }
    public void ConsumeAsterisk()
    {
      ConsumeTokenType(TokenType.Asterisk);
    }
    public void ConsumeAt()
    {
      ConsumeTokenType(TokenType.At);
    }
    public void ConsumeDollarSign()
    {
      ConsumeTokenType(TokenType.DollarSign);
    }
    public string ConsumeWhiteSpace()
    {
      var Result = PeekToken(TokenType.Whitespace).Whitespace;

      ReadContract.ReadToken();

      return Result;
    }
    public string ConsumeOpenScopeString(string Keyword)
    {
      ConsumeKeywordValue(Keyword);
      var Identifier = ConsumeString();
      ConsumeOpenBrace();
      return Identifier;
    }
    public string ConsumeOpenScopeIdentifier(string Keyword)
    {
      ConsumeKeywordValue(Keyword);
      var Identifier = ConsumeIdentifier();
      ConsumeOpenBrace();
      return Identifier;
    }
    public void ConsumeOpenScopeKeyword(string Keyword)
    {
      ConsumeKeywordValue(Keyword);
      ConsumeOpenBrace();
    }
    public void ConsumeOpenCollectionKeyword(string Keyword)
    {
      ConsumeKeywordValue(Keyword);
      ConsumeOpenRound();
    }
    public string ConsumeOpenCollectionIdentifier(string Keyword)
    {
      ConsumeKeywordValue(Keyword);
      var Identifier = ConsumeIdentifier();
      ConsumeOpenRound();
      return Identifier;
    }
    public string ConsumeOptionalStringAttribute(string Keyword)
    {
      if (NextIsKeywordValue(Keyword))
      {
        ConsumeKeywordValue(Keyword);
        ConsumeEqualSign();
        var StringValue = ConsumeString();
        ConsumeSemicolon();
        return StringValue;
      }
      else
      {
        return null;
      }
    }
    public string ConsumeOptionalIdentifierAttribute(string Keyword)
    {
      if (NextIsKeywordValue(Keyword))
      {
        ConsumeKeywordValue(Keyword);
        ConsumeEqualSign();
        var StringValue = ConsumeIdentifier();
        ConsumeSemicolon();
        return StringValue;
      }
      else
      {
        return null;
      }
    }
    public DateTimeOffset? ConsumeOptionalDateTimeAttribute(string Keyword)
    {
      if (NextIsKeywordValue(Keyword))
      {
        ConsumeKeywordValue(Keyword);
        ConsumeEqualSign();
        var DateTimeValue = ConsumeDateTime();
        ConsumeSemicolon();
        return DateTimeValue;
      }
      else
      {
        return null;
      }
    }
    public decimal? ConsumeOptionalDecimalAttribute(string Keyword)
    {
      if (NextIsKeywordValue(Keyword))
      {
        ConsumeKeywordValue(Keyword);
        ConsumeEqualSign();
        var DecimalValue = ConsumeDecimal();
        ConsumeSemicolon();
        return DecimalValue;
      }
      else
      {
        return null;
      }
    }
    public long? ConsumeOptionalIntegerAttribute(string Keyword)
    {
      if (NextIsKeywordValue(Keyword))
      {
        ConsumeKeywordValue(Keyword);
        ConsumeEqualSign();
        var IntegerValue = ConsumeInteger64();
        ConsumeSemicolon();
        return IntegerValue;
      }
      else
      {
        return null;
      }
    }
    public bool? ConsumeOptionalBooleanAttribute(string Keyword)
    {
      if (NextIsKeywordValue(Keyword))
      {
        ConsumeKeywordValue(Keyword);
        ConsumeEqualSign();
        var BooleanValue = ConsumeBoolean();
        ConsumeSemicolon();
        return BooleanValue;
      }
      else
      {
        return null;
      }
    }
    public bool ConsumeOptionalComma()
    {
      return ConsumeOptionalTokenType(TokenType.Comma);
    }
    public bool ConsumeOptionalColon()
    {
      return ConsumeOptionalTokenType(TokenType.Colon);
    }
    public bool ConsumeOptionalMinus()
    {
      return ConsumeOptionalTokenType(TokenType.Minus);
    }
    public bool ConsumeOptionalPipe()
    {
      return ConsumeOptionalTokenType(TokenType.Pipe);
    }
    public bool ConsumeOptionalQuestionMark()
    {
      return ConsumeOptionalTokenType(TokenType.QuestionMark);
    }

    private Token PeekToken(TokenType TokenType)
    {
      var SyntaxToken = ReadContract.PeekToken();

      if (SyntaxToken == null)
        throw ReadContract.Interrupt(string.Format("expected token '{0}' but found the end of the document.", TokenType));

      if (SyntaxToken.Type != TokenType)
        throw ReadContract.Interrupt(string.Format("expected token '{0}' but found token '{1}'.", TokenType, SyntaxToken.Type));

      return SyntaxToken;
    }
  }

  public sealed class FormatException : Exception
  {
    public FormatException(string message)
      : base(message)
    {
    }
  }

  public sealed class ExtractException : Exception
  {
    public ExtractException(string message, long? Location)
      : base(message)
    {
      this.Location = Location;
    }
    public ExtractException(string message, long? Location, Exception innerException)
      : base(message, innerException)
    {
      this.Location = Location;
    }

    public long? Location { get; private set; }
  }

  public static class SymbolTable
  {
    public static readonly Inv.Bidictionary<char, TokenType> CharacterTokenTypeIndex = new Inv.Bidictionary<char, TokenType>();
    public static readonly Inv.CharacterSet WhitespaceCharacterSet = new Inv.CharacterSet();
    public static readonly Inv.CharacterSet IdentifierCharacterSet = new Inv.CharacterSet();
    public static readonly Inv.CharacterSet NumberCharacterSet = new Inv.CharacterSet();
    public static readonly Inv.CharacterSet HexadecimalCharacterSet = new Inv.CharacterSet();
    public static readonly Inv.CharacterSet Base64CharacterSet = new Inv.CharacterSet();
    public static readonly Inv.CharacterSet VerticalWhitespaceCharacterSet = new Inv.CharacterSet();

    static SymbolTable()
    {
      CharacterTokenTypeIndex.Add('(', TokenType.OpenRound);
      CharacterTokenTypeIndex.Add(')', TokenType.CloseRound);
      CharacterTokenTypeIndex.Add('<', TokenType.OpenAngle);
      CharacterTokenTypeIndex.Add('>', TokenType.CloseAngle);
      CharacterTokenTypeIndex.Add('{', TokenType.OpenBrace);
      CharacterTokenTypeIndex.Add('}', TokenType.CloseBrace);
      CharacterTokenTypeIndex.Add(',', TokenType.Comma);
      CharacterTokenTypeIndex.Add('.', TokenType.Period);
      CharacterTokenTypeIndex.Add(';', TokenType.Semicolon);
      CharacterTokenTypeIndex.Add(':', TokenType.Colon);
      CharacterTokenTypeIndex.Add('=', TokenType.EqualSign);
      CharacterTokenTypeIndex.Add('!', TokenType.ExclamationPoint);
      CharacterTokenTypeIndex.Add('?', TokenType.QuestionMark);
      CharacterTokenTypeIndex.Add('&', TokenType.Ampersand);
      CharacterTokenTypeIndex.Add('|', TokenType.Pipe);
      CharacterTokenTypeIndex.Add('+', TokenType.Plus);
      CharacterTokenTypeIndex.Add('-', TokenType.Minus);
      CharacterTokenTypeIndex.Add('%', TokenType.Percent);
      CharacterTokenTypeIndex.Add('*', TokenType.Asterisk);
      CharacterTokenTypeIndex.Add('\\', TokenType.BackSlash);
      CharacterTokenTypeIndex.Add('/', TokenType.ForwardSlash);
      CharacterTokenTypeIndex.Add('@', TokenType.At);
      CharacterTokenTypeIndex.Add('^', TokenType.Caret);
      CharacterTokenTypeIndex.Add('~', TokenType.Tilde);
      CharacterTokenTypeIndex.Add('$', TokenType.DollarSign);
      CharacterTokenTypeIndex.Add('#', TokenType.Hash);
      CharacterTokenTypeIndex.Add('\'', TokenType.SingleQuote);
      CharacterTokenTypeIndex.Add('"', TokenType.DoubleQuote);
      CharacterTokenTypeIndex.Add('`', TokenType.BackQuote);

      WhitespaceCharacterSet.AddRange('\x0000', ' ');
      IdentifierCharacterSet.AddRange('a', 'z');
      IdentifierCharacterSet.AddRange('A', 'Z');
      IdentifierCharacterSet.AddRange('0', '9');
      IdentifierCharacterSet.Add('_');
      NumberCharacterSet.AddRange('0', '9');
      VerticalWhitespaceCharacterSet.Add('\n');
      VerticalWhitespaceCharacterSet.Add('\r');

      HexadecimalCharacterSet.AddRange('0', '9');
      HexadecimalCharacterSet.AddRange('A', 'F');
      HexadecimalCharacterSet.AddRange('a', 'f');

      Base64CharacterSet.AddRange('0', '9');
      Base64CharacterSet.AddRange('A', 'Z');
      Base64CharacterSet.AddRange('a', 'z');
      Base64CharacterSet.Add('+');
      Base64CharacterSet.Add('/');
      Base64CharacterSet.Add('=');
    }
  }
}