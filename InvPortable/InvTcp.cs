﻿/*! 6 !*/
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
#if WINDOWS_APP
using Windows.Networking;
using Windows.Networking.Sockets;
#else
using System.Net;
using System.Net.Sockets;
#endif
using Inv.Support;

namespace Inv.Tcp
{
#if WINDOWS_APP
  public sealed class Client
  {
    public Client(string Host, int Port)
    {
      this.Host = Host;
      this.Port = Port;
    }

    public bool IsActive { get; private set; }
    public System.IO.Stream InputStream
    {
      get { return TcpClient.InputStream.AsStreamForRead(); }
    }
    public System.IO.Stream OutputStream
    {
      get { return TcpClient.OutputStream.AsStreamForWrite(); }
    }

    public void Connect()
    {
      if (!IsActive)
      {
        this.IsActive = true;

        try
        {
          this.TcpClient = new StreamSocket();
          TcpClient.ConnectAsync(new HostName(Host), Port.ToString(), SocketProtectionLevel.Tls10).AsTask().Wait();
        }
        catch
        {
          TcpClient.Dispose();
          this.TcpClient = null;

          throw;
        }
      }
    }
    public void Disconnect()
    {
      if (IsActive)
      {
        this.IsActive = false;

        if (TcpClient != null)
        {
          TcpClient.Dispose();
          this.TcpClient = null;
        }
      }
    }

    private string Host;
    private int Port;
    private StreamSocket TcpClient;
  }
#else
  public sealed class Server
  {
    public Server(string Host, int Port)
    {
      this.TcpServer = new System.Net.Sockets.TcpListener(System.Net.IPAddress.Any, Port);
      this.ChannelList = new Inv.DistinctList<Channel>();
      this.Certificate = new System.Security.Cryptography.X509Certificates.X509Certificate(@"C:\Development\Forge\Phoenix\PhoenixInteropAPI.pfx", "lartsek");
    }

    public bool IsActive { get; private set; }
    public System.Security.Cryptography.X509Certificates.X509Certificate Certificate { get; private set; }
    public event Action<Channel> ConnectEvent;
    public event Action<Channel> DisconnectEvent;

    public void Start()
    {
      if (!IsActive)
      {
        this.IsActive = true;

        TcpServer.Start();

        this.AcceptTask = System.Threading.Tasks.Task.Run(() =>
        {
          while (IsActive)
          {
            try
            {
              var TcpClient = BlockingAcceptClient(TcpServer);

              if (TcpClient != null)
              {
                TcpClient.NoDelay = true;

                var Channel = new Channel(this, TcpClient);

                lock (ChannelList)
                  ChannelList.Add(Channel);

                if (ConnectEvent != null)
                  ConnectEvent(Channel);
              }
            }
            catch
            {
              // TODO: log.
              //this.IsActive = false;
            }
          }
        });
      }
    }
    public void Stop()
    {
      if (IsActive)
      {
        this.IsActive = false;

        TcpServer.Stop();

        if (AcceptTask != null)
        {
          AcceptTask.Wait();
          this.AcceptTask = null;
        }

        foreach (var Channel in ChannelList)
        {
          try
          {
            Channel.Dispose();
          }
          catch
          {
          }
        }

        ChannelList.Clear();
      }
    }

    internal void CloseChannel(Channel Channel)
    {
      try
      {
        if (DisconnectEvent != null)
          DisconnectEvent(Channel);
      }
      finally
      {
        Channel.Dispose();

        lock (ChannelList)
          ChannelList.Remove(Channel);
      }
    }

    [DebuggerNonUserCode]
    private TcpClient BlockingAcceptClient(TcpListener TcpServer)
    {
      try
      {
        return TcpServer.AcceptTcpClient();
      }
      catch (System.Net.Sockets.SocketException Exception)
      {
        // A blocking operation was interrupted by a call to WSACancelBlockingCall"
        if (Exception.SocketErrorCode == SocketError.Interrupted && Exception.ErrorCode == 10004)
          return null;
        else
          throw Exception.Preserve();
      }
    }

    private System.Net.Sockets.TcpListener TcpServer;
    private System.Threading.Tasks.Task AcceptTask;
    private Inv.DistinctList<Channel> ChannelList;
  }

  public sealed class Channel : IDisposable
  {
    internal Channel(Server Server, System.Net.Sockets.TcpClient TcpClient)
    {
      this.Server = Server;
      this.TcpClient = TcpClient;

      this.SecureStream = new System.Net.Security.SslStream(TcpClient.GetStream(), false);
      SecureStream.AuthenticateAsServer(Server.Certificate, false, System.Security.Authentication.SslProtocols.Tls, true);
      //SecureStream.ReadTimeout = 5000;
      //SecureStream.WriteTimeout = 5000;
    }
    public void Dispose()
    {
      this.SecureStream.Dispose();
      this.TcpClient.Close();
    }

    public System.IO.Stream InputStream
    {
      get { return SecureStream; }
    }
    public System.IO.Stream OutputStream
    {
      get { return SecureStream; }
    }
    public bool CanReceive
    {
      get { return SecureStream.CanRead; }
    }
    public bool CanSend
    {
      get { return SecureStream.CanWrite; }
    }

    public void Receive(Action<System.IO.Stream> Action)
    {
      try
      {
        Action(SecureStream);
      }
      catch
      {
        Server.CloseChannel(this);

        throw;
      }
    }
    public void Send(Action<System.IO.Stream> Action)
    {
      try
      {
        Action(SecureStream);
      }
      catch
      {
        Server.CloseChannel(this);

        throw;
      }
    }

    private Server Server;
    private System.Net.Sockets.TcpClient TcpClient;
    private System.Net.Security.SslStream SecureStream;
  }

  public sealed class Client
  {
    public Client(string Host, int Port)
    {
      this.Host = Host;
      this.Port = Port;
      this.TcpClient = new System.Net.Sockets.TcpClient();
      TcpClient.NoDelay = true;
    }

    public bool IsActive { get; private set; }
    public System.IO.Stream Stream
    {
      get { return SecureStream; }
    }

    public void Connect()
    {
      if (!IsActive)
      {
        this.IsActive = true;

        try
        {
          TcpClient.Connect(Host, Port);

          this.SecureStream = new System.Net.Security.SslStream(TcpClient.GetStream(), true, (sender, cert, chain, sslPolicy) => { return true; });
          SecureStream.AuthenticateAsClient(Host, null, System.Security.Authentication.SslProtocols.Tls, false);
          //SecureStream.ReadTimeout = 5000;
          //SecureStream.WriteTimeout = 5000;
        }
        catch
        {
          TcpClient.Close();

          throw;
        }
      }
    }
    public void Disconnect()
    {
      if (IsActive)
      {
        this.IsActive = false;

        if (SecureStream != null)
        {
          try
          {
            SecureStream.Dispose();
          }
          catch
          {
            // TODO: is this allowed?
          }

          this.SecureStream = null;
        }

        try
        {
          TcpClient.Close();
        }
        catch
        {
          // TODO: is this allowed?
        }
      }
    }

    private string Host;
    private int Port;
    private System.Net.Sockets.TcpClient TcpClient;
    private System.Net.Security.SslStream SecureStream;
  }
#endif
}
