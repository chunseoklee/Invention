﻿/*! 1 !*/
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inv
{
  public sealed class TextExtractor : IDisposable
  {
    public TextExtractor(TextReader TextReader)
    {
      this.TextReader = TextReader;
      this.StringBuilder = new StringBuilder();
      this.Cache = "";
      this.Finished = false;

      // Initialise extractor to determine if it is already finished.
      NextCharacter();
    }
    public void Dispose()
    {
      TextReader.Dispose();
    }

    public TextReader TextReader { get; private set; }

    public bool More()
    {
      return !Finished;
    }
    public char NextCharacter()
    {
      char Result;

      if (Cache.Length == 0)
      {
        var NextRead = TextReader.Read();

        if (NextRead == -1)
        {
          Finished = true;
          Result = '\0';
        }
        else
        {
          Result = (char)NextRead;
        }

        Cache += Result;
      }
      else
      {
        Result = Cache[0];
      }

      return Result;
    }
    public bool NextIsString(string Value)
    {
      var ValueLength = Value.Length;
      var CacheLength = Cache.Length;
      var ReadLength = ValueLength - CacheLength;

      if (ReadLength > 0)
      {
        var StreamCache = new char[ReadLength];

        TextReader.Read(StreamCache, 0, (int)ReadLength);

        Cache += new string(StreamCache);
      }

      return Cache.StartsWith(Value);
    }
    public char ConsumeCharacter()
    {
      var Result = NextCharacter();

      Cache = Cache.Substring(1, Cache.Length - 1);

      return Result;
    }
    public string ConsumeString(string Value)
    {
      if (!NextIsString(Value))
        throw new Exception(string.Format(CultureInfo.InvariantCulture, "Expected token '{0}' but found token '{1}'.", Value, Cache.Substring(1, Value.Length)));

      Cache = Cache.Substring(Value.Length, Cache.Length - Value.Length);

      return Value;
    }
    public string ConsumeUntilString(string Value)
    {
      StringBuilder.Length = 0;

      while (More() && !NextIsString(Value))
        StringBuilder.Append(ConsumeCharacter());

      return StringBuilder.ToString();
    }
    public string ConsumeWhileCharacterSet(CharacterSet CharacterSet)
    {
      StringBuilder.Length = 0;

      while (More() && CharacterSet.ContainsValue(NextCharacter()))
        StringBuilder.Append(ConsumeCharacter());

      return StringBuilder.ToString();
    }
    public string ConsumeUntilCharacterSet(CharacterSet CharacterSet)
    {
      StringBuilder.Length = 0;

      while (More() && !CharacterSet.ContainsValue(NextCharacter()))
        StringBuilder.Append(ConsumeCharacter());

      return StringBuilder.ToString();
    }
    public string ConsumeUntilCharacter(char Character)
    {
      StringBuilder.Length = 0;

      while (More() && Character != NextCharacter())
        StringBuilder.Append(ConsumeCharacter());

      return StringBuilder.ToString();
    }
    public char ConsumeCharacter(char Value)
    {
      if (NextCharacter() != Value)
        throw new Exception(string.Format(CultureInfo.InvariantCulture, "Expected character {0} but found character {1}.", Value, NextCharacter()));

      return ConsumeCharacter();
    }

    private string Cache;
    private StringBuilder StringBuilder;
    private bool Finished;
  }
}
