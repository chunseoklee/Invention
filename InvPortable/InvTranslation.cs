﻿/*! 6 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Inv.Support;

namespace Inv.Translation
{
  public sealed class Map
  {
    public Map()
    {
      this.ConceptList = new Inv.DistinctList<Concept>();
    }

    public Concept AddConcept(string Name)
    {
      var Result = new Concept(this, Name);

      ConceptList.Add(Result);

      return Result;
    }
    public IEnumerable<Concept> GetConcepts()
    {
      return ConceptList;
    }

    public void Compile(object This)
    {
      var Type = This.GetType();

      foreach (var Field in Type.GetReflectionFields())
      {
        if (Field.FieldType == typeof(Inv.Translation.Concept))
          Field.SetValue(This, AddConcept(Field.Name));
      }
    }
    public void Clear()
    {
      foreach (var Concept in ConceptList)
        Concept.Dictionary = null;
    }
    public void Strip()
    {
      foreach (var Concept in ConceptList)
      {
        if (Concept.Dictionary != null)
        {
          foreach (var Entry in Concept.Dictionary.ToArray())
          {
            if (string.IsNullOrWhiteSpace(Entry.Value))
              Concept.Dictionary.Remove(Entry.Key);
          }
        }
      }
    }
    public void Import(System.IO.Stream Stream)
    {
      var ConceptDictionary = ConceptList.ToDictionary(C => C.Name);

      using (var CsvReader = new CsvReader(Stream))
      {
        var CurrentConcept = (Concept)null;

        while (CsvReader.More())
        {
          CsvReader.ReadRecordBegin();

          var ConceptName = CsvReader.ReadRecordField();
          if (CurrentConcept == null || CurrentConcept.Name != ConceptName)
          {
            CurrentConcept = ConceptDictionary.GetValueOrDefault(ConceptName);
            if (CurrentConcept != null && CurrentConcept.Dictionary == null)
              CurrentConcept.Dictionary = new Dictionary<string, string>();
          }

          var SourceText = CsvReader.ReadRecordField().Trim();
          var TargetText = CsvReader.ReadRecordField().Trim();

          if (CurrentConcept != null)
          {
            var CurrentText = CurrentConcept.Dictionary.GetValueOrDefault(SourceText);

            if (string.IsNullOrWhiteSpace(CurrentText))
              CurrentConcept.Dictionary[SourceText] = TargetText;
          }

          CsvReader.ReadRecordEnd();
        }
      }
    }
    public void Export(System.IO.Stream Stream)
    {
      using (var CsvWriter = new CsvWriter(Stream))
      {
        foreach (var Concept in ConceptList.OrderBy(C => C.Name))
        {
          if (Concept.Dictionary != null)
          {
            foreach (var Term in Concept.Dictionary.OrderBy(T => T.Key))
              CsvWriter.WriteRecord(Concept.Name, Term.Key, Term.Value);
          }
        }
      }
    }
    public void Merge(Map MergeMap)
    {
      var ConceptDictionary = ConceptList.ToDictionary(C => C.Name);

      foreach (var MergeConcept in MergeMap.GetConcepts())
      {
        var CurrentConcept = ConceptDictionary.GetValueOrDefault(MergeConcept.Name);

        if (CurrentConcept == null)
        {
          // lost concept.
          throw new Exception("Concept missing: " + MergeConcept.Name);
        }
        else if (MergeConcept.Dictionary != null)
        {
          if (CurrentConcept.Dictionary == null)
            CurrentConcept.Dictionary = new Dictionary<string, string>();

          var ObsoleteSet = CurrentConcept.Dictionary.Keys.ToHashSet();

          foreach (var MergeEntry in MergeConcept.Dictionary)
          {
            ObsoleteSet.Remove(MergeEntry.Key);
            
            var CurrentText = CurrentConcept.Dictionary.GetValueOrDefault(MergeEntry.Key);

            if (string.IsNullOrWhiteSpace(CurrentText))
              CurrentConcept.Dictionary[MergeEntry.Key] = MergeEntry.Value;
          }

          foreach (var Obsolete in ObsoleteSet)
            Debug.WriteLine("OBSOLETE " + CurrentConcept.Name + ": " + Obsolete);
        }
      }
    }

    private Inv.DistinctList<Concept> ConceptList;
  }

  public sealed class Concept
  {
    internal Concept(Map Map, string Name)
    {
      this.Map = Map;
      this.Name = Name;
      this.Dictionary = null;
    }

    public string Get(string SourceText)
    {
      if (Dictionary != null && !string.IsNullOrWhiteSpace(SourceText))
        return Dictionary.GetValueOrDefault(SourceText, SourceText).EmptyAsNull() ?? SourceText;
      else
        return SourceText;
    }
    public void Set(string SourceText, string TargetText)
    {
      if (Dictionary == null)
        Dictionary = new Dictionary<string, string>();

      Dictionary[SourceText] = TargetText.NullAsEmpty().Trim();
    }
    public void Declare<T>(IEnumerable<T> Source, Func<T, string> Function)
    {
      foreach (var Item in Source)
      {
        var SourceText = Function(Item);

        if (!string.IsNullOrWhiteSpace(SourceText))
        {
          if (Dictionary == null)
            Dictionary = new Dictionary<string, string>();

          Dictionary.GetOrAdd(SourceText.Trim(), S => "");
        }
      }
    }
    public void Define(Func<string, string> ProcessFunction)
    {
      if (Dictionary != null)
      {
        foreach (var Term in Dictionary.Keys.ToArray())
          Dictionary[Term] = ProcessFunction(Term).NullAsEmpty().Trim();
      }
    }

    internal string Name { get; private set; }
    internal Dictionary<string, string> Dictionary { get; set; }

    private Map Map;
  }
}
