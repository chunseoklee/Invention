﻿/*! 11 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inv.Support;
using System.Diagnostics;
using System.IO;

namespace Inv
{
  public sealed class Web
  {
    internal Web(Application Application)
    {
      this.Application = Application;
    }

    public void Launch(Uri Uri)
    {
      Application.Platform.WebLaunchUri(Uri);
    }
    public WebDownload GetDownload(Uri Uri)
    {
      var RemoteFileRequest = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(Uri);
      RemoteFileRequest.Method = "GET";

      var Response = RemoteFileRequest.GetResponse();

      return new WebDownload(Response.ContentLength, Response.GetResponseStream());
    }
    public WebSocket NewSocket(string Host, int Port)
    {
      return new WebSocket(this, Host, Port);
    }
    public WebBroker NewBroker(string BaseUri)
    {
      return new WebBroker(BaseUri);
    }

    internal Application Application { get; private set; }
  }

  public sealed class WebDownload : IDisposable
  {
    internal WebDownload(long Length, System.IO.Stream Stream)
    {
      this.Length = Length;
      this.Stream = Stream;
    }
    public void Dispose()
    {
      Stream.Dispose();
    }

    public long Length { get; private set; }
    public System.IO.Stream Stream { get; private set; }
  }

  public sealed class WebProtocol
  {
    internal WebProtocol(Inv.Persist.Governor Governor)
    {
      this.Governor = Governor;
    }

    public byte[] Encode<TData>(TData Data)
    {
      using (var MemoryStream = new System.IO.MemoryStream())
      {
        Governor.Save(Data, MemoryStream);

        MemoryStream.Flush();

        return MemoryStream.ToArray();
      }
    }
    public TData Decode<TData>(byte[] DataBuffer)
    {
      using (var MemoryStream = new System.IO.MemoryStream(DataBuffer))
        return (TData)Governor.Load(MemoryStream);
    }
    public void Decode<TData>(byte[] DataBuffer, TData Data)
    {
      using (var MemoryStream = new System.IO.MemoryStream(DataBuffer))
        Governor.Load(Data, MemoryStream);
    }

    private Inv.Persist.Governor Governor;
  }

  internal enum WebContent
  {
    TextPlain,
    TextCsv,
    TextJson
  }
  
  public sealed class WebSocket
  {
    internal WebSocket(Web Web, string Host, int Port)
    {
      this.Web = Web;
      this.Host = Host;
      this.Port = Port;
    }

    public void Connect()
    {
      if (!IsConnected)
      {
        Web.Application.Platform.WebSocketConnect(this);
        this.IsConnected = true;
      }
    }
    public void Disconnect()
    {
      if (IsConnected)
      {
        this.IsConnected = false;
        Web.Application.Platform.WebSocketDisconnect(this);
      }
    }

    public Web Web { get; private set; }
    public string Host { get; private set; }
    public int Port { get; private set; }
    public Stream InputStream { get; private set; }
    public Stream OutputStream { get; private set; }

    // NOTE: set by each platform implementation.
    internal object Node { get; set; }

    internal void SetStreams(System.IO.Stream InputStream, System.IO.Stream OutputStream)
    {
      this.InputStream = InputStream;
      this.OutputStream = OutputStream;
    }

    private bool IsConnected;
  }

  public sealed class WebBroker
  {
    public WebBroker(string BaseUri)
    {
      this.BaseUri = BaseUri;

      //System.Net.WebRequest.DefaultWebProxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
    }

    public WebDownload GetDownload(string QueryText)
    {
      var RemoteFileRequest = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(new Uri(BaseUri + QueryText));
      RemoteFileRequest.Method = "GET";

      var Response = RemoteFileRequest.GetResponse();

      return new WebDownload(Response.ContentLength, Response.GetResponseStream());
    }
    public string GetPlainText(string QueryText)
    {
      var WebRequest = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(new Uri(BaseUri + QueryText));
      WebRequest.ContentType = WebContentType(WebContent.TextPlain);
      WebRequest.Method = "GET";

      using (var WebResponse = WebRequest.GetResponse())
      using (var StreamReader = new System.IO.StreamReader(WebResponse.GetResponseStream()))
        return StreamReader.ReadToEnd().Trim();
    }
    public IEnumerable<string> GetPlainTextLines(string QueryText)
    {
      var WebRequest = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(new Uri(BaseUri + QueryText));
      WebRequest.ContentType = WebContentType(WebContent.TextPlain);
      WebRequest.Method = "GET";

      using (var Response = WebRequest.GetResponse())
      using (var ResponseStream = Response.GetResponseStream())
      using (var ResponseReader = new System.IO.StreamReader(ResponseStream))
      {
        while (!ResponseReader.EndOfStream)
          yield return ResponseReader.ReadLine();
      }
    }
    public IEnumerable<string[]> GetCsvTextLines(string QueryText)
    {
      var WebRequest = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(new Uri(BaseUri + QueryText));
      WebRequest.ContentType = WebContentType(WebContent.TextCsv);
      WebRequest.Method = "GET";

      using (var Response = WebRequest.GetResponse())
      using (var ResponseStream = Response.GetResponseStream())
      using (var ResponseReader = new System.IO.StreamReader(ResponseStream))
      using (var CsvReader = new Inv.CsvReader(ResponseReader))
      {
        while (!ResponseReader.EndOfStream)
          yield return CsvReader.ReadRecord().ToArray();
      }
    }
    public void PostCsvTextLine(string QueryText, string[] Text)
    {
      var WebRequest = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(new Uri(BaseUri + QueryText));
      WebRequest.ContentType = WebContentType(WebContent.TextCsv);
      WebRequest.Method = "POST";

      using (var RequestStream = WebRequest.GetRequestStream())
      using (var RequestWriter = new System.IO.StreamWriter(RequestStream))
      using (var CsvWriter = new Inv.CsvWriter(RequestWriter))
        CsvWriter.WriteRecord(Text);

      WebRequest.GetResponse().Dispose();
    }
    public void PostPlainTextLine(string QueryText, string Line)
    {
      var WebRequest = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(new Uri(BaseUri + QueryText));
      WebRequest.ContentType = WebContentType(WebContent.TextPlain);
      WebRequest.Method = "POST";

      using (var RequestStream = WebRequest.GetRequestStream())
      using (var RequestWriter = new System.IO.StreamWriter(RequestStream))
        RequestWriter.WriteLine(Line);

      WebRequest.GetResponse().Dispose();
    }
    public T GetTextJsonObject<T>(string QueryText)
    {
      var WebRequest = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(BaseUri + QueryText);
      WebRequest.ContentType = WebContentType(WebContent.TextJson);
      WebRequest.Method = "GET";

      return ReadObject<T>(WebRequest);
    }
    public void PostTextJsonObject(string QueryText, object Request)
    {
      PostTextJsonObject<object>(QueryText, Request);
    }
    public TResponse PostTextJsonObject<TResponse>(string QueryText, object Request)
    {
      var RequestText = Newtonsoft.Json.JsonConvert.SerializeObject(Request);
      //Debug.WriteLine(RequestText);

      var WebRequest = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(BaseUri + QueryText);
      WebRequest.Method = "POST";
      WebRequest.ContentType = WebContentType(WebContent.TextJson);
      // TODO: ContentLength?

      using (var RequestStream = WebRequest.GetRequestStream())
      using (var RequestWriter = new System.IO.StreamWriter(RequestStream))
        RequestWriter.Write(RequestText);

      return ReadObject<TResponse>(WebRequest);
    }

    private T ReadObject<T>(System.Net.HttpWebRequest WebRequest)
    {
      System.Net.HttpWebResponse WebResponse;
      bool WebFailed;
      try
      {
        WebResponse = (System.Net.HttpWebResponse)WebRequest.GetResponse();
        WebFailed = false;
      }
      catch (System.Net.WebException WebException)
      {
        WebResponse = (System.Net.HttpWebResponse)WebException.Response;
        WebFailed = true;
      }
      catch
      {
        throw;
      }

      if (WebResponse == null)
        return default(T);

      using (var StreamReader = new System.IO.StreamReader(WebResponse.GetResponseStream()))
      {
        var ResponseText = StreamReader.ReadToEnd();

        if (WebFailed)
          throw new Exception(ResponseText);

        var ResponseValue = Deserialize<T>(ResponseText);

        return ResponseValue;
      }
    }
    private T Deserialize<T>(string Text)
    {
      //Debug.WriteLine(Text);

      try
      {
        var Result = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(Text);

        //Debug.WriteLine(Result);

        return Result;
      }
      catch (Newtonsoft.Json.JsonReaderException)
      {
#if DEBUG
        throw;
#else

        return default(T);
#endif
      }
    }
    private string WebContentType(WebContent Content)
    {
      switch (Content)
      {
        case WebContent.TextCsv:
          return "text/csv";

        case WebContent.TextPlain:
          return "text/plain";

        case WebContent.TextJson:
          return "text/json";

        default:
          throw new Exception("HttpContent not handled: " + Content);
      }
    }

    private string BaseUri;
  }
}
