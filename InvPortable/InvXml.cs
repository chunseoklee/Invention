﻿/*! 1 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Diagnostics;
using System.Globalization;
using Inv.Support;

namespace Inv
{
  [DataContract]
  public struct Xml : IComparable
  {
    public static Xml Empty()
    {
      return new Xml(""); 
    }
    public static Xml Hollow(long Length)
    {
      return new Xml(null, Length);
    }

    public Xml(string Text)
    {
      Debug.Assert(Text != null, "Buffer must not be null.");

      this.Text = Text;
      this.Length = Text.Length;
    }
    internal Xml(string Text, long Length)
    {
      this.Text = Text;
      this.Length = Length;
    }
    internal Xml(Xml Xml)
    {
      this.Text = Xml.Text;
      this.Length = Xml.Length;
    }

    public string GetText()
    {
      Debug.Assert(this.Text != null, "Cannot use GetText() on a hollow Xml.");

      return this.Text;
    }
    public long GetLength()
    {
      return Length;
    }
    public DataSize GetSize()
    {
      return DataSize.FromBytes(Length);
    }
    public bool IsEmpty()
    {
      return Length == 0;
    }
    public bool IsHollow()
    {
      return Text == null;
    }
    public bool EqualTo(Xml Xml)
    {
      if (this.IsHollow() || Xml.IsHollow())
        return this.Length == Xml.Length;
      else
        return this.Text.Equals(Xml.Text, StringComparison.Ordinal);
    }
    public int CompareTo(Xml Xml)
    {
      if (this.IsHollow() || Xml.IsHollow())
        return this.Length.CompareTo(Xml.Length);
      else
        return this.Text.CompareTo(Xml.Text);
    }

    public override string ToString()
    {
      return "Xml[" + Length.ToString(CultureInfo.InvariantCulture) + "]";
    }
    public override bool Equals(object obj)
    {
      var Source = obj as Xml?;

      return Source != null && EqualTo(Source.Value);
    }
    public override int GetHashCode()
    {
      return this.Text.GetHashCode();
    }

    int IComparable.CompareTo(object obj)
    {
      return CompareTo((Xml)obj);
    }

    [DataMember]
    private readonly string Text;
    [DataMember]
    private readonly long Length;
  }
}
