﻿/*! 8 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inv.Support;

namespace InvTest
{
  static class WebProgram
  {
    static void Main(string[] args)
    {
      Inv.WebShell.Run(new InvTest.Application());
    }
  }
}
