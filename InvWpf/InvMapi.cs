﻿/*! 3 !*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace Inv
{
  public sealed class Mapi
  {
    public Mapi()
    {
      this.lastMsgID = new StringBuilder(600);
      this.session = IntPtr.Zero;
      this.origin = new Mapi32.MapiRecipDesc();
      this.recpts = new ArrayList();
      this.attachs = new Dictionary<string, string>();
    }

    public bool Send(string Subject, string Body)
    {
      lastMsg = new Mapi32.MapiMessage();
      lastMsg.subject = Subject;
      lastMsg.noteText = Body;

      // set pointers
      lastMsg.originator = AllocOrigin();
      lastMsg.recips = AllocRecips(out lastMsg.recipCount);
      lastMsg.files = AllocAttachs(out lastMsg.fileCount);

      error = Mapi32.MAPISendMail(session, IntPtr.Zero, lastMsg, Mapi32.MapiDialogModeless | Mapi32.MapiLogonUI | Mapi32.MapiNewSession, 0);
      Dealloc();

      origin = new Mapi32.MapiRecipDesc();
      recpts.Clear();
      attachs.Clear();
      lastMsg = null;

      return error == 0;
    }
    public void AddRecipient(string Name, string Address, bool CC)
    {
      var dest = new Mapi32.MapiRecipDesc();
      if (CC)
        dest.recipClass = Mapi32.MapiCC;
      else
        dest.recipClass = Mapi32.MapiTO;
      dest.name = Name;
      dest.address = Address;
      recpts.Add(dest);
    }
    public void SetSender(string Name, string Address)
    {
      origin.name = Name;
      origin.address = Address;
    }
    public void Attach(string filepath, string displayName = null)
    {
      attachs.Add(filepath, displayName ?? System.IO.Path.GetFileName(filepath));
    }
    public bool Delete(string id)
    {
      error = Mapi32.MAPIDeleteMail(session, IntPtr.Zero, id, 0, 0);
      return error == 0;
    }
    public bool SaveAttachment(string id, string name, string savepath)
    {
      var ptrmsg = IntPtr.Zero;
      error = Mapi32.MAPIReadMail(session, IntPtr.Zero, id, Mapi32.MapiPeek, 0, ref ptrmsg);
      if ((error != 0) || (ptrmsg == IntPtr.Zero))
        return false;

      lastMsg = new Mapi32.MapiMessage();
      Marshal.PtrToStructure(ptrmsg, lastMsg);
      var f = false;
      if ((lastMsg.fileCount > 0) && (lastMsg.fileCount < 100) && (lastMsg.files != IntPtr.Zero))
        f = SaveAttachmentByName(name, savepath);
      Mapi32.MAPIFreeBuffer(ptrmsg);
      return f;
    }
    public bool SingleAddress(string label, out string name, out string addr)
    {
      name = null;
      addr = null;
      var newrec = 0;
      var ptrnew = IntPtr.Zero;
      error = Mapi32.MAPIAddress(session, IntPtr.Zero, null, 1, label, 0, IntPtr.Zero, 0, 0, ref newrec, ref ptrnew);

      if ((error != 0) || (newrec < 1) || (ptrnew == IntPtr.Zero))
        return false;

      var recip = new Mapi32.MapiRecipDesc();
      Marshal.PtrToStructure(ptrnew, recip);
      name = recip.name;
      addr = recip.address;

      Mapi32.MAPIFreeBuffer(ptrnew);
      return true;
    }
    public string Error()
    {
      if (error <= 26)
        return errors[error];
      return "?unknown? [" + error.ToString() + "]";
    }

    private bool SaveAttachmentByName(string name, string savepath)
    {
      var f = true;
      var fdtype = typeof(Mapi32.MapiFileDesc);
      var fdsize = Marshal.SizeOf(fdtype);
      var fdtmp = new Mapi32.MapiFileDesc();
      var runptr = (int)lastMsg.files;
      for (var i = 0; i < lastMsg.fileCount; i++)
      {
        Marshal.PtrToStructure((IntPtr)runptr, fdtmp);
        runptr += fdsize;
        if (fdtmp.flags != 0)
          continue;
        if (fdtmp.name == null)
          continue;

        try
        {
          if (name == fdtmp.name)
          {
            if (System.IO.File.Exists(savepath))
              System.IO.File.Delete(savepath);
            System.IO.File.Move(fdtmp.path, savepath);
          }
        }
        catch (Exception)
        {
          f = false;
          error = 13;
        }

        try
        {
          System.IO.File.Delete(fdtmp.path);
        }
        catch (Exception)
        {
        }
      }
      return f;
    }
    private IntPtr AllocOrigin()
    {
      origin.recipClass = Mapi32.MapiORIG;
      var rtype = typeof(Mapi32.MapiRecipDesc);
      var rsize = Marshal.SizeOf(rtype);
      var ptro = Marshal.AllocHGlobal(rsize);
      Marshal.StructureToPtr(origin, ptro, false);
      return ptro;
    }
    private IntPtr AllocRecips(out int recipCount)
    {
      recipCount = 0;
      if (recpts.Count == 0)
        return IntPtr.Zero;

      var rtype = typeof(Mapi32.MapiRecipDesc);
      var rsize = Marshal.SizeOf(rtype);
      var ptrr = Marshal.AllocHGlobal(recpts.Count * rsize);

      var runptr = (int)ptrr;
      foreach (var recpt in recpts)
      {
        Marshal.StructureToPtr(recpt as Mapi32.MapiRecipDesc, (IntPtr)runptr, false);
        runptr += rsize;
      }

      recipCount = recpts.Count;
      return ptrr;
    }
    private IntPtr AllocAttachs(out int fileCount)
    {
      fileCount = 0;
      if (attachs == null)
        return IntPtr.Zero;
      if ((attachs.Count <= 0) || (attachs.Count > 100))
        return IntPtr.Zero;

      var atype = typeof(Mapi32.MapiFileDesc);
      var asize = Marshal.SizeOf(atype);
      var ptra = Marshal.AllocHGlobal(attachs.Count * asize);

      var mfd = new Mapi32.MapiFileDesc();
      mfd.position = -1;
      var runptr = (int)ptra;
      foreach (var attach in attachs)
      {
        mfd.path = attach.Key;
        mfd.name = attach.Value;
        Marshal.StructureToPtr(mfd, (IntPtr)runptr, false);
        runptr += asize;
      }

      fileCount = attachs.Count;
      return ptra;
    }
    private void Dealloc()
    {
      var rtype = typeof(Mapi32.MapiRecipDesc);
      var rsize = Marshal.SizeOf(rtype);

      if (lastMsg.originator != IntPtr.Zero)
      {
        Marshal.DestroyStructure(lastMsg.originator, rtype);
        Marshal.FreeHGlobal(lastMsg.originator);
      }

      if (lastMsg.recips != IntPtr.Zero)
      {
        var runptr = (int)lastMsg.recips;
        for (var i = 0; i < lastMsg.recipCount; i++)
        {
          Marshal.DestroyStructure((IntPtr)runptr, rtype);
          runptr += rsize;
        }
        Marshal.FreeHGlobal(lastMsg.recips);
      }

      if (lastMsg.files != IntPtr.Zero)
      {
        var ftype = typeof(Mapi32.MapiFileDesc);
        var fsize = Marshal.SizeOf(ftype);

        var runptr = (int)lastMsg.files;
        for (var i = 0; i < lastMsg.fileCount; i++)
        {
          Marshal.DestroyStructure((IntPtr)runptr, ftype);
          runptr += fsize;
        }
        Marshal.FreeHGlobal(lastMsg.files);
      }
    }

    private StringBuilder lastMsgID;
    private Mapi32.MapiMessage lastMsg;
    private IntPtr session;
    private Mapi32.MapiRecipDesc origin;
    private ArrayList recpts;
    private Dictionary<string, string> attachs;
    private int error;

    private static readonly string[] errors = new string[] 
    {
      "OK [0]", "User abort [1]", "General MAPI failure [2]", "MAPI login failure [3]",
      "Disk full [4]", "Insufficient memory [5]", "Access denied [6]", "-unknown- [7]",
      "Too many sessions [8]", "Too many files were specified [9]", "Too many recipients were specified [10]", "A specified attachment was not found [11]",
      "Attachment open failure [12]", "Attachment write failure [13]", "Unknown recipient [14]", "Bad recipient type [15]",
      "No messages [16]", "Invalid message [17]", "Text too large [18]", "Invalid session [19]",
      "Type not supported [20]", "A recipient was specified ambiguously [21]", "Message in use [22]", "Network failure [23]",
      "Invalid edit fields [24]", "Invalid recipients [25]", "Not supported [26]" 
    };
  }

  internal static class Mapi32
  {
    [DllImport("MAPI32.DLL", CharSet = CharSet.Ansi)]
    public static extern int MAPILogon(IntPtr hwnd, string prf, string pw, int flg, int rsv, ref IntPtr sess);
    [DllImport("MAPI32.DLL")]
    public static extern int MAPILogoff(IntPtr sess, IntPtr hwnd, int flg, int rsv);

    public const int MapiORIG = 0;
    public const int MapiTO = 1;
    public const int MapiCC = 2;
    public const int MapiBCC = 3;

    [DllImport("MAPI32.DLL")]
    public static extern int MAPISendMail(IntPtr sess, IntPtr hwnd, MapiMessage message, int flg, int rsv);

    public const int MapiLogonUI = 0x00000001;
    public const int MapiPasswordUI = 0x00020000;
    public const int MapiNewSession = 0x00000002;
    public const int MapiDialog = 0x00000008;
    public const int MapiDialogModeless = 0x00000004 | MapiDialog;
    public const int MapiForceDownload = 0x00001000;
    public const int MapiExtendedUI = 0x00000020;
    public const int MapiForceUnicode = 0x00040000;

    [DllImport("MAPI32.DLL", CharSet = CharSet.Ansi)]
    public static extern int MAPIFindNext(IntPtr sess, IntPtr hwnd, string typ, string seed, int flg, int rsv, StringBuilder id);

    public const int MapiUnreadOnly = 0x00000020;
    public const int MapiGuaranteeFiFo = 0x00000100;
    public const int MapiLongMsgID = 0x00004000;

    [DllImport("MAPI32.DLL", CharSet = CharSet.Ansi)]
    public static extern int MAPIReadMail(IntPtr sess, IntPtr hwnd, string id, int flg, int rsv, ref IntPtr ptrmsg);

    [DllImport("MAPI32.DLL")]
    public static extern int MAPIFreeBuffer(IntPtr ptr);

    [DllImport("MAPI32.DLL", CharSet = CharSet.Ansi)]
    public static extern int MAPIDeleteMail(IntPtr sess, IntPtr hwnd, string id, int flg, int rsv);

    public const int MapiPeek = 0x00000080;
    public const int MapiSuprAttach = 0x00000800;
    public const int MapiEnvOnly = 0x00000040;
    public const int MapiBodyAsFile = 0x00000200;

    public const int MapiUnread = 0x00000001;
    public const int MapiReceiptReq = 0x00000002;
    public const int MapiSent = 0x00000004;

    [DllImport("MAPI32.DLL", CharSet = CharSet.Ansi)]
    public static extern int MAPIAddress(IntPtr sess, IntPtr hwnd, string caption, int editfld, string labels, int recipcount, IntPtr ptrrecips, int flg, int rsv, ref int newrec, ref IntPtr ptrnew);

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public class MapiMessage
    {
      public int reserved;
      public string subject;
      public string noteText;
      public string messageType;
      public string dateReceived;
      public string conversationID;
      public int flags;
      public IntPtr originator;		// MapiRecipDesc* [1]
      public int recipCount;
      public IntPtr recips;			// MapiRecipDesc* [n]
      public int fileCount;
      public IntPtr files;			// MapiFileDesc*  [n]
    }

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public class MapiRecipDesc
    {
      public int reserved;
      public int recipClass;
      public string name;
      public string address;
      public int eIDSize;
      public IntPtr entryID;			// void*
    }

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public class MapiFileDesc
    {
      public int reserved;
      public int flags;
      public int position;
      public string path;
      public string name;
      public IntPtr type;
    }
  }
}