﻿/*! 26 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Foundation;
using UIKit;
using CoreGraphics;
using Inv.Support;

namespace Inv
{
  public interface iOSLayoutElement
  {
    CGSize CalculateSize(CGSize MaxSize);
    UIView View { get; }
  }

  public sealed class iOSLayoutButton : UIButton, iOSLayoutElement
  {
    public iOSLayoutButton()
    {
      this.TranslatesAutoresizingMaskIntoConstraints = false;
      this.LayoutMargins = UIEdgeInsets.Zero;
    }

    public event Action PreviewTouchesBeganEvent;
    public event Action PreviewTouchesEndedEvent;
    public event Action PreviewTouchesCancelledEvent;

    public void SetContentElement(iOSLayoutElement Element)
    {
      if (Content != Element)
      {
        if (Content != null)
          Content.View.RemoveFromSuperview();

        this.Content = Element;

        if (Content != null)
          this.AddSubview(Content.View);

        this.Arrange();
      }
    }

    public override void TouchesBegan(NSSet touches, UIEvent evt)
    {
      base.TouchesBegan(touches, evt);

      if (PreviewTouchesBeganEvent != null)
        PreviewTouchesBeganEvent();
    }
    public override void TouchesCancelled(NSSet touches, UIEvent evt)
    {
      base.TouchesCancelled(touches, evt);

      if (PreviewTouchesCancelledEvent != null)
        PreviewTouchesCancelledEvent();
    }
    public override void TouchesEnded(NSSet touches, UIEvent evt)
    {
      base.TouchesEnded(touches, evt);

      if (PreviewTouchesEndedEvent != null)
        PreviewTouchesEndedEvent();
    }
    public override void LayoutSubviews()
    {
      base.LayoutSubviews();

      if (Content != null)
        Content.View.Frame = LayoutMargins.InsetRect(Bounds);
    }

    UIView iOSLayoutElement.View
    {
      get { return this; }
    }
    CGSize iOSLayoutElement.CalculateSize(CGSize MaxSize)
    {
      var Result = Content != null ? Content.CalculateSize(this.FitToSize(MaxSize)) : CGSize.Empty;

      Result.Width += LayoutMargins.Left + LayoutMargins.Right;
      Result.Height += LayoutMargins.Top + LayoutMargins.Bottom;

      return Result;
    }

    private iOSLayoutElement Content;
  }

  public class iOSLayoutLabel : UILabel, iOSLayoutElement
  {
    public iOSLayoutLabel()
    {
      this.TranslatesAutoresizingMaskIntoConstraints = false;
      this.LayoutMargins = UIEdgeInsets.Zero;
      this.ClipsToBounds = true; // NOTE: otherwise corner radius doesn't work.
    }

    public override string Text
    {
      get
      {
        return base.Text;
      }
      set
      {
        base.Text = value;
        this.Arrange();
      }
    }

    public override void DrawText(CoreGraphics.CGRect rect)
    {
      base.DrawText(LayoutMargins.InsetRect(rect));
      //base.DrawText(rect);
    }
    public override CoreGraphics.CGRect TextRectForBounds(CoreGraphics.CGRect bounds, nint numberOfLines)
    {
      return base.TextRectForBounds(bounds, numberOfLines);
    }
    public override void InvalidateIntrinsicContentSize()
    {
      base.InvalidateIntrinsicContentSize();

      this.Arrange();
    }

    UIView iOSLayoutElement.View
    {
      get { return this; }
    }
    CGSize iOSLayoutElement.CalculateSize(CGSize MaxSize)
    {
      var Result = SizeThatFits(this.FitToSize(MaxSize));

      Result.Width += LayoutMargins.Left + LayoutMargins.Right;
      Result.Height += LayoutMargins.Top + LayoutMargins.Bottom;

      return Result;
    }
  }

  public sealed class iOSLayoutGraphic : iOSLayoutView, iOSLayoutElement
  {
    public iOSLayoutGraphic()
    {
      this.ImageView = new UIImageView();
      AddSubview(ImageView);
    }

    public UIImage Image
    {
      get { return ImageView.Image; }
      set { ImageView.Image = value; }
    }

    public override void LayoutSubviews()
    {
      base.LayoutSubviews();

      ImageView.Frame = LayoutMargins.InsetRect(Bounds);
    }

    CGSize iOSLayoutElement.CalculateSize(CGSize MaxSize)
    {
      var Result = ImageView.SizeThatFits(this.FitToSize(MaxSize));

      Result.Width += LayoutMargins.Left + LayoutMargins.Right;
      Result.Height += LayoutMargins.Top + LayoutMargins.Bottom;

      return Result;
    }

    UIView iOSLayoutElement.View
    {
      get { return this; }
    }

    private UIImageView ImageView;
  }

  public class iOSLayoutEdit : UITextField, iOSLayoutElement
  {
    public iOSLayoutEdit()
    {
      this.TranslatesAutoresizingMaskIntoConstraints = false;
      this.LayoutMargins = UIEdgeInsets.Zero; 
      this.ShouldBeginEditing = (t) => !IsReadOnly;
      this.ShouldReturn = (t) =>
      {
        t.EndEditing(true);

        return true;
      };
    }

    public bool IsReadOnly { get; set; }

    UIView iOSLayoutElement.View
    {
      get { return this; }
    }
    CGSize iOSLayoutElement.CalculateSize(CGSize MaxSize)
    {
      var Result = SizeThatFits(this.FitToSize(MaxSize));

      Result.Width += LayoutMargins.Left + LayoutMargins.Right;
      Result.Height += LayoutMargins.Top + LayoutMargins.Bottom;

      return Result;
    }
 }

  public class iOSLayoutMemo : UITextView, iOSLayoutElement
  {
    public iOSLayoutMemo()
    {
      this.TranslatesAutoresizingMaskIntoConstraints = false;
      this.LayoutMargins = UIEdgeInsets.Zero; 
      this.TextContainer.LineFragmentPadding = 0;
      this.TextContainerInset = UIEdgeInsets.Zero;
      this.Changed += (Sender, Event) => this.Arrange();
      this.ShouldBeginEditing = (t) => !IsReadOnly;
    }

    public override string Text
    {
      get
      {
        return base.Text;
      }
      set
      {
        base.Text = value;
        this.Arrange();
      }
    }
    public bool IsReadOnly { get; set; }
    public override UIEdgeInsets LayoutMargins
    {
      get { return base.TextContainerInset; }
      set { base.TextContainerInset = value; }
    }

    UIView iOSLayoutElement.View
    {
      get { return this; }
    }
    CGSize iOSLayoutElement.CalculateSize(CGSize MaxSize)
    {
      var Result = SizeThatFits(this.FitToSize(MaxSize));

      Result.Width += LayoutMargins.Left + LayoutMargins.Right;
      Result.Height += LayoutMargins.Top + LayoutMargins.Bottom;

      return Result;
    }
  }

  internal sealed class iOSLayoutRender : UIView, iOSLayoutElement
  {
    public iOSLayoutRender()
    {
      this.TranslatesAutoresizingMaskIntoConstraints = false;
      this.LayoutMargins = UIEdgeInsets.Zero;

      //this.Layer.ShouldRasterize = true; // NOTE: does not seem to help achieve 60fps.
      this.Layer.DrawsAsynchronously = true; // NOTE: very much seems to help achieve 60fps!

      AddGestureRecognizer(new UIKit.UITapGestureRecognizer(R =>
      {
        if (SingleTapEvent != null && R.State == UIKit.UIGestureRecognizerState.Ended)
        {
          var Point = R.LocationInView(this);

          SingleTapEvent(Point);

          TouchUpInvoke(Point);
        }
      }));
      AddGestureRecognizer(new UIKit.UITapGestureRecognizer(R =>
      {
        if (DoubleTapEvent != null && R.State == UIKit.UIGestureRecognizerState.Ended)
        {
          var Point = R.LocationInView(this);

          DoubleTapEvent(Point);

          TouchUpInvoke(Point);
        }
      }) { NumberOfTapsRequired = 2 });
      AddGestureRecognizer(new UIKit.UILongPressGestureRecognizer(R =>
      {
        if (LongPressEvent != null && R.State == UIKit.UIGestureRecognizerState.Ended)
        {
          var Point = R.LocationInView(this);

          LongPressEvent(Point);

          TouchUpInvoke(Point);
        }
      }));
      AddGestureRecognizer(new UIKit.UIPinchGestureRecognizer(R =>
      {
        // NOTE: R.Scale is percentage increase/decrease eg 120% (1.20) or 20% (0.20)

        if (ZoomEvent != null/* && R.State == UIKit.UIGestureRecognizerState.Ended*/)
          ZoomEvent(R.Scale > 1.0F ? +1 : -1);
      }));
    }

    public event Action<CoreGraphics.CGContext> DrawEvent;
    public event Action<CoreGraphics.CGPoint> TouchDownEvent;
    public event Action<CoreGraphics.CGPoint> TouchUpEvent;
    public event Action<CoreGraphics.CGPoint> TouchMoveEvent;
    public event Action<CoreGraphics.CGPoint> SingleTapEvent;
    public event Action<CoreGraphics.CGPoint> DoubleTapEvent;
    public event Action<CoreGraphics.CGPoint> LongPressEvent;
    public event Action<int> ZoomEvent;

    public override void Draw(CoreGraphics.CGRect rect)
    {
      base.Draw(rect);

      DrawEvent(UIGraphics.GetCurrentContext());
    }

    public override void TouchesBegan(NSSet touches, UIEvent evt)
    {
      base.TouchesBegan(touches, evt);

      if (TouchDownEvent != null)
      {
        var Touch = touches.AnyObject as UITouch;

        if (Touch != null)
          TouchDownEvent(Touch.LocationInView(this));
      }
    }
    public override void TouchesMoved(NSSet touches, UIEvent evt)
    {
      base.TouchesMoved(touches, evt);

      if (TouchMoveEvent != null)
      {
        var Touch = touches.AnyObject as UITouch;

        if (Touch != null)
          TouchMoveEvent(Touch.LocationInView(this));
      }
    }
    public override void TouchesEnded(NSSet touches, UIEvent evt)
    {
      base.TouchesEnded(touches, evt);

      if (TouchUpEvent != null)
      {
        var Touch = touches.AnyObject as UITouch;

        if (Touch != null)
          TouchUpEvent(Touch.LocationInView(this));
      }
    }

    private void TouchUpInvoke(CGPoint Point)
    {
      if (TouchUpEvent != null)
        TouchUpEvent(Point);
    }

    UIView iOSLayoutElement.View
    {
      get { return this; }
    }
    CGSize iOSLayoutElement.CalculateSize(CGSize MaxSize)
    {
      var Result = SizeThatFits(this.FitToSize(MaxSize));

      Result.Width += LayoutMargins.Left + LayoutMargins.Right;
      Result.Height += LayoutMargins.Top + LayoutMargins.Bottom;

      return Result;
    }
  }

  public abstract class iOSLayoutView : UIView
  {
    /*public override void Dispose()
    {
      // Brute force, remove everything
      foreach (var view in Subviews)
        view.RemoveFromSuperview();
      base.Dispose();
    }*/

    public override UIView HitTest(CGPoint point, UIEvent uievent)
    {
      // ignore any user interaction on the layout view itself.
      var Result = base.HitTest(point, uievent);
    
      if (Result == this)
        return null;
      else
        return Result;
    }
  }

  public enum iOSLayoutHorizontal
  {
    Stretch,
    Center,
    Left,
    Right
  }

  public enum iOSLayoutVertical
  {
    Stretch,
    Center,
    Top,
    Bottom
  }

  public class iOSLayoutContainer : iOSLayoutView, iOSLayoutElement
  {
    public iOSLayoutContainer()
    {
      this.TranslatesAutoresizingMaskIntoConstraints = false;
      this.LayoutMargins = UIEdgeInsets.Zero; 
      this.Width = float.NaN;
      this.Height = float.NaN;
      this.MinimumWidth = float.NaN;
      this.MinimumHeight = float.NaN;
      this.MaximumWidth = float.NaN;
      this.MaximumHeight = float.NaN;
      this.Visible = true;
    }

    public void SetContentVisiblity(bool Visible)
    {
      if (Visible != this.Visible)
      {
        this.Visible = Visible;

        this.Arrange();
      }
    }
    public void SetContentHorizontal(iOSLayoutHorizontal Horizontal)
    {
      if (Horizontal != this.Horizontal)
      {
        this.Horizontal = Horizontal;

        this.Arrange();
      }
    }
    public void SetContentVertical(iOSLayoutVertical Vertical)
    {
      if (Vertical != this.Vertical)
      {
        this.Vertical = Vertical;

        this.Arrange();
      }
    }
    public void SetContentElement(iOSLayoutElement Content)
    {
      if (Content != this.Content)
      {
        if (this.Content != null)
          this.Content.View.RemoveFromSuperview();

        this.Content = Content;

        if (this.Content != null)
          AddSubview(this.Content.View);

        this.Arrange();
      }
    }
    public void SetContentWidth(float Width)
    {
      if (Width != this.Width)
      {
        this.Width = Width;

        this.Arrange();
      }
    }
    public void SetContentHeight(float Height)
    {
      if (Height != this.Height)
      {
        this.Height = Height;

        this.Arrange();
      }
    }
    public void SetContentMinimumWidth(float MinimumWidth)
    {
      if (MinimumWidth != this.MinimumWidth)
      {
        this.MinimumWidth = MinimumWidth;

        this.Arrange();
      }
    }
    public void SetContentMaximumWidth(float MaximumWidth)
    {
      if (MaximumWidth != this.MaximumWidth)
      {
        this.MaximumWidth = MaximumWidth;

        this.Arrange();
      }
    }
    public void SetContentMinimumHeight(float MinimumHeight)
    {
      if (MinimumHeight != this.MinimumHeight)
      {
        this.MinimumHeight = MinimumHeight;

        this.Arrange();
      }
    }
    public void SetContentMaximumHeight(float MaximumHeight)
    {
      if (MaximumHeight != this.MaximumHeight)
      {
        this.MaximumHeight = MaximumHeight;

        this.Arrange();
      }
    }

    public override void LayoutSubviews()
    {
      base.LayoutSubviews();

      if (Content != null)
      {
        var Element = Content.View;

        if (!Visible)
        {
          Element.Hidden = true;
          Element.Frame = CGRect.Empty;
        }
        else
        {
          var BoundsSize = Bounds.Size;
          BoundsSize.Width -= LayoutMargins.Left + LayoutMargins.Right;
          BoundsSize.Height -= LayoutMargins.Top + LayoutMargins.Bottom;

          var ContentSize = MeasureSize(BoundsSize);

          var ContentWidth = ContentSize.Width;
          nfloat ContentX;

          if (ContentWidth > BoundsSize.Width)
          {
            ContentWidth = BoundsSize.Width;
            ContentX = 0;
          }
          else
          {
            switch (Horizontal)
            {
              case iOSLayoutHorizontal.Stretch:
                if (/*(ContentSize.Width > BoundsSize.Width) ||*/ (!float.IsNaN(Width) && Width < BoundsSize.Width) || (!float.IsNaN(MaximumWidth) && MaximumWidth < BoundsSize.Width))
                {
                  ContentWidth = ContentSize.Width;
                  ContentX = (BoundsSize.Width - ContentWidth) / 2;
                }
                else
                {
                  ContentWidth = BoundsSize.Width;
                  ContentX = 0;
                }
                break;

              case iOSLayoutHorizontal.Center:
                ContentX = (BoundsSize.Width - ContentWidth) / 2;
                break;

              case iOSLayoutHorizontal.Left:
                ContentX = 0;
                break;

              case iOSLayoutHorizontal.Right:
                ContentX = BoundsSize.Width - ContentWidth;
                break;

              default:
                throw new Exception("LayoutVertical not handled.");
            }
          }

          var ContentHeight = ContentSize.Height;
          nfloat ContentY;

          if (ContentHeight > BoundsSize.Height)
          {
            ContentHeight = BoundsSize.Height;
            ContentY = 0;
          }
          else
          {
            switch (Vertical)
            {
              case iOSLayoutVertical.Stretch:
                if (/*(ContentSize.Height > BoundsSize.Height) ||*/ (!float.IsNaN(Height) && Height < BoundsSize.Height) || (!float.IsNaN(MaximumHeight) && MaximumHeight < BoundsSize.Height))
                {
                  ContentHeight = ContentSize.Height;
                  ContentY = (BoundsSize.Height - ContentHeight) / 2;
                }
                else
                {
                  ContentHeight = BoundsSize.Height;
                  ContentY = 0;
                }
                break;

              case iOSLayoutVertical.Center:
                ContentY = (BoundsSize.Height - ContentHeight) / 2;
                break;

              case iOSLayoutVertical.Top:
                ContentY = 0;
                break;

              case iOSLayoutVertical.Bottom:
                ContentY = BoundsSize.Height - ContentHeight;
                break;

              default:
                throw new Exception("LayoutVertical not handled.");
            }
          }

          //Element.Frame = LayoutMargins.InsetRect(new CGRect(ContentX, ContentY, ContentWidth, ContentHeight));
          Element.Hidden = false;
          Element.Frame = new CGRect(LayoutMargins.Left + ContentX, LayoutMargins.Top + ContentY, ContentWidth, ContentHeight);
        }
      }
    }

    UIView iOSLayoutElement.View
    {
      get { return this; }
    }
    CGSize iOSLayoutElement.CalculateSize(CGSize MaxSize)
    {
      var Result = CGSize.Empty;

      if (Visible)
      {
        Result = MeasureSize(this.FitToSize(MaxSize));

        Result.Width += LayoutMargins.Left + LayoutMargins.Right;
        Result.Height += LayoutMargins.Top + LayoutMargins.Bottom;
      }

      return Result;
    }

    private CGSize MeasureSize(CGSize MaxSize)
    {
      if (!float.IsNaN(Width) && !float.IsNaN(Height))
      {
        return new CGSize(Width, Height);
      }
      else
      {
        var ContentSize = MaxSize;
        if (!float.IsNaN(Width))
          ContentSize.Width = Width;
        else if (!float.IsNaN(MaximumWidth) && MaximumWidth < ContentSize.Width)
          ContentSize.Width = MaximumWidth;

        if (!float.IsNaN(Height))
          ContentSize.Height = Height;
        else if (!float.IsNaN(MaximumHeight) && MaximumHeight < ContentSize.Height)
          ContentSize.Height = MaximumHeight;

        var Result = Content != null ? Content.CalculateSize(ContentSize) : CGSize.Empty;

        if (!float.IsNaN(Width))
        {
          Result.Width = Width;
        }
        else
        {
          if (!float.IsNaN(MinimumWidth))
            Result.Width = Math.Max(MinimumWidth, (float)Result.Width);

          if (!float.IsNaN(MaximumWidth))
            Result.Width = Math.Min(MaximumWidth, (float)Result.Width);
        }

        if (!float.IsNaN(Height))
        {
          Result.Height = Height;
        }
        else
        {
          if (!float.IsNaN(MinimumHeight))
            Result.Height = Math.Max(MinimumHeight, (float)Result.Height);

          if (!float.IsNaN(MaximumHeight))
            Result.Height = Math.Min(MaximumHeight, (float)Result.Height);
        }

        return Result;
      }
    }

    private iOSLayoutElement Content;
    private iOSLayoutVertical Vertical;
    private iOSLayoutHorizontal Horizontal;
    private bool Visible;
    private float Width;
    private float Height;
    private float MinimumWidth;
    private float MinimumHeight;
    private float MaximumWidth;
    private float MaximumHeight;
  }

  public enum iOSLayoutOrientation
  {
    Vertical,
    Horizontal
  }

  public sealed class iOSLayoutFrame : iOSLayoutView, iOSLayoutElement
  {
    public void SetContentElement(iOSLayoutElement Content)
    {
      if (Content != this.Content)
      {
        if (this.Content != null)
          this.Content.View.RemoveFromSuperview();

        this.Content = Content;

        if (this.Content != null)
          AddSubview(this.Content.View);

        this.Arrange();
      }
    }

    public override void LayoutSubviews()
    {
      base.LayoutSubviews();

      if (Content != null)
        Content.View.Frame = LayoutMargins.InsetRect(Bounds);
    }

    CGSize iOSLayoutElement.CalculateSize(CGSize MaxSize)
    {
      var Result = Content != null ? Content.CalculateSize(this.FitToSize(MaxSize)) : CGSize.Empty;

      Result.Width += LayoutMargins.Left + LayoutMargins.Right;
      Result.Height += LayoutMargins.Top + LayoutMargins.Bottom;

      return Result;
    }

    UIView iOSLayoutElement.View
    {
      get { return this; }
    }

    private iOSLayoutElement Content;
  }

  public sealed class iOSLayoutStack : iOSLayoutView, iOSLayoutElement
  {
    public iOSLayoutStack()
    {
      this.TranslatesAutoresizingMaskIntoConstraints = false;
      this.LayoutMargins = UIEdgeInsets.Zero;
      this.ElementArray = new iOSLayoutElement[] { };
    }

    public void SetOrientation(iOSLayoutOrientation Orientation)
    {
      if (Orientation != this.Orientation)
      {
        this.Orientation = Orientation;

        this.Arrange();
      }
    }
    public void ComposeElements(IEnumerable<iOSLayoutElement> Elements)
    {
      var PreviousArray = ElementArray;
      this.ElementArray = Elements.ToArray();

      foreach (var Element in ElementArray.Except(PreviousArray))
        AddSubview(Element.View);

      foreach (var Element in PreviousArray.Except(ElementArray))
        Element.View.RemoveFromSuperview();

      this.Arrange();
    }

    public override void LayoutSubviews()
    {
      base.LayoutSubviews();

      switch (Orientation)
      {
        case iOSLayoutOrientation.Horizontal:
          var StackHeight = Bounds.Height - LayoutMargins.Top - LayoutMargins.Bottom;
          var ElementLeft = (nfloat)LayoutMargins.Left;

          var ColumnSize = new CGSize(UIView.NoIntrinsicMetric, StackHeight);

          foreach (var Element in ElementArray)
          {
            var ElementSize = Element.CalculateSize(ColumnSize);

            Element.View.Frame = new CGRect(ElementLeft, LayoutMargins.Top, ElementSize.Width, StackHeight);

            ElementLeft += ElementSize.Width;
          }
          break;

        case iOSLayoutOrientation.Vertical:
          var StackWidth = Bounds.Width - LayoutMargins.Left - LayoutMargins.Right;
          var ElementTop = (nfloat)LayoutMargins.Top;

          var RowSize = new CGSize(StackWidth, UIView.NoIntrinsicMetric);

          foreach (var Element in ElementArray)
          {
            var PanelSize = Element.CalculateSize(RowSize);

            Element.View.Frame = new CGRect(LayoutMargins.Left, ElementTop, StackWidth, PanelSize.Height);

            ElementTop += PanelSize.Height;
          }
          break;

        default:
          throw new Exception("LayoutOrientation is not handled.");
      }
    }

    UIView iOSLayoutElement.View
    {
      get { return this; }
    }
    CGSize iOSLayoutElement.CalculateSize(CGSize MaxSize)
    {
      var FitSize = this.FitToSize(MaxSize);

      var Result = new CGSize();

      switch (Orientation)
      {
        case iOSLayoutOrientation.Vertical:
          foreach (var Element in ElementArray)
          {
            var ElementSize = Element.CalculateSize(FitSize);

            if (ElementSize.Width > Result.Width)
              Result.Width = ElementSize.Width;

            Result.Height += ElementSize.Height;
          }
          break;

        case iOSLayoutOrientation.Horizontal:
          foreach (var Element in ElementArray)
          {
            var ElementSize = Element.CalculateSize(FitSize);

            if (ElementSize.Height > Result.Height)
              Result.Height = ElementSize.Height;

            Result.Width += ElementSize.Width;
          }
          break;

        default:
          throw new Exception("LayoutOrientation is not handled.");
      }

      Result.Width += LayoutMargins.Left + LayoutMargins.Right;
      Result.Height += LayoutMargins.Top + LayoutMargins.Bottom;

      return Result;
    }

    private iOSLayoutElement[] ElementArray;
    private iOSLayoutOrientation Orientation;
  }

  public sealed class iOSLayoutDock : iOSLayoutView, iOSLayoutElement
  {
    public iOSLayoutDock()
    {
      this.TranslatesAutoresizingMaskIntoConstraints = false;
      this.LayoutMargins = UIEdgeInsets.Zero;
      this.ElementList = new DistinctList<iOSLayoutElement>();
      this.HeaderList = new DistinctList<iOSLayoutElement>();
      this.ClientList = new DistinctList<iOSLayoutElement>();
      this.FooterList = new DistinctList<iOSLayoutElement>();
    }

    public void SetOrientation(iOSLayoutOrientation Orientation)
    {
      if (Orientation != this.Orientation)
      {
        this.Orientation = Orientation;

        this.Arrange();
      }
    }
    public void ComposeElements(IEnumerable<iOSLayoutElement> Headers, IEnumerable<iOSLayoutElement> Clients, IEnumerable<iOSLayoutElement> Footers)
    {
      this.HeaderList.Clear();
      HeaderList.AddRange(Headers);

      this.ClientList.Clear();
      ClientList.AddRange(Clients);

      this.FooterList.Clear();
      FooterList.AddRange(Footers);

      var PreviousElementList = ElementList;
      this.ElementList = new DistinctList<iOSLayoutElement>(HeaderList.Count + ClientList.Count + FooterList.Count);
      ElementList.AddRange(HeaderList);
      ElementList.AddRange(ClientList);
      ElementList.AddRange(FooterList);

      foreach (var Element in ElementList.Except(PreviousElementList))
        AddSubview(Element.View);

      foreach (var Header in PreviousElementList.Except(ElementList))
        Header.View.RemoveFromSuperview();

      this.Arrange();
    }

    public override void LayoutSubviews()
    {
      base.LayoutSubviews();

      var DockWidth = Bounds.Width - LayoutMargins.Left - LayoutMargins.Right;
      var DockHeight = Bounds.Height - LayoutMargins.Top - LayoutMargins.Bottom;

      switch (Orientation)
      {
        case iOSLayoutOrientation.Horizontal:
          var PanelLeft = (nfloat)LayoutMargins.Left;

          var ColumnSize = new CGSize(UIView.NoIntrinsicMetric, DockHeight);
          var HeaderWidth = (nfloat)0;

          foreach (var Header in HeaderList)
          {
            var HeaderSize = Header.CalculateSize(ColumnSize);
            Header.View.Frame = new CGRect(PanelLeft, LayoutMargins.Top, HeaderSize.Width, DockHeight);
            PanelLeft += HeaderSize.Width;
            HeaderWidth += HeaderSize.Width;
          }

          var FooterWidth = (nfloat)FooterList.Sum(F => F.CalculateSize(ColumnSize).Width);
          var RemainderWidth = DockWidth - HeaderWidth - FooterWidth;

          if (ClientList.Count > 0)
          {
            var SharedWidth = RemainderWidth < 0 ? 0 : RemainderWidth / ClientList.Count;

            var LastClient = ClientList.Last();

            foreach (var Client in ClientList)
            {
              var FrameWidth = SharedWidth;
              if (RemainderWidth > 0 && Client == LastClient)
                FrameWidth += RemainderWidth - (SharedWidth * ClientList.Count);

              Client.View.Frame = new CGRect(PanelLeft, LayoutMargins.Top, FrameWidth, DockHeight);
              PanelLeft += FrameWidth;
            }
          }

          var PanelRight = LayoutMargins.Left + (RemainderWidth < 0 ? DockWidth - RemainderWidth : DockWidth);

          foreach (var Footer in FooterList)
          {
            var FooterSize = Footer.CalculateSize(ColumnSize);
            PanelRight -= FooterSize.Width;
            Footer.View.Frame = new CGRect(PanelRight, LayoutMargins.Top, FooterSize.Width, DockHeight);
          }
          break;

        case iOSLayoutOrientation.Vertical:
          var PanelTop = (nfloat)LayoutMargins.Top;

          var RowSize = new CGSize(DockWidth, UIView.NoIntrinsicMetric);
          var HeaderHeight = (nfloat)0;

          foreach (var Header in HeaderList)
          {
            var HeaderSize = Header.CalculateSize(RowSize);
            Header.View.Frame = new CGRect(LayoutMargins.Left, PanelTop, DockWidth, HeaderSize.Height);
            PanelTop += HeaderSize.Height;
            HeaderHeight += HeaderSize.Height;
          }

          var FooterHeight = (nfloat)FooterList.Sum(F => F.CalculateSize(RowSize).Height);
          var RemainderHeight = DockHeight - HeaderHeight - FooterHeight;

          if (ClientList.Count > 0)
          {
            var SharedHeight = RemainderHeight < 0 ? 0 : RemainderHeight / ClientList.Count;
            var LastClient = ClientList.Last();

            foreach (var Client in ClientList)
            {
              var FrameHeight = SharedHeight;
              if (RemainderHeight > 0 && Client == LastClient)
                FrameHeight += RemainderHeight - (SharedHeight * ClientList.Count);

              Client.View.Frame = new CGRect(LayoutMargins.Left, PanelTop, DockWidth, FrameHeight);
              PanelTop += FrameHeight;
            }
          }

          var PanelBottom = LayoutMargins.Top + (RemainderHeight < 0 ? DockHeight - RemainderHeight : DockHeight);

          foreach (var Footer in FooterList)
          {
            var FooterSize = Footer.CalculateSize(RowSize);
            PanelBottom -= FooterSize.Height;
            Footer.View.Frame = new CGRect(LayoutMargins.Left, PanelBottom, DockWidth, FooterSize.Height);
          }
          break;

        default:
          throw new Exception("LayoutOrientation is not handled.");
      }
    }

    UIView iOSLayoutElement.View
    {
      get { return this; }
    }
    CGSize iOSLayoutElement.CalculateSize(CGSize MaxSize)
    {
      var FitSize = this.FitToSize(MaxSize);

      var Result = new CGSize();

      switch (Orientation)
      {
        case iOSLayoutOrientation.Vertical:
          foreach (var Element in ElementList)
          {
            var ElementSize = Element.CalculateSize(FitSize);

            if (ElementSize.Width > Result.Width)
              Result.Width = ElementSize.Width;

            Result.Height += ElementSize.Height;
          }
          break;

        case iOSLayoutOrientation.Horizontal:
          foreach (var Element in ElementList)
          {
            var ElementSize = Element.CalculateSize(FitSize);

            if (ElementSize.Height > Result.Height)
              Result.Height = ElementSize.Height;

            Result.Width += ElementSize.Width;
          }
          break;

        default:
          throw new Exception("LayoutOrientation is not handled.");
      }

      Result.Width += LayoutMargins.Left + LayoutMargins.Right;
      Result.Height += LayoutMargins.Top + LayoutMargins.Bottom;

      return Result;
    }

    private Inv.DistinctList<iOSLayoutElement> ElementList;
    private Inv.DistinctList<iOSLayoutElement> HeaderList;
    private Inv.DistinctList<iOSLayoutElement> ClientList;
    private Inv.DistinctList<iOSLayoutElement> FooterList;
    private iOSLayoutOrientation Orientation;
  }

  public sealed class iOSLayoutOverlay : iOSLayoutView, iOSLayoutElement
  {
    public iOSLayoutOverlay()
    {
      this.TranslatesAutoresizingMaskIntoConstraints = false;
      this.LayoutMargins = UIEdgeInsets.Zero; 
      this.ElementList = new DistinctList<iOSLayoutElement>();
    }

    public void RemoveElements()
    {
      if (ElementList.Count > 0)
      {
        foreach (var Element in ElementList)
          Element.View.RemoveFromSuperview();
        ElementList.Clear();

        this.Arrange();
      }
    }
    public void AddElement(iOSLayoutElement Element)
    {
      ElementList.Add(Element);
      AddSubview(Element.View);

      this.Arrange();
    }
    public void ComposeElements(IEnumerable<iOSLayoutElement> Elements)
    {
      var PreviousList = ElementList;
      this.ElementList = Elements.ToDistinctList();

      foreach (var Element in ElementList.Except(PreviousList))
        AddSubview(Element.View);

      foreach (var Element in PreviousList.Except(ElementList))
        Element.View.RemoveFromSuperview();

      this.Arrange();
    }

    public override void LayoutSubviews()
    {
      base.LayoutSubviews();

      var PanelFrame = LayoutMargins.InsetRect(Bounds);
      foreach (var Panel in ElementList)
        Panel.View.Frame = PanelFrame;
    }

    UIView iOSLayoutElement.View
    {
      get { return this; }
    }
    CGSize iOSLayoutElement.CalculateSize(CGSize MaxSize)
    {
      var FitSize = this.FitToSize(MaxSize);

      var Result = new CGSize();

      foreach (var Panel in ElementList)
      {
        var PanelSize = Panel.CalculateSize(FitSize);

        if (PanelSize.Width > Result.Width)
          Result.Width = PanelSize.Width;

        if (PanelSize.Height > Result.Height)
          Result.Height = PanelSize.Height;
      }

      Result.Width += LayoutMargins.Left + LayoutMargins.Right;
      Result.Height += LayoutMargins.Top + LayoutMargins.Bottom;

      return Result;
    }

    private Inv.DistinctList<iOSLayoutElement> ElementList;
  }

  public sealed class iOSLayoutCanvas : iOSLayoutView, iOSLayoutElement
  {
    public iOSLayoutCanvas()
    {
      this.TranslatesAutoresizingMaskIntoConstraints = false;
      this.LayoutMargins = UIEdgeInsets.Zero; 
      this.RecordList = new DistinctList<iOSLayoutRecord>();
    }

    public void RemoveElements()
    {
      if (RecordList.Count > 0)
      {
        foreach (var Record in RecordList)
          Record.Element.View.RemoveFromSuperview();
        RecordList.Clear();

        this.Arrange();
      }
    }
    public void AddElement(iOSLayoutElement Element, CGRect Rect)
    {
      RecordList.Add(new iOSLayoutRecord()
      {
        Element = Element,
        Rect = Rect
      });
      AddSubview(Element.View);

      this.Arrange();
    }

    public override void LayoutSubviews()
    {
      base.LayoutSubviews();

      foreach (var Record in RecordList)
        Record.Element.View.Frame = LayoutMargins.InsetRect(Record.Rect);
    }

    UIView iOSLayoutElement.View
    {
      get { return this; }
    }
    CGSize iOSLayoutElement.CalculateSize(CGSize MaxSize)
    {
      var Result = new CGSize();

      foreach (var Record in RecordList)
      {
        var RecordSize = Record.Rect.Size;

        if (RecordSize.Width > Result.Width)
          Result.Width = RecordSize.Width;

        if (RecordSize.Height > Result.Height)
          Result.Height = RecordSize.Height;
      }

      Result.Width += LayoutMargins.Left + LayoutMargins.Right;
      Result.Height += LayoutMargins.Top + LayoutMargins.Bottom;

      return Result;
    }

    private Inv.DistinctList<iOSLayoutRecord> RecordList;

    private sealed class iOSLayoutRecord
    {
      public iOSLayoutElement Element;
      public CGRect Rect;
    }
  }

  public sealed class iOSLayoutTable : iOSLayoutView, iOSLayoutElement
  {
    public iOSLayoutTable()
    {
      this.TranslatesAutoresizingMaskIntoConstraints = false;
      this.LayoutMargins = UIEdgeInsets.Zero;
      this.RowList = new DistinctList<iOSLayoutTableRow>();
      this.ColumnList = new DistinctList<iOSLayoutTableColumn>();
      this.CellList = new DistinctList<iOSLayoutTableCell>();
      this.ElementList = new DistinctList<iOSLayoutElement>();
    }

    public void ComposeElements(IEnumerable<iOSLayoutTableRow> Rows, IEnumerable<iOSLayoutTableColumn> Columns, IEnumerable<iOSLayoutTableCell> Cells)
    {
      this.RowList.Clear();
      RowList.AddRange(Rows);

      this.ColumnList.Clear();
      ColumnList.AddRange(Columns);

      this.CellList.Clear();
      CellList.AddRange(Cells);

      var PreviousElementList = ElementList;
      this.ElementList = new DistinctList<iOSLayoutElement>(RowList.Count + ColumnList.Count + CellList.Count);
      ElementList.AddRange(RowList.Select(R => R.Element).ExceptNull());
      ElementList.AddRange(ColumnList.Select(R => R.Element).ExceptNull());
      ElementList.AddRange(CellList.Select(R => R.Element).ExceptNull());

      foreach (var Element in ElementList.Except(PreviousElementList))
        AddSubview(Element.View);

      foreach (var Header in PreviousElementList.Except(ElementList))
        Header.View.RemoveFromSuperview();

      this.Arrange();
    }

    public override void LayoutSubviews()
    {
      base.LayoutSubviews();
      
      var AvailableWidth = Bounds.Width - LayoutMargins.Left - LayoutMargins.Right;
      var AvailableHeight = Bounds.Height - LayoutMargins.Top - LayoutMargins.Bottom;

      var RowLimit = new CGSize(AvailableWidth, UIView.NoIntrinsicMetric);
      var ColumnLimit = new CGSize(UIView.NoIntrinsicMetric, AvailableHeight);

      var StaticRowList = RowList.Where(R => !R.Length.IsStar).ToDistinctList();
      var DynamicRowList = RowList.Where(R => R.Length.IsStar).ToDistinctList();

      var StaticColumnList = ColumnList.Where(R => !R.Length.IsStar).ToDistinctList();
      var DynamicColumnList = ColumnList.Where(R => R.Length.IsStar).ToDistinctList();

      var LastStarRow = DynamicRowList.LastOrDefault();
      var LastStarColumn = DynamicColumnList.LastOrDefault();

      var DynamicRowWeight = (nfloat)DynamicRowList.Sum(R => R.Length.Size ?? 1);
      var DynamicColumnWeight = (nfloat)DynamicColumnList.Sum(C => C.Length.Size ?? 1);

      var StaticHeight = (nfloat)StaticRowList.Sum(R => R.CalculateSize(RowLimit).Height);
      var StaticWidth = (nfloat)StaticColumnList.Sum(C => C.CalculateSize(ColumnLimit).Width);

      var RemainderWidth = AvailableWidth - StaticWidth;
      var RemainderHeight = AvailableHeight - StaticHeight;

      var ActualSize = new CGSize(DynamicColumnList.Count > 0 ? AvailableWidth : StaticWidth, DynamicRowList.Count > 0 ? AvailableHeight : StaticHeight);

      var TableTop = LayoutMargins.Top;
      var TableLeft = LayoutMargins.Left;

      foreach (var Column in ColumnList)
      {
        var ColumnWidth = Column.CalculateSize(ActualSize).Width;

        if (Column.Length.IsStar)
        {
          ColumnWidth = ((Column.Length.Size ?? 1) / DynamicColumnWeight) * RemainderWidth;
          // TODO: last star column bucket.
        }

        if (Column.Element != null)
          Column.Element.View.Frame = new CGRect(TableLeft, TableTop, ColumnWidth, ActualSize.Height);

        Column.TempStart = TableLeft;
        Column.TempLength = ColumnWidth;

        TableLeft += ColumnWidth;
      }

      TableTop = LayoutMargins.Top;
      TableLeft = LayoutMargins.Left;

      foreach (var Row in RowList)
      {
        var RowHeight = Row.CalculateSize(ActualSize).Height;

        if (Row.Length.IsStar)
        {
          RowHeight = ((Row.Length.Size ?? 1) / DynamicRowWeight) * RemainderHeight;
          // TODO: last star row bucket.
        }

        if (Row.Element != null)
          Row.Element.View.Frame = new CGRect(TableLeft, TableTop, ActualSize.Width, RowHeight);

        Row.TempStart = TableTop;
        Row.TempLength = RowHeight;

        TableTop += RowHeight;
      }

      foreach (var Cell in CellList)
      {
        if (Cell.Element != null)
          Cell.Element.View.Frame = new CGRect(Cell.Column.TempStart, Cell.Row.TempStart, Cell.Column.TempLength, Cell.Row.TempLength);
      }
    }

    internal Inv.DistinctList<iOSLayoutTableRow> RowList { get; private set; }
    internal Inv.DistinctList<iOSLayoutTableColumn> ColumnList { get; private set; }
    internal Inv.DistinctList<iOSLayoutTableCell> CellList { get; private set; }

    UIView iOSLayoutElement.View
    {
      get { return this; }
    }
    CGSize iOSLayoutElement.CalculateSize(CGSize MaxSize)
    {
      var FitSize = this.FitToSize(MaxSize);

      var Result = new CGSize();

      foreach (var Row in RowList)
      {
        var RowSize = Row.CalculateSize(FitSize);

        if (RowSize.Width > Result.Width)
          Result.Width = RowSize.Width;

        Result.Height += RowSize.Height;
      }

      foreach (var Column in ColumnList)
      {
        var ColumnSize = Column.CalculateSize(FitSize);

        if (ColumnSize.Height > Result.Height)
          Result.Height = ColumnSize.Height;

        Result.Width += ColumnSize.Width;
      }

      Result.Width += LayoutMargins.Left + LayoutMargins.Right;
      Result.Height += LayoutMargins.Top + LayoutMargins.Bottom;

      return Result;
    }

    private DistinctList<iOSLayoutElement> ElementList;
  }

  public sealed class iOSLayoutTableLength
  {
    public iOSLayoutTableLength(bool Fill, int? Value)
    {
      this.Fill = Fill;
      this.Size = Value;
    }

    public bool Fill { get; private set; }
    public int? Size { get; private set; }
    public bool IsStar
    {
      get { return Fill; }
    }
    public bool IsAuto
    {
      get { return !Fill && Size == null; }
    }
    public bool IsFixed
    {
      get { return !Fill && Size != null; }
    }
  }

  public sealed class iOSLayoutTableColumn
  {
    public iOSLayoutTableColumn(iOSLayoutTable Table, iOSLayoutElement Element, bool Fill, int? Value)
    {
      this.Table = Table;
      this.Element = Element;
      this.Length = new iOSLayoutTableLength(Fill, Value);
    }

    public iOSLayoutElement Element { get; private set; }
    public iOSLayoutTableLength Length { get; private set; }

    internal nfloat TempStart;
    internal nfloat TempLength;

    internal CGSize CalculateSize(CGSize MaxSize)
    {
      var Result = Element != null ? Element.CalculateSize(MaxSize) : CGSize.Empty;

      if (Length.IsFixed)
        Result.Width = Length.Size.Value;

      foreach (var Cell in Table.CellList.Where(C => C.Column == this))
      {
        var CellSize = Cell.Element.CalculateSize(MaxSize);

        if (!Length.IsFixed && CellSize.Width > Result.Width)
          Result.Width = CellSize.Width;

        if (Cell.Row.Length.IsFixed)
          Result.Height += Cell.Row.Length.Size.Value;
        else
          Result.Height += CellSize.Height;
      }

      return Result;
    }

    private iOSLayoutTable Table;
  }

  public sealed class iOSLayoutTableRow
  {
    public iOSLayoutTableRow(iOSLayoutTable Table, iOSLayoutElement Element, bool Fill, int? Value)
    {
      this.Table = Table;
      this.Element = Element;
      this.Length = new iOSLayoutTableLength(Fill, Value);
    }

    public iOSLayoutElement Element { get; private set; }
    public iOSLayoutTableLength Length { get; private set; }

    internal nfloat TempStart;
    internal nfloat TempLength;

    internal CGSize CalculateSize(CGSize MaxSize)
    {
      var Result = Element != null ? Element.CalculateSize(MaxSize) : CGSize.Empty;

      if (Length.IsFixed)
        Result.Height = Length.Size.Value;

      foreach (var Cell in Table.CellList.Where(C => C.Row == this))
      {
        var CellSize = Cell.Element.CalculateSize(MaxSize);

        if (!Length.IsFixed && CellSize.Height > Result.Height)
          Result.Height = CellSize.Height;

        if (Cell.Column.Length.IsFixed)
          Result.Width += Cell.Column.Length.Size.Value;
        else
          Result.Width += CellSize.Width;
      }

      return Result;
    }

    private iOSLayoutTable Table;
  }

  public sealed class iOSLayoutTableCell
  {
    public iOSLayoutTableCell(iOSLayoutTableColumn Column, iOSLayoutTableRow Row, iOSLayoutElement Element)
    {
      this.Column = Column;
      this.Row = Row;
      this.Element = Element;
    }

    public iOSLayoutTableColumn Column { get; private set; }
    public iOSLayoutTableRow Row { get; private set; }
    public iOSLayoutElement Element { get; private set; }
  }

  public sealed class iOSLayoutScroll : UIScrollView, iOSLayoutElement
  {
    public iOSLayoutScroll()
    {
      this.TranslatesAutoresizingMaskIntoConstraints = false;
      this.LayoutMargins = UIEdgeInsets.Zero;
    }

    public void SetOrientation(iOSLayoutOrientation Orientation)
    {
      if (Orientation != this.Orientation)
      {
        this.Orientation = Orientation;

        this.Arrange();
      }
    }
    public void SetContentElement(iOSLayoutElement Content)
    {
      if (Content != this.Content)
      {
        if (this.Content != null)
          this.Content.View.RemoveFromSuperview();

        this.Content = Content;

        if (this.Content != null)
          AddSubview(this.Content.View);

        this.Arrange();
      }
    }

    public override void LayoutSubviews()
    {
      base.LayoutSubviews();

      if (Content != null)
      {
        var LayoutSize = Content.CalculateSize(Bounds.Size);

        if (Orientation == iOSLayoutOrientation.Vertical)
        {
          LayoutSize.Width = Bounds.Width;

          var LayoutHeight = Bounds.Height;
          if (LayoutSize.Height < LayoutHeight)
            LayoutSize.Height = LayoutHeight;
        }
        else if (Orientation == iOSLayoutOrientation.Horizontal)
        {
          LayoutSize.Height = Bounds.Height;

          var LayoutWidth = Bounds.Width;
          if (LayoutSize.Width < LayoutWidth)
            LayoutSize.Width = LayoutWidth;
        }

        this.ContentSize = LayoutSize;

        Content.View.Frame = LayoutMargins.InsetRect(new CGRect(0, 0, LayoutSize.Width, LayoutSize.Height));
      }
      else
      {
        ContentSize = CGSize.Empty;
      }
    }

    UIView iOSLayoutElement.View
    {
      get { return this; }
    }
    CGSize iOSLayoutElement.CalculateSize(CGSize MaxSize)
    {
      var Result = Content != null ? Content.CalculateSize(this.FitToSize(MaxSize)) : CGSize.Empty;

      Result.Width += LayoutMargins.Left + LayoutMargins.Right;
      Result.Height += LayoutMargins.Top + LayoutMargins.Bottom;

      return Result;
    }

    private iOSLayoutElement Content;
    private iOSLayoutOrientation Orientation;
  }

  internal sealed class iOSLayoutDatePicker : UIDatePicker, iOSLayoutElement
  {
    public iOSLayoutDatePicker()
    {
      this.TranslatesAutoresizingMaskIntoConstraints = false;
      this.LayoutMargins = UIEdgeInsets.Zero;
    }

    UIView iOSLayoutElement.View
    {
      get { return this; }
    }
    CGSize iOSLayoutElement.CalculateSize(CGSize MaxSize)
    {
      var Result = IntrinsicContentSize;

      Result.Width += LayoutMargins.Left + LayoutMargins.Right;
      Result.Height += LayoutMargins.Top + LayoutMargins.Bottom;

      return Result;
    }
  }

  internal sealed class iOSPopoverViewController : UIViewController
  {
    public iOSPopoverViewController()
    {
      this.ModalPresentationStyle = UIKit.UIModalPresentationStyle.Popover;
    }

    public event Action LoadEvent;

    public override void ViewDidLoad()
    {
      base.ViewDidLoad();

      if (LoadEvent != null)
        LoadEvent();
    }
  }

  public sealed class iOSLayoutController : UIKit.UIViewController
  {
    public iOSLayoutController()
      : base()
    {
      this.LeftEdgeRecognizer = new UIKit.UIScreenEdgePanGestureRecognizer(() =>
      {
        if (LeftEdgeRecognizer.State == UIGestureRecognizerState.Recognized)
        {
          if (LeftEdgeSwipeEvent != null)
            LeftEdgeSwipeEvent();
        }
      }) { Edges = UIKit.UIRectEdge.Left };
      View.AddGestureRecognizer(LeftEdgeRecognizer);

      this.RightEdgeRecognizer = new UIKit.UIScreenEdgePanGestureRecognizer(() =>
      {
        if (RightEdgeRecognizer.State == UIGestureRecognizerState.Recognized)
        {
          if (RightEdgeSwipeEvent != null)
            RightEdgeSwipeEvent();
        }
      }) { Edges = UIKit.UIRectEdge.Right };

      View.AddGestureRecognizer(RightEdgeRecognizer);
    }

    public event Action LoadEvent;
    public event Action LeftEdgeSwipeEvent;
    public event Action RightEdgeSwipeEvent;
    public event Action MemoryWarningEvent;
    public event Action<CoreGraphics.CGSize> ResizeEvent;
    public event Func<UIKeyCommand[]> KeyQuery;
    public event Action<UIKit.UIKeyModifierFlags, string> KeyEvent;

    public override bool CanBecomeFirstResponder
    {
      get { return true; }
    }
    public override UIKeyCommand[] KeyCommands
    {
      get
      {
        if (KeyQuery != null)
          return KeyQuery();
        else
          return base.KeyCommands;
      }
    }

    public void SetContentElement(iOSLayoutElement Content)
    {
      if (this.Content != Content)
      {
        if (this.Content != null)
          this.Content.View.RemoveFromSuperview();

        this.Content = Content;

        if (this.Content != null)
          View.AddSubview(this.Content.View);
      }
    }
    public void Reload()
    {
      if (IsLoaded)
        LoadInvoke();
    }

    public override void ViewDidLayoutSubviews()
    {
      base.ViewDidLayoutSubviews();

      if (Content != null)
        Content.View.Frame = View.Bounds;
    }
    public override void DidReceiveMemoryWarning()
    {
      // Release any cached data, images, etc that aren't in use.
      if (MemoryWarningEvent != null)
        MemoryWarningEvent();

      base.DidReceiveMemoryWarning();
    }
    public override void ViewDidLoad()
    {
      base.ViewDidLoad();

      //View.TranslatesAutoresizingMaskIntoConstraints = false; // NOTE: causes the background colour to be lost.
      View.LayoutMargins = UIEdgeInsets.Zero; // NOTE: this is a workaround to avoid the default leading/trailing 16pt margin.

      this.IsLoaded = true;

      LoadInvoke();
    }
    public override void ViewWillAppear(bool animated)
    {
      base.ViewWillAppear(animated);
    }
    public override void ViewDidAppear(bool animated)
    {
      base.ViewDidAppear(animated);

      BecomeFirstResponder();
    }
    public override void ViewWillDisappear(bool animated)
    {
      base.ViewWillDisappear(animated);
    }
    public override void ViewDidDisappear(bool animated)
    {
      base.ViewDidDisappear(animated);
    }
    public override void ViewWillTransitionToSize(CoreGraphics.CGSize toSize, IUIViewControllerTransitionCoordinator coordinator)
    {
      base.ViewWillTransitionToSize(toSize, coordinator);

      if (ResizeEvent != null)
        ResizeEvent(toSize);
    }
    public override bool ShouldAutorotateToInterfaceOrientation(UIInterfaceOrientation toInterfaceOrientation)
    {
      return true;
    }
    public override void WillAnimateRotation(UIInterfaceOrientation toInterfaceOrientation, double duration)
    {
      base.WillAnimateRotation(toInterfaceOrientation, duration);
    }

    private static bool UserInterfaceIdiomIsPhone
    {
      get { return UIKit.UIDevice.CurrentDevice.UserInterfaceIdiom == UIKit.UIUserInterfaceIdiom.Phone; }
    }

    [Foundation.Export("KeyDown:")]
    private void KeyDown(UIKeyCommand Command)
    {
      if (KeyEvent != null)
        KeyEvent(Command.ModifierFlags, Command.Input);
    }

    private void LoadInvoke()
    {
      if (LoadEvent != null)
        LoadEvent();
    }

    private bool IsLoaded;
    private iOSLayoutElement Content;
    private UIScreenEdgePanGestureRecognizer LeftEdgeRecognizer;
    private UIScreenEdgePanGestureRecognizer RightEdgeRecognizer;
  }

  public sealed class iOSNavigationControllerDelegate : UINavigationControllerDelegate
  {
    public override void DidShowViewController(UINavigationController navigationController, UIViewController viewController, bool animated)
    {
      if (animated)
      {
        foreach (var child in navigationController.ChildViewControllers)
        {
          if (child != viewController)
            child.RemoveFromParentViewController();
        }
      }
    }
  }

  [Foundation.Register("iOSAppDelegate")]
  internal sealed class iOSAppDelegate : UIKit.UIApplicationDelegate
  {
    public override UIKit.UIWindow Window { get; set; }

    public override void OnActivated(UIApplication application)
    {
    }
    public override void OnResignActivation(UIApplication application)
    {
      iOSEngine.Pausing();
    }
    public override void DidEnterBackground(UIApplication application)
    {
    }
    public override void WillEnterForeground(UIApplication application)
    {
      iOSEngine.Resuming();
    }
    public override void WillTerminate(UIApplication application)
    {
      iOSEngine.Terminating();
    }
    public override bool FinishedLaunching(UIKit.UIApplication app, Foundation.NSDictionary options)
    {
      this.Window = iOSEngine.Launching();

      return true;
    }
    public override void ReceiveMemoryWarning(UIApplication application)
    {
      // TODO: what to do?
    }

    internal static iOSEngine iOSEngine;
  }

  internal static class iOSLayoutFoundation
  {
    public static CGSize FitToSize(this UIView iOSView, CGSize MaxSize)
    {
      var Result = MaxSize;
      if (Result.Width != UIView.NoIntrinsicMetric)
        Result.Width -= iOSView.LayoutMargins.Left + iOSView.LayoutMargins.Right;

      if (Result.Height != UIView.NoIntrinsicMetric)
        Result.Height -= iOSView.LayoutMargins.Top + iOSView.LayoutMargins.Bottom;

      return Result;
    }
    public static void Arrange(this UIView iOSView)
    {
      var iOSCurrent = iOSView;
      while (iOSCurrent != null)
      {
        iOSCurrent.SetNeedsLayout();

        iOSCurrent = iOSCurrent.Superview;
      }
    }
    public static void FadeIn(this UIView iOSView, double Duration = 0.3, double Delay = 0.0, Action CompleteAction = null)
    {
      Fade(iOSView, MinAlpha, MaxAlpha, Duration, Delay, CompleteAction);
    }
    public static void FadeOut(this UIView iOSView, double Duration = 0.3, double Delay = 0.0, Action CompleteAction = null)
    {
      Fade(iOSView, MaxAlpha, MinAlpha, Duration, Delay, CompleteAction);
    }
    public static void SlideInLeft(this UIView iOSView, double Duration = 0.3, Action CompleteAction = null)
    {
      SlideHorizontal(iOSView, true, true, Duration, CompleteAction);
    }
    public static void SlideOutRight(this UIView iOSView, double Duration = 0.3, Action CompleteAction = null)
    {
      SlideHorizontal(iOSView, false, false, Duration, CompleteAction);
    }
    public static void Fade(this UIView iOSView, nfloat FromAlpha, nfloat ToAlpha, double Duration = 0.3, double Delay = 0.0, Action CompleteAction = null, bool UserInteractions = false)
    {
      iOSView.Alpha = FromAlpha;
      iOSView.Transform = CGAffineTransform.MakeIdentity();
      UIView.Animate(Duration, Delay, UserInteractions ? UIViewAnimationOptions.CurveEaseInOut | UIViewAnimationOptions.AllowUserInteraction : UIViewAnimationOptions.CurveEaseInOut, () => iOSView.Alpha = ToAlpha, CompleteAction);
    }

    private static void SlideHorizontal(this UIView iOSView, bool IsIn, bool FromLeft, double Duration = 0.3, Action CompleteAction = null)
    {
      var MinTransform = CGAffineTransform.MakeTranslation((FromLeft ? -1 : 1) * iOSView.Bounds.Width, 0);
      var MaxTransform = CGAffineTransform.MakeIdentity();

      iOSView.Transform = IsIn ? MinTransform : MaxTransform;
      UIView.Animate(Duration, 0, UIViewAnimationOptions.CurveEaseInOut, () => iOSView.Transform = IsIn ? MaxTransform : MinTransform, CompleteAction);
    }

    private static readonly nfloat MinAlpha = (nfloat)0.0f;
    private static readonly nfloat MaxAlpha = (nfloat)1.0f;
  }
}